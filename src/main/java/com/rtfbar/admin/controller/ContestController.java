package com.rtfbar.admin.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.rtfbar.admin.dao.impl.DAORegistry;
import com.rtfbar.admin.data.ContestGrandWinners;
import com.rtfbar.admin.data.ContestParticipants;
import com.rtfbar.admin.data.ContestQuestions;
import com.rtfbar.admin.data.ContestWinner;
import com.rtfbar.admin.data.Contests;
import com.rtfbar.admin.data.PackageDetails;
import com.rtfbar.admin.data.QuestionBank;
import com.rtfbar.admin.firebase.FirestoreDataMigrationThread;
import com.rtfbar.admin.firebase.FirestoreUtil;
import com.rtfbar.admin.notification.Notification;
import com.rtfbar.admin.utils.ExcelUtil;
import com.rtfbar.admin.utils.GridHeaderFilters;
import com.rtfbar.admin.utils.GridHeaderFiltersUtil;
import com.rtfbar.admin.utils.PaginationUtil;
import com.rtfbar.admin.utils.Util;

@Controller
public class ContestController {
	

	@RequestMapping(value = "/Contest")
	public String loadContestsForFireBasePage(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			
			model.addAttribute("sts",0);
			model.addAttribute("status",status);
			String returnPage = "";
			GridHeaderFilters filter = null;
			
			if(status== null || status.isEmpty()){
				System.err.println("Invalid status found."+status);
				model.addAttribute("msg", "Invalid status found.");
				return returnPage;
			}
			
			if(status.equalsIgnoreCase("ALL") || status.equalsIgnoreCase("EXPIRED")){
				returnPage =  "page-contests-firebase-mobile-allcontests";
			}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
				returnPage = "page-contests-firebase-mobile-questionbank";
			}else if(status.equalsIgnoreCase("TODAY")){
				returnPage = "page-contests-firebase-mobile-todaycontest";
			}
			
			
			if(status.equalsIgnoreCase("ALL")){
				Integer count = 0;
				filter = GridHeaderFiltersUtil.getContestsFilter(headerFilter);
				List<Contests> contestsList = DAORegistry.getContestsDAO().getAllContests(null, filter,"'ACTIVE','STARTED'");
				
				if(contestsList != null && contestsList.size() > 0){
					for(Contests co : contestsList){
						System.out.println(co.getStartDateTimeStr());
					}
					count = contestsList.size();
				}
				if(contestsList == null || contestsList.size() <= 0){
					model.addAttribute("msg","No Contests found.");
					model.addAttribute("contestsList",contestsList);
					model.addAttribute("contestsPagingInfo",new Gson().toJson(PaginationUtil.getDummyPaginationParameter(count)));
					return returnPage;
				}
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("sts",1);
					model.addAttribute("contestsList",contestsList);
					model.addAttribute("contestsPagingInfo",PaginationUtil.getDummyPaginationParameter(count));
					return returnPage;
				}else{
					model.addAttribute("sts",1);
					model.addAttribute("contestsList",new Gson().toJson(contestsList));
					model.addAttribute("contestsPagingInfo", new Gson().toJson(PaginationUtil.getDummyPaginationParameter(count)));
					return returnPage;
				}
				
			}else if(status.equalsIgnoreCase("EXPIRED")){
				Integer count = 0;
				filter = GridHeaderFiltersUtil.getContestsFilter(headerFilter);
				List<Contests> contestsList = DAORegistry.getContestsDAO().getAllContests(null, filter,"'EXPIRED'");
				
				if(contestsList != null && contestsList.size() > 0){
					for(Contests co : contestsList){
						System.out.println(co.getStartDateTimeStr());
					}
					count = contestsList.size();
				}
				if(contestsList == null || contestsList.size() <= 0){
					model.addAttribute("msg","No Contests found.");
					model.addAttribute("contestsList",contestsList);
					model.addAttribute("contestsPagingInfo",new Gson().toJson(PaginationUtil.getDummyPaginationParameter(count)));
					return returnPage;
				}
				
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("sts",1);
					model.addAttribute("contestsList",contestsList);
					model.addAttribute("contestsPagingInfo",PaginationUtil.getDummyPaginationParameter(count));
					return returnPage;
				}else{
					model.addAttribute("sts",1);
					model.addAttribute("contestsList",new Gson().toJson(contestsList));
					model.addAttribute("contestsPagingInfo", new Gson().toJson(PaginationUtil.getDummyPaginationParameter(count)));
					return returnPage;
				}
			}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
				Integer count = 0;
				//filter = GridHeaderFiltersUtil.getQuestionBankFilter(headerFilter); 
				List<QuestionBank> questionBankList = null;
				/*if(status.equalsIgnoreCase("ACTIVEQ")){
					questionBankList = DAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,"'ACTIVE'",true);
					count = DAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,"'ACTIVE'");
				}else if(status.equalsIgnoreCase("USEDQ")){
					questionBankList = DAORegistry.getQuestionBankDAO().getQuestionBankList(filter, null, pageNo,"'USED'",true);
					count = DAORegistry.getQuestionBankDAO().getQuestionBankCount(filter, null,"'USED'");
				}
				
				if(questionBankList == null || questionBankList.size() <= 0){
					model.addAttribute("msg","No Question's found from Question Bank.");
					return returnPage;
				}
				model.addAttribute("questionBankList", questionBankList);
				model.addAttribute("questionBankPagingInfo", PaginationUtil.getDummyPaginationParameter(count));
				model.addAttribute("sts",1);
				return returnPage;*/
				model.addAttribute("msg","Invalid page.");
				return returnPage;
			}else{
				if(status.equalsIgnoreCase("TODAY")){
					Calendar today = Calendar.getInstance();
					today.set(Calendar.HOUR, 00);
					today.set(Calendar.MINUTE, 00);
					today.set(Calendar.SECOND, 00);
					Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'ACTIVE','STARTED'");
					if(contest== null){
						System.err.println("There is no scheduled contest for today.");
						model.addAttribute("msg","There is no scheduled contest for today.");
						return returnPage;
					}
					Integer questionCount = DAORegistry.getContestQuestionsDAO().getQuestionCount(contest.getId());
					if(today.getTime().compareTo(contest.getStartDateTime()) > 0){
						System.err.println("Not allowed to start contest, There is a past dated Contest in ACTIVE status please end it or update it to future date from All contest ACTIVE tab.");
						model.addAttribute("msg","Not allowed to start contest, Contest with past date is in ACTIVE status please end it/update it to future date from All contest ACTIVE tab.");
						return returnPage;
					}
					if(!contest.getQuestionSize().equals(questionCount)){
						System.err.println("Not allowed to start contest, There is mismatch in number of question configured in contest level is "+contest.getQuestionSize()+" and actual number of question in contest is."+questionCount);
						model.addAttribute("msg","Not allowed to start contest, There is mismatch in number of question configured in contest level is "+contest.getQuestionSize()+" and actual number of question in contest is."+questionCount);
						return returnPage;
					}
					if(contest.getStatus().equalsIgnoreCase("STARTED")){
						if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("ANSWER")){
							Map<String, String> qMap = Util.getParameterMap(request);
							qMap.put("contestId", contest.getId().toString());
							qMap.put("questionNo",contest.getLastQuestionNo().toString());
							
							ContestQuestions question = DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contest.getId(), contest.getLastQuestionNo());
							if(question == null){
								model.addAttribute("buttonText","NONE");
								System.err.println("Contest last state is not found, Resume contest is not allowed.");
								model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
								return returnPage;
							}
							
							String buttoneText = null;
							buttoneText = Util.getResumeContestText(contest,question);
							model.addAttribute("buttonText",buttoneText);
							if(buttoneText.equalsIgnoreCase("NONE")){
								model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
							}
							model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
						}else{
							model.addAttribute("buttonText",Util.getResumeContestText(contest,null));
							model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
						}
					}else{
						model.addAttribute("buttonText","Start Contest");
						model.addAttribute("lastQuestionNo",0);
					}
					model.addAttribute("contest",contest);
					model.addAttribute("sts",1);
					return returnPage;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdateContest")
	public String updateContest(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			String action = request.getParameter("action");
			String contestIdStr = request.getParameter("contestId");
			String contestName = request.getParameter("contestName");
			String startDateHour = request.getParameter("startDateHour");
			String startDateMinute = request.getParameter("startDateMinute");
			String contestFromDate = request.getParameter("contestFromDate");
			String maxTickets = request.getParameter("maxWinners");
			String ticketsPerWinner = request.getParameter("prizeQty");
			String questionSize = request.getParameter("questionSize");
			String prizeVal = request.getParameter("prizeValue");
			String isSpPublicStr = request.getParameter("isPublicSponsor");
			String status = request.getParameter("status");
			String extContestName = request.getParameter("prizeText");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			model.addAttribute("sts",0);
			model.addAttribute("status",status);
			String returnPage =  "";
			
			String statuses = null;
			if(status!= null && status.equalsIgnoreCase("ALL")){
				statuses = "'ACTIVE','STARTED'";
			}else if(status!= null && status.equalsIgnoreCase("EXPIRED")){
				statuses = "'EXPIRED'";
			}
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				if(contestIdStr == null || contestIdStr.isEmpty()){
					model.addAttribute("msg","Please select any Contest to Edit.");
					return returnPage;
				}
				Integer contestId = Integer.parseInt(contestIdStr);
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<Contests> contestsList = DAORegistry.getContestsDAO().getAllContests(contestId, filter,null);
				model.addAttribute("contestsList", contestsList);
				model.addAttribute("sts",1);
				return returnPage;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				if(contestName==null || contestName.isEmpty()){
					System.err.println("Please provide Contest Name.");
					model.addAttribute("msg","Please provide Contest Name.");
					return returnPage;
				}
				if(contestFromDate==null || contestFromDate.isEmpty()){
					System.err.println("Please provide contest start date.");
					model.addAttribute("msg","Please provide contest start date.");
					return returnPage;
				}
				if(startDateHour==null || startDateHour.isEmpty()){
					System.err.println("Please provide contest start time hours.");
					model.addAttribute("msg","Please provide contest start time hours.");
					return returnPage;
				}
				if(startDateMinute==null || startDateMinute.isEmpty()){
					System.err.println("Please provide contest start time minutes.");
					model.addAttribute("msg","Please provide contest start time minutes.");
					return returnPage;
				}
				if(isSpPublicStr == null || isSpPublicStr.isEmpty()){
					isSpPublicStr = "false";
				}
				
				if(maxTickets==null || maxTickets.isEmpty()){
					maxTickets="0";
				}
				if(prizeVal==null || prizeVal.isEmpty()){
					prizeVal = "0.0";
				}
				if(ticketsPerWinner==null || ticketsPerWinner.isEmpty()){
					ticketsPerWinner ="0";
				}
				if(questionSize==null || questionSize.isEmpty()){
					System.err.println("Please provide number of question.");
					model.addAttribute("msg","Please provide number of question.");
					return returnPage;
				}
				
				Integer maxTicket = null;
				Double prizeValue = null;
				Integer ticketPerWinner = null;
				Integer questionSizes = null;
				Boolean isPublicSponsor = Boolean.FALSE;
				try {
					maxTicket = Integer.parseInt(maxTickets);
					ticketPerWinner =  Integer.parseInt(ticketsPerWinner);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("invalid prize qty per winner.");
					model.addAttribute("msg","invalid prize qty per winner.");
					return returnPage;
				}
				
				try {
					isPublicSponsor = Boolean.parseBoolean(isSpPublicStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("invalid sponsor option public/private selection");
					model.addAttribute("msg","invalid sponsor option public/private selection");
					return returnPage;
				}
				
				try {
					questionSizes = Integer.parseInt(questionSize);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid integer for Number of questions.");
					model.addAttribute("msg","Please provide valid integer for Number of questions.");
					return returnPage;
				}
				
				try {
					prizeValue =Double.parseDouble(prizeVal);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid integer for Number of questions.");
					model.addAttribute("msg","Please provide valid integer for Number of questions.");
					return returnPage;
				}
								
				
				String contestStartDateStr = "";			
				contestStartDateStr = contestFromDate+" "+startDateHour+":"+startDateMinute+":00";
				Date startDate = null;
				try {
					startDate = Util.getDateWithTwentyFourHourFormat1(contestStartDateStr);
				} catch (Exception e) {
					System.err.println("Please provide contest start date.");
					model.addAttribute("msg","Please provide contest start date.");
					return returnPage;
				}
				
				Date today = new Date();
				Contests contest = null;
				
				if(action.equalsIgnoreCase("UPDATE")){
					if(contestIdStr == null || contestIdStr.isEmpty()){
						System.err.println("Please select any Contest to Update.");
						model.addAttribute("msg","Please select any Contest to Update.");
						return returnPage;
					}
					contest = DAORegistry.getContestsDAO().get(Integer.parseInt(contestIdStr));
					if(contest == null){
						System.err.println("Contest is not found.");
						model.addAttribute("msg","Contest is not found.");
						return returnPage;
					}
				}
				if(contest == null){
					contest = new Contests();
					contest.setWpId(-1);
				}
				contest.setContestName(contestName);
				contest.setStartDateTime(startDate);
				contest.setPrizeQty(ticketPerWinner);
				contest.setPrizeText(extContestName);
				contest.setPrizeValue(prizeValue);
				contest.setQuestionSize(questionSizes);
				contest.setMaxWinners(maxTicket);
				contest.setIsCustomerStatUpdated(false);
				contest.setIsSponPublic(isPublicSponsor);
				
				if(action.equalsIgnoreCase("SAVE")){
					contest.setCreatedBy(userName);
					contest.setCreatedDate(today);
					contest.setStatus("ACTIVE");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					contest.setUpdatedBy(userName);
					contest.setUpdatedDate(today);
					contest.setStatus("ACTIVE");
				}
				DAORegistry.getContestsDAO().saveOrUpdate(contest);
				
				if(action.equalsIgnoreCase("SAVE")){
					model.addAttribute("msg","Contest Created successfully.");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					model.addAttribute("msg","Contest Updated successfully.");
				}
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<Contests> contestsList = DAORegistry.getContestsDAO().getAllContests(null, filter,statuses);
				
				model.addAttribute("contestsList",contestsList);
				model.addAttribute("contestsPagingInfo",PaginationUtil.getDummyPaginationParameter(contestsList!=null?contestsList.size():0));
				FirestoreUtil.updateUpcomingContestDetails();
				model.addAttribute("sts",1);
				return returnPage;
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				Contests contest =  null;
				if(contestIdStr == null || contestIdStr.isEmpty()){
					System.err.println("Please select any Contest to Delete.");
					model.addAttribute("msg","Contest Updated successfully.");
					return returnPage;
				}
				contest = DAORegistry.getContestsDAO().get(Integer.parseInt(contestIdStr));
				if(contest == null){
					System.err.println("Contest is not found.");
					model.addAttribute("msg","Contest Updated successfully.");
					return returnPage;
				}
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("DELETED");
				DAORegistry.getContestsDAO().update(contest);
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<Contests> contestsList = DAORegistry.getContestsDAO().getAllContests(null, filter,statuses);
				model.addAttribute("msg","Contest Deleted successfully.");
				model.addAttribute("contestsList", contestsList);
				model.addAttribute("contestsPagingInfo", PaginationUtil.getDummyPaginationParameter(contestsList!=null?contestsList.size():0));
				FirestoreUtil.updateUpcomingContestDetails();
				model.addAttribute("sts",1);
				return returnPage;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	
	
	
	
	@RequestMapping(value = "/UpdateSponsor")
	public String updateSponsor(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			String action = request.getParameter("action");
			String sContestId = request.getParameter("sContestId");
			String pdID = request.getParameter("pdId");
			String brandName = request.getParameter("brandName");
			String sponsorId = request.getParameter("sponsorId");
			String userLimitStr = request.getParameter("userLimit");
			String winPrize = request.getParameter("winPrize");
			String finalistPrize = request.getParameter("finalistPrize");
			String spfName = request.getParameter("spfName");
			String splName = request.getParameter("splName");
			String addr = request.getParameter("spAddr");
			String spEmail = request.getParameter("spEmail");
			String spPhone = request.getParameter("spPhone");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			model.addAttribute("sts",0);
			String returnPage =  "";
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				if(pdID == null || pdID.isEmpty()){
					model.addAttribute("msg","Please select any Sponsor to Edit.");
					return returnPage;
				}
				Integer id = Integer.parseInt(pdID);
				PackageDetails pack = DAORegistry.getPackageDetailsDAO().get(id);
				if(pack == null){
					System.err.println("Selected sponsor detail not found.");
					model.addAttribute("msg","Selected sponsor detail not found.");
					return returnPage;
				}
				if(pack.getWpId() !=null){
					System.err.println("Selected sponsor detail auto created and not allowed to update.");
					model.addAttribute("msg","Selected sponsor detail auto created and not allowed to update.");
					return returnPage;
				}
				model.addAttribute("sponsor", pack);
				model.addAttribute("sts",1);
				return returnPage;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				if(brandName==null || brandName.isEmpty()){
					System.err.println("Please provide sponsor Name.");
					model.addAttribute("msg","Please provide sponsor Name.");
					return returnPage;
				}
				if(sContestId==null || sContestId.isEmpty()){
					System.err.println("Please provide contest ID");
					model.addAttribute("msg","Please provide contest ID");
					return returnPage;
				}
				if(sponsorId==null || sponsorId.isEmpty()){
					System.err.println("Please provide sponsor ID");
					model.addAttribute("msg","Please provide sponsor ID");
					return returnPage;
				}
				if(userLimitStr==null || userLimitStr.isEmpty()){
					System.err.println("Please provide Max. user limit.");
					model.addAttribute("msg","Please provide Max. user limit.");
					return returnPage;
				}
				
				if(winPrize==null || winPrize.isEmpty()){
					System.err.println("Please provide Winner prize details.");
					model.addAttribute("msg","Please provide Winner prize details.");
					return returnPage;
				}
				if(spfName==null || spfName.isEmpty()){
					System.err.println("Please provide sponsor first name.");
					model.addAttribute("msg","Please provide sponsor first name.");
					return returnPage;
				}
				if(splName==null || splName.isEmpty()){
					System.err.println("Please provide sponsor last name.");
					model.addAttribute("msg","Please provide sponsor last name.");
					return returnPage;
				}
				if(spEmail==null || spEmail.isEmpty()){
					System.err.println("Please provide sponsor email.");
					model.addAttribute("msg","Please provide sponsor email.");
					return returnPage;
				}
				if(spPhone==null || spPhone.isEmpty()){
					System.err.println("Please provide sponsor phone.");
					model.addAttribute("msg","Please provide sponsor phone.");
					return returnPage;
				}
				
				Integer coId = null;
				Integer userLimit = null;
				try {
					coId = Integer.parseInt(sContestId);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("invalid contest id.");
					model.addAttribute("msg","invalid contest id.");
					return returnPage;
				}
				
				try {
					userLimit = Integer.parseInt(userLimitStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("invalid max. user limit");
					model.addAttribute("msg","invalid max. user limit");
					return returnPage;
				}
				
				PackageDetails pack = null;
				if(action.equalsIgnoreCase("UPDATE")){
					if(pdID == null || pdID.isEmpty()){
						System.err.println("invalid sponsor details to update");
						model.addAttribute("msg","invalid sponsor details to update");
						return returnPage;
					}
					pack = DAORegistry.getPackageDetailsDAO().get(Integer.parseInt(pdID));
					if(pack == null){
						System.err.println("Sponsor details is not found to update.");
						model.addAttribute("msg","Sponsor details is not found to update.");
						return returnPage;
					}
				}
				if(pack == null){
					pack = new PackageDetails();
				}
				Date today = new Date();
				pack.setBrandName(brandName);
				pack.setContestId(coId);
				pack.setEmail(spEmail);
				pack.setFirstName(spfName);
				pack.setLastName(splName);
				pack.setPhone(spPhone);
				pack.setUserLimitCnt(userLimit);
				pack.setWinnerPrize(winPrize);
				pack.setFinelistPrize(finalistPrize);
				pack.setAddr(addr);
				pack.setStatus("ACTIVE");
				
				
				if(action.equalsIgnoreCase("SAVE")){
					pack.setPassCode(sponsorId);
					pack.setIsCancellationEmailSent(false);
					pack.setIsWinnerEmailSent(false);
					pack.setCrDate(today);
					pack.setCrBy(userName);
				}
				if(action.equalsIgnoreCase("UPDATE")){
					if(!pack.getPassCode().equalsIgnoreCase(sponsorId)){
						System.err.println("not allowed to update Sponsor ID");
						model.addAttribute("msg","not allowed to update Sponsor ID");
						return returnPage;
					}
					pack.setUpdDate(today);
					pack.setUpdBy(userName);
				}
				
				DAORegistry.getPackageDetailsDAO().saveOrUpdate(pack);
				
				if(action.equalsIgnoreCase("SAVE")){
					model.addAttribute("msg","Sponsor Created successfully.");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					model.addAttribute("msg","Sponsor Updated successfully.");
				}
				
				Integer count = 0;
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPackageDetailsFilter(null);
				List<PackageDetails> packageList = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(coId,filter);
				if(packageList != null && packageList.size() > 0){
					count = packageList.size();
				}
				model.addAttribute("packageList",packageList);
				model.addAttribute("packagePagingInfo",PaginationUtil.getDummyPaginationParameter(count));
				model.addAttribute("sts",1);
				return returnPage;
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				PackageDetails pack =  null;
				if(pdID == null || pdID.isEmpty()){
					System.err.println("Please select any sponsor to Delete.");
					model.addAttribute("msg","Please select any sponsor to Delete.");
					return returnPage;
				}
				pack = DAORegistry.getPackageDetailsDAO().get(Integer.parseInt(pdID));
				if(pack == null){
					System.err.println("Sponsor details not found.");
					model.addAttribute("msg","Sponsor details not found.");
					return returnPage;
				}
				
				if(pack.getWpId() != null){
					System.err.println("Selected sponsor detail auto created and not allowed to delete.");
					model.addAttribute("msg","Selected sponsor detail auto created and not allowed to delete.");
					return returnPage;
				}
				
				Integer coId = 0;
				try {
					coId = Integer.parseInt(sContestId);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("invalid contest id.");
					model.addAttribute("msg","invalid contest id.");
					return returnPage;
				}
				
				pack.setUpdDate(new Date());
				pack.setUpdBy(userName);
				pack.setStatus("DELETED");
				DAORegistry.getPackageDetailsDAO().update(pack);
				model.addAttribute("msg","Sponsor deleted successfully.");
				Integer count = 0;
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPackageDetailsFilter(null);
				List<PackageDetails> packageList = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(coId,filter);
				if(packageList != null && packageList.size() > 0){
					count = packageList.size();
				}
				model.addAttribute("packageList",packageList);
				model.addAttribute("packagePagingInfo",PaginationUtil.getDummyPaginationParameter(count));
				model.addAttribute("sts",1);
				return returnPage;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/ContestQuestions")
	public String getContestQuestions(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
			
			model.addAttribute("sts",0);
			String returnPage = "";
									
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				model.addAttribute("msg","Not able to identfy contest, pelase send valid contest id to load questions.");
				return returnPage;
			}
			
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				model.addAttribute("msg","Invalid contestId, Please send valid ContestId.");
				return returnPage;
			}
			
			Integer count = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestQuestionFilter(headerFilter);
			
			List<ContestQuestions> questionList = DAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null,contestId);
			
			if(questionList != null && questionList.size() > 0){
				count = questionList.size();
			}
			if(questionList == null || questionList.size() <= 0){
				model.addAttribute("questionList",questionList);
				model.addAttribute("questionPagingInfo",PaginationUtil.getDummyPaginationParameter(count));
				model.addAttribute("msg","No Questions found for selected Contest.");
				return returnPage;
			}
			model.addAttribute("questionList",questionList);
			model.addAttribute("questionPagingInfo",PaginationUtil.getDummyPaginationParameter(count));
			model.addAttribute("sts",1);
			return returnPage;
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdateQuestion")
	public String updateContestQuestion(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			String action = request.getParameter("action");
			String contestIdStr = request.getParameter("qContestId");
			String questionIdStr = request.getParameter("questionId");
			String questionText = request.getParameter("questionText");
			String optionA = request.getParameter("optionA");
			String optionB = request.getParameter("optionB");
			String optionC = request.getParameter("optionC");
			String answer = request.getParameter("answer");
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			model.addAttribute("sts",0);
			String returnPage = "";
			
			
			if(action != null && action.equalsIgnoreCase("EDIT")){
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Edit.");
					model.addAttribute("msg","Please select any Question to Edit.");
					return returnPage;
				}
				
				Integer questionId = null;
				try {
					questionId = Integer.parseInt(questionIdStr);
				} catch (Exception e) {
					System.err.println("Invalid QuestionId found, Please send valid questioId.");
					model.addAttribute("msg","Invalid QuestionId found, Please send valid questioId.");
					return returnPage;
				} 
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestQuestionFilter(null); 
				List<ContestQuestions> questionList = DAORegistry.getContestQuestionsDAO().getContestQuestions(filter, questionId, null);
				if(questionList.size() == 0){
					System.err.println("Selected question is not found in system.");
					model.addAttribute("msg","Selected question is not found in system.");
					return returnPage;
				}
				model.addAttribute("questionList",questionList);
				model.addAttribute("sts",1);
				return returnPage;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				
				if(questionText==null || questionText.isEmpty()){
					System.err.println("Please provide Question Text.");
					model.addAttribute("msg","Please provide Question Text.");
					return returnPage;
				}
				if(optionA==null || optionA.isEmpty()){
					System.err.println("Please provide option A text.");
					model.addAttribute("msg","Please provide option A text.");
					return returnPage;
				}
				if(optionB==null || optionB.isEmpty()){
					System.err.println("Please provide option B text.");
					model.addAttribute("msg","Please provide option B text.");
					return returnPage;
				}
				if(optionC==null || optionC.isEmpty()){
					System.err.println("Please provide option C text.");
					model.addAttribute("msg","Please provide option C text.");
					return returnPage;
				}
				if(answer==null || answer.isEmpty()){
					System.err.println("Please provide answer text.");
					model.addAttribute("msg","Please provide answer text.");
					return returnPage;
				}
				if(contestIdStr==null || contestIdStr.isEmpty()){
					System.err.println("Please provide contestId.");
					model.addAttribute("msg","Please provide contestId.");
					return returnPage;
				}
				Integer contestId = null;
				try {
					contestId = Integer.parseInt(contestIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid contestId.");
					model.addAttribute("msg","Please provide valid contestId.");
					return returnPage;
				}

				Date today = new Date();
				ContestQuestions question = null;
				
				if(action.equalsIgnoreCase("UPDATE")){
					
					if(questionIdStr == null || questionIdStr.isEmpty()){
						System.err.println("Please select any Question to Update.");
						model.addAttribute("msg","Please select any Question to Update.");
						return returnPage;
					}
					question = DAORegistry.getContestQuestionsDAO().get(Integer.parseInt(questionIdStr));
					if(question == null){
						System.err.println("Question is not found.");
						model.addAttribute("msg","Question is not found.");
						return returnPage;
					}
				}
				if(question == null){
					question = new ContestQuestions();
				}
				if(action.equalsIgnoreCase("SAVE")){
					question.setCreatedBy(userName);
					question.setCreatedDate(today);
					question.setSerialNo(1000);
				}
				if(action.equalsIgnoreCase("UPDATE")){
					question.setUpdatedBy(userName);
					question.setUpdatedDate(today);
				}
				
				question.setAnswer(answer);
				question.setContestId(contestId);
				question.setOptionA(optionA);
				question.setOptionB(optionB);
				question.setOptionC(optionC);
				question.setQuestion(questionText);
				
				DAORegistry.getContestQuestionsDAO().saveOrUpdate(question);
				List<ContestQuestions> contestQuestionList = null;
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				contestQuestionList = DAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId);
				if(action.equalsIgnoreCase("SAVE")){
					model.addAttribute("msg","Question Created successfully.");
					if(!contestQuestionList.isEmpty()){
						int i=1;
						for(ContestQuestions q : contestQuestionList){
							q.setSerialNo(i);
							i++;
						}
						DAORegistry.getContestQuestionsDAO().updateAll(contestQuestionList);
					}
				}
				if(action.equalsIgnoreCase("UPDATE")){
					model.addAttribute("msg","Question Updated successfully.");
				}
				model.addAttribute("questionList",contestQuestionList);
				model.addAttribute("questionPagingInfo",PaginationUtil.getDummyPaginationParameter(contestQuestionList!=null?contestQuestionList.size():0));
				model.addAttribute("sts",1);
				return returnPage;
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				ContestQuestions contestQuestion =  null;
				if(questionIdStr == null || questionIdStr.isEmpty()){
					System.err.println("Please select any Question to Delete.");
					model.addAttribute("msg","Please select any Question to Delete.");
					return returnPage;
				}
				if(contestIdStr == null || contestIdStr.isEmpty()){
					System.err.println("ContestId is not found.");
					model.addAttribute("msg","ContestId is not found.");
					return returnPage;
				}
				contestQuestion = DAORegistry.getContestQuestionsDAO().get(Integer.parseInt(questionIdStr));
				if(contestQuestion == null){
					System.err.println("Question is not found.");
					model.addAttribute("msg","Question is not found.");
					return returnPage;
				}
				DAORegistry.getContestQuestionsDAO().delete(contestQuestion);
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<ContestQuestions> list = DAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, Integer.parseInt(contestIdStr));
				if(!list.isEmpty()){
					int i=1;
					for(ContestQuestions q : list){
						q.setSerialNo(i);
						i++;
					}
					DAORegistry.getContestQuestionsDAO().updateAll(list);
				}
				model.addAttribute("msg","Question Deleted successfully.");
				model.addAttribute("questionList",list);
				model.addAttribute("questionPagingInfo",PaginationUtil.getDummyPaginationParameter(list!=null?list.size():0));
				model.addAttribute("sts",1);
				return returnPage;
			}else if(action != null && action.equalsIgnoreCase("ADDNEW")){
				
				String questionBankIdsStr = request.getParameter("questionBankIds");
				Integer contestId = null;
				try {
					contestId = Integer.parseInt(contestIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Please provide valid contestId.");
					model.addAttribute("msg","Please provide valid contestId.");
					return returnPage;
				}
				
				if(questionBankIdsStr == null && questionBankIdsStr.isEmpty()){
					System.err.println("Please select valid Question(s).");
					model.addAttribute("msg","Please select valid Question(s).");
					return returnPage;
				}
				
				/*int usedQuestionCount = DAORegistry.getQuestionBankDAO().getUsedQuestionCount(questionBankIdsStr);
				
				if(usedQuestionCount  > 0 ){
					System.err.println("opps it's looks like somebody already used some of the selected questions , Please refresh grid and try again ");
					model.addAttribute("msg","opps it's looks like somebody already used some of the selected questions , Please refresh grid and try again ");
					return returnPage;
				}*/	
				Contests contest =DAORegistry.getContestsDAO().get(contestId);
				Integer contestQuestionCount =DAORegistry.getContestQuestionsDAO().getQuestionCount(contestId);
				
				Date today = new Date();
				List<ContestQuestions> contestQuestionList = new ArrayList<ContestQuestions>();
				ContestQuestions contestQuestion = null;
				List<QuestionBank> questionBankList = new ArrayList<QuestionBank>();
				QuestionBank questionBank = null;
				
				if(questionBankIdsStr != null && !questionBankIdsStr.isEmpty()){
					String arr[] = questionBankIdsStr.split(",");
					Integer noOfQuestionsCount = arr.length + contestQuestionCount;
					
					if(contest.getQuestionSize() < noOfQuestionsCount){
						System.err.println("Contest has declared question size "+contest.getQuestionSize()+" you can add only "+contest.getQuestionSize()+" questions.");
						model.addAttribute("msg","Contest has declared question size "+contest.getQuestionSize()+" you can add only "+contest.getQuestionSize()+" questions.");
						return returnPage;
					}
					if(arr.length > 0){
						for(String questionBankId : arr){
							questionBank = DAORegistry.getQuestionBankDAO().get(Integer.parseInt(questionBankId));
							
							contestQuestion = DAORegistry.getContestQuestionsDAO().getContestQuestionByQuestion(contestId, questionBank.getQuestion());
							if(contestQuestion == null){
								contestQuestion = new ContestQuestions();
								contestQuestion.setQuestion(questionBank.getQuestion());
								contestQuestion.setOptionA(questionBank.getOptionA());
								contestQuestion.setOptionB(questionBank.getOptionB());
								contestQuestion.setOptionC(questionBank.getOptionC());
								contestQuestion.setAnswer(questionBank.getAnswer());
								contestQuestion.setContestId(contestId);
								contestQuestion.setCreatedBy(userName);
								contestQuestion.setCreatedDate(today);
								
								contestQuestionList.add(contestQuestion);
								
								//Question Bank Status - Update
								questionBank.setStatus("USED");
								questionBank.setUpdatedBy(userName);
								questionBank.setUpdatedDate(today);
								questionBankList.add(questionBank);
							}
						}
						
						DAORegistry.getContestQuestionsDAO().saveAll(contestQuestionList);
						DAORegistry.getQuestionBankDAO().updateAll(questionBankList);
						
						model.addAttribute("msg","Question(s) Created Successfully.");
					}
				}
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
				List<ContestQuestions> newContestQuestionList = DAORegistry.getContestQuestionsDAO().getContestQuestions(filter, null, contestId);
				if(!newContestQuestionList.isEmpty()){
					int i=1;
					for(ContestQuestions q : newContestQuestionList){
						q.setSerialNo(i);
						i++;
					}
					DAORegistry.getContestQuestionsDAO().updateAll(newContestQuestionList);
				}
				model.addAttribute("questionList",newContestQuestionList);
				model.addAttribute("questionPagingInfo",PaginationUtil.getDummyPaginationParameter(newContestQuestionList!=null?newContestQuestionList.size():0));
				model.addAttribute("sts",1);
				return returnPage;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/NextContestQuestionFireBase")
	public String startContestFireBase(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestIdStr = request.getParameter("runningContestId");
			String questionNoStr = request.getParameter("runningQuestionNo");
			String type = request.getParameter("type");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			model.addAttribute("sts",0);
			String returnPage = "";
			
			if(contestIdStr==null || contestIdStr.isEmpty()){
				System.err.println("Contest not found.");
				model.addAttribute("msg","Contest not found.");
				return returnPage;
			}
			if(questionNoStr==null || questionNoStr.isEmpty()){
				System.err.println("Contest question is not found.");
				model.addAttribute("msg","Contest question is not found.");
				return returnPage;
			}
			if(type==null || type.isEmpty()){
				System.err.println("Question or Answer type is not found.");
				model.addAttribute("msg","Question or Answer type is not found.");
				return returnPage;
			}
			Integer contestId = null;
			Integer questionNo = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				System.err.println("ContestId is invalid.");
				model.addAttribute("msg","ContestId is invalid.");
				return returnPage;
			}
			try {
				questionNo = Integer.parseInt(questionNoStr);
			} catch (Exception e) {
				System.err.println("Question No is invalid.");
				model.addAttribute("msg","Question No is invalid.");
				return returnPage;
			}
			ContestQuestions question = null;
			List<ContestQuestions> questionList = new ArrayList<ContestQuestions>();
			ContestQuestions nextQuestion = null;
			Contests contest =DAORegistry.getContestsDAO().get(contestId);
			model.addAttribute("contest",contest);
			
			if(type.equals("QUESTION")){
				questionNo++;
				question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				if(question==null){
					question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo-1);
					contest.setStatus("EXPIRED");
					contest.setLastQuestionNo(question.getSerialNo());
					contest.setLastAction("END");
					contest.setActualEndTime(new Date());
					DAORegistry.getContestsDAO().update(contest);
					
					question.setAnswer("END");
					FirestoreUtil.endContest(model,question.getAnswer());
					FirestoreUtil.updateUpcomingContestDetails();
					
					//DATA MIGRATION CODE
					List<ContestQuestions> questions = DAORegistry.getContestQuestionsDAO().getContestQuestions(new GridHeaderFilters(), null, contestId);
					Map<Integer, String> ansMap = new HashMap<Integer, String>();
					for(ContestQuestions q : questions){
						ansMap.put(q.getSerialNo(), q.getAnswer());
					}
					
					
					FirestoreDataMigrationThread migration = new FirestoreDataMigrationThread(contest, ansMap);
					Thread thread = new Thread(migration);
					thread.start();
					//FirestoreUtil.migrateParticipantsData(contest);
					
					questionList.add(question);
					model.addAttribute("sts",1);
					model.addAttribute("question",question);
					model.addAttribute("msg","Contest is completed.");
					return returnPage;
				}else{
					contest.setLastQuestionNo(question.getSerialNo());
					contest.setLastAction("QUESTION");
					DAORegistry.getContestsDAO().update(contest);
				}
				question.setCorrectAnswer(question.getAnswer());
				question.setAnswer("Z");
				
				//FirestoreUtil.clearAnswerCounts(contest.getQuestionSize());
				FirestoreUtil.updateNextQuestion(question, model);
				FirestoreUtil.updateContestDetailsQNo(question, model);
				FirestoreUtil.updateHostTracker("QUEFIRED");
				model.addAttribute("sts",1);
				model.addAttribute("question",question);
				return returnPage;
			}else if(type.equals("ANSWER") || type.equals("COUNT")){
				if(questionNo == contest.getQuestionSize() || questionNo.equals(contest.getQuestionSize())){
					
				}
				question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				if(type.equals("ANSWER")){
					nextQuestion =  DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo+1);
				}
				question.setCorrectAnswer(question.getAnswer());
				contest.setLastQuestionNo(question.getSerialNo());
				contest.setLastAction(type);
				DAORegistry.getContestsDAO().update(contest);
				
				if(type.equalsIgnoreCase("ANSWER")){
					FirestoreUtil.updateNextQuestion(question, model);
					FirestoreUtil.updateNextQuestionHost(nextQuestion, model);
				}else{
					FirestoreUtil.updateHostTracker("COUNT");
				}
				model.addAttribute("sts",1);
				model.addAttribute("question",question);
				model.addAttribute("nextQuestion",nextQuestion);
				return returnPage;
			}else if(type.equals("SUMMARY") || type.equals("LOTTERY") ){
				question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				contest.setLastQuestionNo(question.getSerialNo());
				contest.setLastAction(type);
				DAORegistry.getContestsDAO().update(contest);
				question.setAnswer(type);
				
				FirestoreUtil.updateNextQuestion(question, model);
				if(type.equals("LOTTERY")){
					FirestoreUtil.updateHostTracker("LOTTERY");
				}else{
					FirestoreUtil.updateHostTracker("SUMMARY");
				}
				
				model.addAttribute("sts",1);
				model.addAttribute("question",question);
				model.addAttribute("nextQuestion",nextQuestion);
				return returnPage;
			}else if(type.equals("WINNER") || type.equals("WINNERS")){
				question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
				contest.setLastQuestionNo(question.getSerialNo());
				contest.setLastAction(type);
				DAORegistry.getContestsDAO().update(contest);
				question.setAnswer(type);
				
				if(type.equals("WINNER")){
					Map<String, List<ContestGrandWinners>> winMap = FirestoreUtil.getGrandWinnerMap(contest.getId());
					if(winMap==null || winMap.isEmpty()){
						System.err.println("Something went wrong while getting grand winners.");
						model.addAttribute("msg","Something weent wrong while gettting grand winners.");
						return returnPage;
					}
					FirestoreUtil.setGrandWinners(winMap);
					FirestoreUtil.updateNextQuestion(question, model);
					FirestoreUtil.updateHostTracker("WINFIRED");
					
				}else if( type.equals("WINNERS")){
					Boolean isWinner =  FirestoreUtil.declareGrandWinners(contest);
					if(!isWinner){
						System.err.println("Something weent wrong while calculating grand winners.");
						model.addAttribute("msg","Something weent wrong while calculating grand winners.");
						return returnPage;
					}
					FirestoreUtil.updateHostTracker("WINNER");
				}
				
				model.addAttribute("sts",1);
				model.addAttribute("question",question);
				model.addAttribute("nextQuestion",nextQuestion);
				return returnPage;
			}else if(type != null && type.equals("RESUME")){
				if(contest.getLastAction().equalsIgnoreCase("ANSWER") || contest.getLastAction().equalsIgnoreCase("STARTED")){
					ContestQuestions lastQuestion = DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					questionNo++;
					question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					if(question==null){
						question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo-1);
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("SUMMARY");
						DAORegistry.getContestsDAO().update(contest);
						question.setAnswer("SUMMARY");
						
						FirestoreUtil.updateNextQuestion(question, model);
						FirestoreUtil.updateHostTracker("SUMMARY");
						//FirestoreUtil.setSummaryWinnersHost(summaryWinners, summaryInfo.getwCount(), model);
						model.addAttribute("sts",1);
						model.addAttribute("question",question);
						model.addAttribute("nextQuestion",nextQuestion);
						return returnPage;
					}else{
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("QUESTION");
						DAORegistry.getContestsDAO().update(contest);
					}
					question.setCorrectAnswer(question.getAnswer());
					question.setAnswer("Z");
					//FirestoreUtil.clearAnswerCounts();
					FirestoreUtil.updateNextQuestion(question, model);
					FirestoreUtil.updateContestDetailsQNo(question, model);
					FirestoreUtil.updateHostTracker("QUEFIRED");
					model.addAttribute("sts",1);
					model.addAttribute("question",question);
					return returnPage;
				}else if(contest.getLastAction().equalsIgnoreCase("QUESTION") || contest.getLastAction().equalsIgnoreCase("COUNT")){
					question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					if(contest.getLastAction().equalsIgnoreCase("COUNT")){
						nextQuestion = DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo+1);
						contest.setLastAction("ANSWER");
					}else{
						contest.setLastAction("COUNT");
					}
					question.setCorrectAnswer(question.getAnswer());
					contest.setLastQuestionNo(question.getSerialNo());
					DAORegistry.getContestsDAO().update(contest);
					
					if(contest.getLastAction().equalsIgnoreCase("ANSWER")){
						FirestoreUtil.updateNextQuestion(question, model);
						FirestoreUtil.updateNextQuestionHost(nextQuestion, model);
					}else{
						//FirestoreUtil.setAnswerCountHost(userCount, model);
						FirestoreUtil.updateHostTracker("COUNT");
					}
					model.addAttribute("sts",1);
					model.addAttribute("question",question);
					model.addAttribute("nextQuestion",nextQuestion);
					return returnPage;
				}else if(contest.getLastAction().equalsIgnoreCase("SUMMARY") || contest.getLastAction().equalsIgnoreCase("LOTTERY")
						|| contest.getLastAction().equalsIgnoreCase("WINNER") || contest.getLastAction().equalsIgnoreCase("WINNERS")){
					question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo);
					if(contest.getLastAction().equalsIgnoreCase("SUMMARY")){
						question.setAnswer("LOTTERY");
						contest.setLastAction("LOTTERY");
						
						contest.setLastQuestionNo(question.getSerialNo());
						DAORegistry.getContestsDAO().update(contest);
						
						FirestoreUtil.updateNextQuestion(question, model);
						FirestoreUtil.updateHostTracker("LOTTERY");
						
						model.addAttribute("sts",1);
						model.addAttribute("question",question);
						model.addAttribute("nextQuestion",nextQuestion);
						return returnPage;
					}else if(contest.getLastAction().equalsIgnoreCase("LOTTERY")){
						question.setAnswer("WINNERS");
						contest.setLastAction("WINNERS");
						contest.setLastQuestionNo(question.getSerialNo());
						DAORegistry.getContestsDAO().update(contest);

						Boolean isWinner =  FirestoreUtil.declareGrandWinners(contest);
						if(!isWinner){
							System.err.println("Something went wrong while calculating grand winners.");
							model.addAttribute("msg","Something went wrong while calculating grand winners.");
							return returnPage;
						}
						FirestoreUtil.updateHostTracker("WINNER");
						model.addAttribute("sts",1);
						model.addAttribute("question",question);
						model.addAttribute("nextQuestion",nextQuestion);
						return returnPage;
					}else if(contest.getLastAction().equalsIgnoreCase("WINNERS")){
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("WINNER");
						DAORegistry.getContestsDAO().update(contest);
						question.setAnswer("WINNER");
						
						Map<String, List<ContestGrandWinners>> winMap = FirestoreUtil.getGrandWinnerMap(contest.getId());
						if(winMap==null || winMap.isEmpty()){
							System.err.println("Something went wrong while getting grand winners.");
							model.addAttribute("msg","Something went wrong while gettting grand winners.");
							return returnPage;
						}
						FirestoreUtil.setGrandWinners(winMap);
						FirestoreUtil.updateNextQuestion(question, model);
						FirestoreUtil.updateHostTracker("WINFIRED");
						
						model.addAttribute("sts",1);
						model.addAttribute("question",question);
						model.addAttribute("nextQuestion",nextQuestion);
						return returnPage;
					}else if(contest.getLastAction().equalsIgnoreCase("WINNER")){
						contest.setStatus("EXPIRED");
						contest.setLastQuestionNo(question.getSerialNo());
						contest.setLastAction("END");
						contest.setActualEndTime(new Date());
						DAORegistry.getContestsDAO().update(contest);
						question.setAnswer("END");
						
						FirestoreUtil.endContest(model,question.getAnswer());
						FirestoreUtil.updateUpcomingContestDetails();
						
						
						//DATA MIGRATION CODE
						List<ContestQuestions> questions = DAORegistry.getContestQuestionsDAO().getContestQuestions(new GridHeaderFilters(), null, contestId);
						Map<Integer, String> ansMap = new HashMap<Integer, String>();
						for(ContestQuestions q : questions){
							ansMap.put(q.getSerialNo(), q.getAnswer());
						}
						FirestoreDataMigrationThread migration = new FirestoreDataMigrationThread(contest, ansMap);
						Thread thread = new Thread(migration);
						thread.start();
						//FirestoreUtil.migrateParticipantsData(contest);
						
						questionList.add(question);
						model.addAttribute("sts",1);
						model.addAttribute("question",question);
						model.addAttribute("msg","Contest is completed.");
						return returnPage;
					}
				}
			}else{
				List<Contests> contests = DAORegistry.getContestsDAO().getAllStartedContests();
				if(contests.size() > 0){
					System.err.println("There is already one running contest, Not allowed to run mutiple contest at same time.");
					model.addAttribute("msg","There is already one running contest, Not allowed to run mutiple contest at same time.");
					return returnPage;
				}
				if(contest.getStatus().equalsIgnoreCase("ACTIVE")){
					contest.setStatus("STARTED");
					contest.setLastQuestionNo(0);
					contest.setLastAction("STARTED");
					contest.setActualStartTime(new Date());
					DAORegistry.getContestsDAO().update(contest);
					
					question= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, questionNo-1);
					nextQuestion= DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contestId, 1);
					
				}else{
					System.err.println("Contest is Already started by someone, not allowed to start multiple time.");
					model.addAttribute("msg","Contest is Already started by someone, not allowed to start multiple time.");
					return returnPage;
				}
				question = new ContestQuestions();
				question.setSerialNo(0);
				question.setAnswer("PLAY");
				question.setOptionA("");
				question.setOptionB("");
				question.setOptionC("");
				question.setQuestion("");
				question.setContestId(contestId);
				
				question.setAnswer("PLAY");
				FirestoreUtil.setContestStarted(true);
				FirestoreUtil.updateNextQuestion(question, model);
				FirestoreUtil.setContestDetails(contest, model);
				FirestoreUtil.updateUpcomingContestDetails();
				FirestoreUtil.updateNextQuestionHost(nextQuestion, model);
				model.addAttribute("contestMsg", "Contest is started, Press Show Question-1 button to start broadcasting questions.");
				model.addAttribute("sts",1);
				model.addAttribute("question",question);
				return returnPage;
			
			}
			
			model.addAttribute("msg", "Something went wrong, Please try again.");
			model.addAttribute("sts",0);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "Something went wrong, Please try again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/ResetContest")
	public String resetContest(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestId = request.getParameter("contestId");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			model.addAttribute("sts", 0);
			
			if(contestId== null || contestId.isEmpty()){
				System.err.println("Please send Contest Id to reset.");
				model.addAttribute("msg", "Please send Contest Id to reset.");
				return "";
			}
			if(action== null || action.isEmpty()){
				System.err.println("Please send valid Action RESET OR END.");
				model.addAttribute("msg", "Please send valid Action RESET OR END.");
				return "";
			}
			Contests contest = DAORegistry.getContestsDAO().get(Integer.parseInt(contestId));
			if(contest == null){
				System.err.println("Contest not found in system with given id.");
				model.addAttribute("msg", "Contest not found in system with given id.");
				return "";
			}
			if(action.equalsIgnoreCase("RESET")){
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("ACTIVE");
				contest.setLastAction(null);
				contest.setLastQuestionNo(null);
				contest.setIsCustomerStatUpdated(false);
				
				DAORegistry.getContestsDAO().update(contest);
				
				FirestoreUtil.clearAnswerCounts(15);
				FirestoreUtil.endContest(model, "");
				FirestoreUtil.updateUpcomingContestDetails();
				FirestoreUtil.cleanParticipantData();
				model.addAttribute("msg", "Contest data reseted.");
				model.addAttribute("sts", 1);
				return "";
			}else if(action.equalsIgnoreCase("END")){
				Boolean isEndedSuccessfully = false;
				if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("WINNER")){
					isEndedSuccessfully = true;
				}
				
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("EXPIRED");
				contest.setLastQuestionNo(contest.getQuestionSize());
				contest.setLastAction("END");
				contest.setIsCustomerStatUpdated(false);
				DAORegistry.getContestsDAO().update(contest);
				FirestoreUtil.endContest(model,"END");
				FirestoreUtil.updateUpcomingContestDetails();
				model.addAttribute("msg", "Contest Ended successfully.");
				model.addAttribute("sts", 1);
				return "";
			}else if(action.equalsIgnoreCase("ACTIVE")){
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				contest.setStatus("ACTIVE");
				contest.setIsCustomerStatUpdated(false);
				DAORegistry.getContestsDAO().update(contest);
				FirestoreUtil.updateUpcomingContestDetails();
				model.addAttribute("msg", "Contest move to Active status successfully.");
				model.addAttribute("sts", 1);
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/HostPage")
	public String loadContestHostPage(HttpServletRequest request, HttpServletResponse response,Model model){
		String returnPage = "page-contests-firebase-mobile-host-contest";
		try {
			model.addAttribute("sts", 0);
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR, 00);
			today.set(Calendar.MINUTE, 00);
			today.set(Calendar.SECOND, 00);
			Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'ACTIVE','STARTED'");
			if(contest== null){
				System.err.println("There is no scheduled contest for today.");
				model.addAttribute("msg","There is no scheduled contest for today.");
				return returnPage;
			}
			if(contest.getStatus().equalsIgnoreCase("STARTED")){
				if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("ANSWER")){
					Map<String, String> qMap = Util.getParameterMap(request);
					qMap.put("contestId", contest.getId().toString());
					qMap.put("questionNo",contest.getLastQuestionNo().toString());
					
					ContestQuestions question = DAORegistry.getContestQuestionsDAO().getContestQuestionsByNo(contest.getId(), contest.getLastQuestionNo());
					if(question == null){
						model.addAttribute("buttonText","NONE");
						System.err.println("Contest last state is not found, Resume contest is not allowed.");
						model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
						return returnPage;
					}
					
					String buttoneText = null;
					buttoneText = Util.getResumeContestText(contest,question);
					model.addAttribute("buttonText",buttoneText);
					if(buttoneText.equalsIgnoreCase("NONE")){
						model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
					}
					model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
				}else{
					model.addAttribute("buttonText",Util.getResumeContestText(contest,null));
					model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
				}
			}else{
				model.addAttribute("buttonText","Start Contest");
				model.addAttribute("lastQuestionNo",0);
			}
		
			model.addAttribute("contest",contest);
			model.addAttribute("status","TODAY");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return returnPage;
	
	}
	
	
	
	
	@RequestMapping(value = "/ContestParticipantBrands")
	public String getContestSubscriberBrands(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
			
			model.addAttribute("sts",0);
			String returnPage = "";
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				model.addAttribute("msg","Not able to identfy contest, pelase send valid contest id to load participant sponsors.");
				return returnPage;
			}
			
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				model.addAttribute("msg","Invalid contestId, Please send valid ContestId.");
				return returnPage;
			}
			
			
			Integer count = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestPackageDetailsFilter(headerFilter);
			List<PackageDetails> packageList = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(contestId,filter);
			if(packageList != null && packageList.size() > 0){
				count = packageList.size();
			}
			if(packageList == null || packageList.size() <= 0){
				model.addAttribute("packagePagingInfo",PaginationUtil.getDummyPaginationParameter(count));
				model.addAttribute("msg","No participants sponsors found for selected Contest.");
				return returnPage;
			}
			model.addAttribute("packageList",packageList);
			model.addAttribute("packagePagingInfo",PaginationUtil.getDummyPaginationParameter(count));
			model.addAttribute("sts",1);
			return returnPage;
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/ActivatePasscodes")
	public String activatePasscodes(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			
			String contestIdStr = request.getParameter("contestId");
			model.addAttribute("sts",0);
			String returnPage = "";
			Date currentTime = new Date();
			
			if(contestIdStr == null || contestIdStr.isEmpty()){
				model.addAttribute("msg","Invalid contest ID.");
				return returnPage;  
			}
			
			Integer coId = 0;
			try {
				coId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				model.addAttribute("msg","Invalid contest ID.");
				return returnPage;  
			}
			
			List<Contests> contests = DAORegistry.getContestsDAO().getAllContests(null, new GridHeaderFilters(), "'ACTIVE'");
			if(contests.isEmpty()){
				model.addAttribute("msg","Contest not found in system.");
				return returnPage;  
			}
			Contests contest = contests.get(0);
			if(contest == null){
				model.addAttribute("msg","Only Todays dated contest sponsor IDs can be activated.");
				return returnPage;  
			}
			if(coId > contest.getId() || coId < contest.getId() ){
				model.addAttribute("msg","Only first upcoming contest sponsor IDs can be activated.");
				return returnPage;  
			}
			
			if(contest.getStatus().equalsIgnoreCase("STARTED")){
				model.addAttribute("msg","Not Allowed to activate sponsor IDs after contest is already started.");
				return returnPage;  
			}
			
			if(currentTime.after(contest.getStartDateTime())){
				model.addAttribute("msg","Not Allowed to activate sponsor IDs for past dated/timed contest (Contest Name : "+contest.getContestName()+" | Start DateTime :  "+contest.getStartDateTimeStr()+")");
				return returnPage; 
			}
			
			List<PackageDetails> packages = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(contest.getId(), new GridHeaderFilters());
			if(packages == null || packages.isEmpty()){
				model.addAttribute("msg","No sponsor IDs found for contest (Contest Name : "+contest.getContestName()+" | Start DateTime :  "+contest.getStartDateTimeStr()+")");
				return returnPage; 
			}
			Boolean isUploaded = FirestoreUtil.uploadPasscodetoFirestore(packages);
			if(isUploaded){
				model.addAttribute("msg","All sponsor IDs are activated for Contest : "+contest.getContestName()+" | Start DateTime :  "+contest.getStartDateTimeStr());
				model.addAttribute("sts",1);
				return returnPage;
			}else{
				model.addAttribute("msg","Something went wrong while activating sponsor IDs.");
				return returnPage; 
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/ContestWinners")
	public String getAllContestWinners(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String headerFilter = request.getParameter("headerFilter");
			String contestIdStr = request.getParameter("contestId");
			
			model.addAttribute("sts",0);
			String returnPage = "";
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				model.addAttribute("msg","Not able to identfy contest, pelase send valid contest id to load winners.");
				return returnPage;
			}
			
			Integer contestId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				model.addAttribute("msg","Invalid contestId, Please send valid ContestId.");
				return returnPage;
			}
			
			
			Integer count = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestWinnersFilter(headerFilter);
			List<ContestGrandWinners> winners =  DAORegistry.getContestGrandWinnersDAO().getGrandWinnersByContestId(contestId,filter);
			if(winners == null || winners.size() <= 0){
				model.addAttribute("winnerList",winners);
				model.addAttribute("winnerPagingInfo",PaginationUtil.getDummyPaginationParameter(count));
				model.addAttribute("msg","No winners found for selected Contest.");
				return returnPage;
			}
			model.addAttribute("winnerList",winners);
			model.addAttribute("winnerPagingInfo",PaginationUtil.getDummyPaginationParameter(count));
			model.addAttribute("sts",1);
			return returnPage;
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/GetWinnersExcel")
	public void getWinnersExcel(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestIdStr = request.getParameter("contestId");
			String brandIdStr = request.getParameter("brandId");
			
			String msg = "";
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				msg = "Not able to identfy contest, pelase send valid contest id to load winners.";
			}
			
			Integer contestId = null;
			Integer brandId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				msg = "Invalid contestId, Please send valid ContestId.";
			}
			
			try {
				brandId = Integer.parseInt(brandIdStr);
			} catch (Exception e) {
				msg ="Invalid sponsor ID";
			}
			
			int startRowCnt = 0;
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("Contest Winners");
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Brand Name");
			headerList.add("User ID");
			headerList.add("Email");
			headerList.add("Phone");
			headerList.add("Date Of Birth");
			headerList.add("Zipcode");
			headerList.add("Prize");
			headerList.add("Created Date");
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);
			
			
			List<ContestGrandWinners> winners =  DAORegistry.getContestGrandWinnersDAO().getGrandWinnersByContestIdAndBrandId(contestId,brandId);
			if(winners == null || winners.isEmpty()){
				msg = "No winners data found";
			}
			
			if(msg != null && !msg.isEmpty()){
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt++, msg);
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition","attachment; filename=Contest_Winners.xlsx"); 
				workbook.write(response.getOutputStream());
				//workbook.close();
			}
			
			
			int j = 0;
			int rowCount = startRowCnt + 1;
			for(ContestGrandWinners win : winners){
				Row rowhead = ssSheet.createRow((int) rowCount);
				rowCount++;
				j = 0;
				ExcelUtil.getExcelStringCell(win.getBrandName(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(win.getUserId(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(win.getEmail(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(win.getPhone(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(win.getBirthDate(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(win.getZipcode(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(win.getPrizeText(), j, rowhead);
				j++;
				ExcelUtil.getExcelDateCell(win.getCrDateStr(), j, rowhead,cellStyleWithHourMinute);
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename=Contest_Winners.xlsx"); 
			workbook.write(response.getOutputStream());
			//workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
	}
	
	
	
	
	@RequestMapping(value = "/GetContestParticipantExcel")
	public void getContestParticipantExcel(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestIdStr = request.getParameter("contestId");
			String brandIdStr = request.getParameter("brandId");
			
			String msg = "";
			if(contestIdStr == null || contestIdStr.trim().isEmpty()){
				msg ="Invalid contest ID";
			}
			
			Integer contestId = null;
			Integer brandId = null;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				msg ="Invalid contest ID";
			}
			
			try {
				brandId = Integer.parseInt(brandIdStr);
			} catch (Exception e) {
				msg ="Invalid sponsor ID";
			}
			
			
			int startRowCnt = 0;
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("Contest Participants");
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Brand Name");
			headerList.add("User ID");
			headerList.add("Email");
			headerList.add("Phone");
			headerList.add("Date Of Birth");
			headerList.add("Zipcode");
			headerList.add("Game Played?");
			headerList.add("Created Date");
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);
			
			
			List<ContestParticipants> participants =  DAORegistry.getContestParticipantsDAO().getContestParticipantsByContestAndBrandId(contestId, brandId);
			if(participants == null || participants.isEmpty()){
				msg ="No participants records found.";
			}
			
			if(msg != null && !msg.isEmpty()){
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt++, msg);
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition","attachment; filename=Contest_Participants.xlsx"); 
				workbook.write(response.getOutputStream());
				//workbook.close();
			}
			
			int j = 0;
			int rowCount = startRowCnt + 1;
			for(ContestParticipants cp : participants){
				Row rowhead = ssSheet.createRow((int) rowCount);
				rowCount++;
				j = 0;
				ExcelUtil.getExcelStringCell(cp.getBrandName(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(cp.getUserId(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(cp.getEmail(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(cp.getPhone(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(cp.getBirthDate(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(cp.getZipcode(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(cp.getIsJoined()?"YES":"NO", j, rowhead);
				j++;
				ExcelUtil.getExcelDateCell(cp.getCrDateStr(), j, rowhead,cellStyleWithHourMinute);
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename=Contest_Participants.xlsx"); 
			workbook.write(response.getOutputStream());
			//workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
	}
	
	
	
	
	@RequestMapping(value = "/LoadTestFirestore")
	public String loadTestFirestore(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String doc = request.getParameter("doc");
			String qNo = request.getParameter("qNo");
			String action = request.getParameter("action");
			Boolean flag = false;
			if(action.equals("PUSH")){
				//flag = FirestoreUtil.testAnswerLoad(doc, Integer.parseInt(qNo));
			}else if(action.equals("CLEAR")){
				model.addAttribute("failed", FirestoreUtil.failedReqCnt);
				System.out.println("FAILED CNT : "+FirestoreUtil.failedReqCnt);
				FirestoreUtil.clearAnswerCounts(Integer.parseInt(qNo));
				FirestoreUtil.failedReqCnt = 0;
				flag = true;
			}else if(action.equals("PARTI")){
				flag = FirestoreUtil.testParticipantLoad(doc);
			}else if(action.equals("HELLO")){
				System.out.println("HELLO!!");
			}
			model.addAttribute("msg", flag);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/ClearFirestore")
	public String clearFirestoreData(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'STARTED'");
			if(contest != null){
				model.addAttribute("msg", "Contest is running cannot clear data.");
			}
			FirestoreUtil.cleanParticipantData();
			FirestoreUtil.clearAnswerCounts(15);
			FirestoreUtil.clearWinnersData();
			model.addAttribute("msg", "All Data Removed from firestore.");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/MigratedFirestoreData")
	public String migratedFirestoreData(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String coId = request.getParameter("coId");
			String action = request.getParameter("action");
			Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'STARTED'");
			if(contest != null){
				model.addAttribute("msg", "Contest is running cannot migrate data.");
				return "";
			}
			contest =DAORegistry.getContestsDAO().get(Integer.parseInt(coId));
			if(contest == null){
				model.addAttribute("msg","Contest not found with given ID");
				return "";
				
			}
			
			List<ContestQuestions> questions = DAORegistry.getContestQuestionsDAO().getContestQuestions(new GridHeaderFilters(), null, Integer.parseInt(coId));
			Map<Integer, String> ansMap = new HashMap<Integer, String>();
			for(ContestQuestions q : questions){
				ansMap.put(q.getSerialNo(), q.getAnswer());
			}
			String msg = "";
			if(action.equalsIgnoreCase("PART")){
				FirestoreUtil.migrateParticipantsData(contest);
				msg = "Participants Data Migrated from firestore.";
			}else if(action.equalsIgnoreCase("ANS")){
				FirestoreUtil.migrateCustomerAnswerData(contest, ansMap);
				msg = "Customer Answers Data Migrated from firestore.";
			}else if(action.equalsIgnoreCase("WIN")){
				FirestoreUtil.migrateWinnersData(contest);
				msg = "WInners Data Migrated from firestore.";
			}
			model.addAttribute("msg",msg);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/ContestSetting")
	public String contestSetting(HttpServletRequest request, HttpServletResponse response,Model model){
		String returnPage = "page-contests-setting";
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return returnPage;
	}
	
	
	@RequestMapping(value = "/SendNotificationNow")
	public String sendNotificationNow(HttpServletRequest request, HttpServletResponse response,Model model){
		String returnPage = "";
		try {
			String noTitle = request.getParameter("noTitle");
			String noText = request.getParameter("noText");
			model.addAttribute("sts",0);
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			if(noTitle == null || noTitle.isEmpty()){
				model.addAttribute("msg","Notification title is mendatory.");
				return "";
			}
			if(noText == null || noText.isEmpty()){
				model.addAttribute("msg","Notification text is mendatory.");
				return "";
			}
			
			List<String> tokens = FirestoreUtil.getNotifictionTokens();
			if(tokens==null || tokens.isEmpty()){
				model.addAttribute("msg", "No authenticated users are found to send notification.");
				return returnPage;
			}
			Boolean isSent = false;
			try {
				isSent = Notification.sendNotificationNow(tokens, noText, noTitle);
				if(isSent){
					model.addAttribute("sts",1);
					model.addAttribute("msg", "Notification sent to "+tokens.size()+" users.");
				}else{
					model.addAttribute("msg", "Error occured with sending notification.");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				model.addAttribute("msg", "Error occured while sending notifications.");
				return returnPage;
			}
			System.out.println("NOTIFICATION SENT BY : "+userName+"   |   USERS : "+tokens.size());
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "Error occured with sending notification.");
		}
		return returnPage;
	}
	
	
	
	
	
	@RequestMapping({"/GetfinalistExcel"})
	public void downloadContestWinnersReport(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String coIdStr = request.getParameter("contestId");
			String brandId = request.getParameter("brandId");
			
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("User ID");
			headerList.add("Email");
			headerList.add("Phone");
			headerList.add("Date Of Birth");
			headerList.add("Zipcode");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<ContestWinner> winners = DAORegistry.getContestsDAO().getContestWinners(Integer.parseInt(coIdStr),Integer.parseInt(brandId));
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int rowCount = 1;
			int j=0;
			
			for(ContestWinner win : winners){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(win.getUserId() != null){
					rowhead.createCell((int) j).setCellValue(win.getUserId());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(win.getEmail() != null){
					rowhead.createCell((int) j).setCellValue(win.getEmail());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
								
				if(win.getPhone() != null){
					rowhead.createCell((int) j).setCellValue(win.getPhone());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(win.getBirthDate()!= null){
					rowhead.createCell((int) j).setCellValue(win.getBirthDate());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(win.getZipcode() != null){
					rowhead.createCell((int) j).setCellValue(win.getZipcode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename=contest_finalist_winners.xlsx");
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	/*@RequestMapping({"/DownloadContestGrandWinnersReport"})
	public void downloadContestGrandWinnersReport(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String coIdStr = request.getParameter("coId");
			String passcode = request.getParameter("passcode");
			
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("User ID");
			headerList.add("Email");
			headerList.add("Phone");
			headerList.add("Date Of Birth");
			headerList.add("Zipcode");
			headerList.add("Grand Prize");
			//headerList.add("Grand Prize Qty.");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<ContestGrandWinners> winners = DAORegistry.getContestsDAO().getContestGrandWinners(Integer.parseInt(coIdStr),passcode);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int rowCount = 1;
			int j=0;
			
			for(ContestGrandWinners win : winners){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(win.getUserId() != null){
					rowhead.createCell((int) j).setCellValue(win.getUserId());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(win.getEmail() != null){
					rowhead.createCell((int) j).setCellValue(win.getEmail());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
								
				if(win.getPhone() != null){
					rowhead.createCell((int) j).setCellValue(win.getPhone());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(win.getBirthDate()!= null){
					rowhead.createCell((int) j).setCellValue(win.getBirthDate());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(win.getZipcode() != null){
					rowhead.createCell((int) j).setCellValue(win.getZipcode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(win.getPrizeText() != null){
					rowhead.createCell((int) j).setCellValue(win.getPrizeText());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(win.getPrizeQty() != null){
					rowhead.createCell((int) j).setCellValue(win.getPrizeQty());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename=contest_grand_winners.xmls");
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	@RequestMapping({"/DownloadContestParticpantsReport"})
	public void downloadContestParticpantsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String coIdStr = request.getParameter("coId");
			String passcode = request.getParameter("passcode");
			
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("User ID");
			headerList.add("Email");
			headerList.add("Phone");
			headerList.add("Date Of Birth");
			headerList.add("Zipcode");
			headerList.add("Game Played?");
			headerList.add("Device Type");
			
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<ContestParticipants> participants = DAORegistry.getContestParticipantsDAO().getContestParticipantsByContest(Integer.parseInt(coIdStr),passcode);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int rowCount = 1;
			int j=0;
			
			for(ContestParticipants parti : participants){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(parti.getUserId() != null){
					rowhead.createCell((int) j).setCellValue(parti.getUserId());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(parti.getEmail() != null){
					rowhead.createCell((int) j).setCellValue(parti.getEmail());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
								
				if(parti.getPhone() != null){
					rowhead.createCell((int) j).setCellValue(parti.getPhone());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(parti.getBirthDate()!= null){
					rowhead.createCell((int) j).setCellValue(parti.getBirthDate());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(parti.getZipcode() != null){
					rowhead.createCell((int) j).setCellValue(parti.getZipcode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(parti.getZipcode() != null){
					rowhead.createCell((int) j).setCellValue(parti.getZipcode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(parti.getIsJoined() != null){
					rowhead.createCell((int) j).setCellValue(parti.getIsJoined().equals(Boolean.TRUE)?"YES":"NO");
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(parti.getDeviceType() != null){
					rowhead.createCell((int) j).setCellValue(parti.getDeviceType());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename=contest_participants.xmls");
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	

}
