package com.rtfbar.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rtfbar.admin.data.TrackerUser;


@Controller
@RequestMapping({"/","/Login","/Dashboard","/Logout"})
public class LoginController {

	@RequestMapping(value = {"/Login"}, method=RequestMethod.GET)
	public String loadHomePage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		String error = request.getParameter("error");
		map.put("error", error);
		TrackerUser trackerUser = (TrackerUser)session.getAttribute("trackerUser");
		if(trackerUser != null ){
			session.setAttribute("trackerUser", null);
		}
		return "page-login";
	}
	
	@RequestMapping(value = {"/Logout"}, method=RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		String error = request.getParameter("error");
		map.put("error", error);
		session.setAttribute("trackerUser", null);
		return "page-login";
	}
	
	
	@RequestMapping({"/","/Dashboard"})
	public String loadDashboardPage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){		
		TrackerUser trackerUser = (TrackerUser)session.getAttribute("trackerUser");
		if(trackerUser == null ){
			session.setAttribute("trackerUser", null);
			return "page-login";
		}
		return "redirect:Contest?status=ALL";
	}
}
