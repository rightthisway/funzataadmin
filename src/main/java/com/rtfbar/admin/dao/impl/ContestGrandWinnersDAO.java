package com.rtfbar.admin.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfbar.admin.data.ContestGrandWinners;
import com.rtfbar.admin.utils.GridHeaderFilters;

public class ContestGrandWinnersDAO extends HibernateDAO<Integer, ContestGrandWinners> implements com.rtfbar.admin.dao.services.ContestGrandWinnersDAO{

	private static Session contestSession = null;

	public static Session getContestSession() {
		return ContestGrandWinnersDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		ContestGrandWinnersDAO.contestSession = contestSession;
	}
	
	
	@Override
	public List<ContestGrandWinners> getGrandWinnersByContestId(Integer coId,GridHeaderFilters filter) {
		List<ContestGrandWinners> winners = null;
		try {
			String sql ="select * from contest_grand_winners WHERE contestId="+coId;
			
			if(filter.getUserId()!=null && !filter.getUserId().isEmpty()){
				sql += " AND userId like '%"+filter.getUserId()+"%'";
			}
			if(filter.getEmail()!=null && !filter.getEmail().isEmpty()){
				sql += " AND email like '%"+filter.getEmail()+"%'";
			}
			if(filter.getPhone()!=null && !filter.getPhone().isEmpty()){
				sql += " AND phone like '%"+filter.getPhone()+"%'";
			}
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND barcode like '%"+filter.getText1()+"%'";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND prizeText like '%"+filter.getText2()+"%'";
			}
			
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				if(filter.getText3().equalsIgnoreCase("YES")){
					sql += " AND isWinnerEmailSent = 1";
				}else if(filter.getText3().equalsIgnoreCase("YES")){
					sql += " AND isWinnerEmailSent = 0";
				}
			}
			if(filter.getText4()!=null && !filter.getText4().isEmpty()){
				sql += " AND brandName like '%"+filter.getText4()+"%'";
			}
			
			sql += " ORDER BY brandName, userId";
			
			contestSession = ContestGrandWinnersDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestGrandWinnersDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			//query.addScalar("customerId", Hibernate.INTEGER);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("prizeText", Hibernate.STRING);
			query.addScalar("brandName", Hibernate.STRING);
			query.addScalar("barcode", Hibernate.STRING);
			query.addScalar("prizeQty", Hibernate.INTEGER);
			query.addScalar("prizeValue", Hibernate.DOUBLE);
			query.addScalar("isWinnerEmailSent", Hibernate.BOOLEAN);
			
			winners = query.setResultTransformer(Transformers.aliasToBean(ContestGrandWinners.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return winners;
	}

	@Override
	public List<ContestGrandWinners> getWinnerstoEmail() {
		return find("FROM ContestGrandWinners WHERE isWinnerEmailSent=?", new Object[]{false});
	}

	@Override
	public List<ContestGrandWinners> getWinnersByBrandToSendEmail(String passcode) {
		return find("FROM ContestGrandWinners WHERE barcode=? AND isWinnerEmailSent=?", new Object[]{passcode,false});
	}

	@Override
	public List<ContestGrandWinners> getGrandWinnersByContestIdAndBrandId(Integer coId, Integer brandId) {

		List<ContestGrandWinners> winners = null;
		try {
			String sql ="select * from contest_grand_winners w join package_details pd on w.contestId = pd.contestId ";
			sql += "where w.contestId="+coId+" AND pd.pdID="+brandId;
			sql += " ORDER BY  userId";
			
			contestSession = ContestGrandWinnersDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestGrandWinnersDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("prizeText", Hibernate.STRING);
			query.addScalar("brandName", Hibernate.STRING);
			query.addScalar("barcode", Hibernate.STRING);
			query.addScalar("birthDate", Hibernate.STRING);
			query.addScalar("zipcode", Hibernate.STRING);
			query.addScalar("prizeQty", Hibernate.INTEGER);
			query.addScalar("prizeValue", Hibernate.DOUBLE);
			query.addScalar("isWinnerEmailSent", Hibernate.BOOLEAN);
			
			winners = query.setResultTransformer(Transformers.aliasToBean(ContestGrandWinners.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return winners;
	
	}

}
