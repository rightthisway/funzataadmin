package com.rtfbar.admin.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfbar.admin.data.ContestParticipants;

public class ContestParticipantsDAO extends HibernateDAO<Integer, ContestParticipants> implements com.rtfbar.admin.dao.services.ContestParticipantsDAO{

	private static Session contestSession = null;

	public static Session getContestSession() {
		return ContestParticipantsDAO.contestSession;
	}
	public static void setContestSession(Session contestSession) {
		ContestParticipantsDAO.contestSession = contestSession;
	}
	
	
	
	@Override
	public List<ContestParticipants> getContestParticipantsByContestAndBrandId(Integer coId, Integer brandId) {
		List<ContestParticipants> participents = null;
		try {
			String sql ="select p.cpID as id,p.contestId as coId,p.ageGroup as ageGrp,p.crDate as crDate,p.isJoined as isJoined,p.birthDate as birthDate,p.zipcode as zipcode,"
					+ "p.userId as userId,p.email as email, p.phone as phone,pd.brandName as brandName, p.version as version, p.deviceType as deviceType  "
					+ "from contest_participants p join package_details pd on p.contestId=pd.contestId ";
					sql += "where p.contestId="+coId+" and pdID="+brandId;
					sql += " ORDER BY  p.userId";
			
			contestSession = ContestParticipantsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestParticipantsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("coId", Hibernate.INTEGER);
			query.addScalar("ageGrp", Hibernate.STRING);
			query.addScalar("birthDate", Hibernate.STRING);
			query.addScalar("zipcode", Hibernate.STRING);
			query.addScalar("version", Hibernate.STRING);
			query.addScalar("deviceType", Hibernate.STRING);
			query.addScalar("isJoined", Hibernate.BOOLEAN);
			query.addScalar("crDate", Hibernate.TIMESTAMP);
			query.addScalar("brandName", Hibernate.STRING);
			
			participents = query.setResultTransformer(Transformers.aliasToBean(ContestParticipants.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return participents;
	}
	
	
	
	@Override
	public List<ContestParticipants> getContestParticipantsByContest(Integer coId,String passcode) {
		List<ContestParticipants> participents = null;
		try {
			String sql ="select p.cpID as id,p.contestId as coId,p.ageGroup as ageGrp,p.crDate as crDate,p.isJoined as isJoined,"
					+ "p.userId as userId,p.email as email, p.phone as phone,pd.brandName as brandName, p.version as version, p.deviceType as deviceType  "
					+ "from contest_participants p join package_details pd on p.contestId=pd.contestId ";
					sql += "where p.contestId="+coId+" AND p.passcode = '"+passcode+"' ";
					sql += " ORDER BY  p.userId";
			
			contestSession = ContestParticipantsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestParticipantsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("coId", Hibernate.INTEGER);
			query.addScalar("birthDate", Hibernate.STRING);
			query.addScalar("zipcode", Hibernate.STRING);
			query.addScalar("version", Hibernate.STRING);
			query.addScalar("deviceType", Hibernate.STRING);
			query.addScalar("isJoined", Hibernate.BOOLEAN);
			query.addScalar("crDate", Hibernate.TIMESTAMP);
			query.addScalar("brandName", Hibernate.STRING);
			
			participents = query.setResultTransformer(Transformers.aliasToBean(ContestParticipants.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return participents;
	}

	
}
