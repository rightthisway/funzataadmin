package com.rtfbar.admin.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfbar.admin.data.ContestQuestions;
import com.rtfbar.admin.utils.GridHeaderFilters;
import com.rtfbar.admin.utils.Util;


public class ContestQuestionsDAO extends HibernateDAO<Integer, ContestQuestions> implements com.rtfbar.admin.dao.services.ContestQuestionsDAO{

	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return ContestQuestionsDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		ContestQuestionsDAO.contestSession = contestSession;
	}
	
	@Override
	public List<ContestQuestions> getContestQuestions(GridHeaderFilters filter,Integer questionId,Integer contestId) {
		List<ContestQuestions> questionList = null;		
		try {
			String sql = "select * from contest_questions with(nolock)  WHERE 1=1 ";
				
			if(questionId!=null){
				sql += " AND id ="+questionId;
			}
			if(contestId!=null){
				sql += " AND contestId="+contestId;
			}
			if(filter.getqSerialNo()!=null){
				sql += " AND serialNo ="+filter.getqSerialNo();
			}
			if(filter.getText7()!=null && !filter.getText7().isEmpty()){
				sql += " AND question like '%"+filter.getText7()+"%' ";
			}
			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND optionA like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND optionB like '%"+filter.getText2()+"%' ";
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND optionC like '%"+filter.getText3()+"%' ";
			}
			/*if(filter.getText4()!=null && !filter.getText4().isEmpty()){
				sql += " AND option_d like '%"+filter.getText4()+"%' ";
			}*/
			if(filter.getText5()!=null && !filter.getText5().isEmpty()){
				sql += " AND answer like '%"+filter.getText5()+"%' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND createdBy like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql += " AND updatedBy like '"+filter.getUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDate()!=null ){
				if(Util.extractDateElement(filter.getCreatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day,createdDate) = "+Util.
							extractDateElement(filter.getCreatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,createdDate) = "+Util.extractDateElement(filter.getCreatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,createdDate) = "+Util.extractDateElement(filter.getCreatedDate(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, updatedDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,updatedDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,updatedDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			sql += " order by serialNo";
			
			contestSession = ContestQuestionsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestQuestionsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			

			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("serialNo", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);						
			query.addScalar("optionC", Hibernate.STRING);
			query.addScalar("answer", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);
			
			questionList = query.setResultTransformer(Transformers.aliasToBean(ContestQuestions.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	
		
	}

	@Override
	public ContestQuestions getContestQuestionsByNo(Integer contestId,Integer questionNo) {
		return findSingle("FROM ContestQuestions WHERE contestId=? and serialNo=?",new Object[]{contestId,questionNo});
	}

	@Override
	public Integer getQuestionCount(Integer contestId) {
		Integer count =0;		
		try {
			String sql = "select count(id) from contest_questions with(nolock) where contestId="+contestId;
			
			contestSession = ContestQuestionsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestQuestionsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;	
	}

	@Override
	public ContestQuestions getContestQuestionByQuestion(Integer contestId,String question){
		return findSingle("FROM ContestQuestions WHERE contestId=? and question like ?",new Object[]{contestId, "%"+question+"%"});
	}
	
	@Override
	public List<ContestQuestions> getContestQuestionstoReposition(Integer actualPosition,Integer contestId) {
		List<ContestQuestions> questionList = null;		
		try {
			String sql = "select * from contest_questions with(nolock) WHERE serialNo <> "+actualPosition;
				
			sql += " AND contestId = "+contestId+" order by serialNo";
			
			contestSession = ContestQuestionsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestQuestionsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("serialNo", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);						
			query.addScalar("optionC", Hibernate.STRING);
			query.addScalar("answer", Hibernate.STRING);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);
			
			
			questionList = query.setResultTransformer(Transformers.aliasToBean(ContestQuestions.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	
		
	}
}
