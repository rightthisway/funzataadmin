package com.rtfbar.admin.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfbar.admin.data.ContestGrandWinners;
import com.rtfbar.admin.data.ContestWinner;
import com.rtfbar.admin.data.Contests;
import com.rtfbar.admin.data.Customer;
import com.rtfbar.admin.data.SharedProperty;
import com.rtfbar.admin.utils.GridHeaderFilters;
import com.rtfbar.admin.utils.Util;

public class ContestsDAO extends HibernateDAO<Integer, Contests> implements com.rtfbar.admin.dao.services.ContestsDAO{

	//private String zonesApiDbUrl = "ZonesApiPlatformV2.dbo";
	private static Session contestSession = null;

	public static Session getContestSession() {
		return ContestsDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		ContestsDAO.contestSession = contestSession;
	}

	public List<Contests> getAllContests(Integer id, GridHeaderFilters filter,String status){
		List<Contests> contestsList = null;
		try {
			String sql = "select convert(varchar(2), datepart(hour,startDateTime)) as hours, convert(varchar(2), datepart(minute,startDateTime)) as minutes,* from contest with(nolock) "+
					" where 1=1 ";

			if(id != null  && id > 0){
				sql += " AND id="+id;
			}
			if(status!=null && !status.isEmpty()){
				sql += " AND status in("+status+") ";
			}
			if(filter.getContestName()!=null && !filter.getContestName().isEmpty()){
				sql += " AND contestName like '%"+filter.getContestName()+"%' ";
			}
			if(filter.getContestDate()!=null ){
				if(Util.extractDateElement(filter.getContestDate(),"DAY") > 0){
					sql += " AND DATEPART(day, startDateTime) = "+Util.extractDateElement(filter.getContestDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getContestDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,startDateTime) = "+Util.extractDateElement(filter.getContestDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getContestDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, startDateTime) = "+Util.extractDateElement(filter.getContestDate(),"YEAR")+" ";
				}
			}


			if(filter.getCount() != null){
				sql += " AND questionSize= "+filter.getCount();
			}
			
			if(filter.getMaxWinners() != null){
				sql += " AND maxWinners ="+filter.getMaxWinners();
			}
			if(filter.getPrizeQty() != null){
				sql += " AND prizeQty="+filter.getPrizeQty();
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND status like '"+filter.getStatus()+"' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND createdBy like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql += " AND updatedBy like '"+filter.getUpdatedBy()+"' ";
			}

			if(filter.getCreatedDate()!=null ){
				if(Util.extractDateElement(filter.getCreatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, createdDate) = "+Util.extractDateElement(filter.getCreatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, createdDate) = "+Util.extractDateElement(filter.getCreatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, createdDate) = "+Util.extractDateElement(filter.getCreatedDate(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day,updatedDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, updatedDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, updatedDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}

			if(status !=null && status.contains("EXPIRED")){
				sql +=" order by startDateTime desc";
			}else{
				sql +=" order by startDateTime";
			}

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);

			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestName", Hibernate.STRING);
			query.addScalar("startDateTime", Hibernate.TIMESTAMP);
			query.addScalar("maxWinners", Hibernate.INTEGER);
			query.addScalar("prizeQty", Hibernate.INTEGER);
			query.addScalar("prizeText", Hibernate.STRING);
			query.addScalar("prizeValue", Hibernate.DOUBLE);
			query.addScalar("questionSize", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("minutes", Hibernate.STRING);
			query.addScalar("hours", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("isPasscodeAct", Hibernate.BOOLEAN);
			query.addScalar("isSponPublic", Hibernate.BOOLEAN);

			contestsList = query.setResultTransformer(Transformers.aliasToBean(Contests.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return contestsList;
	}

	@Override
	public List<Contests> getAllActiveContests() {
		return find("FROM Contests where status=?",new Object[]{"ACTIVE"});
	}

	@Override
	public List<Contests> getAllStartedContests() {
		return find("FROM Contests where status=?",new Object[]{"STARTED"});
	}

	@Override
	public Contests getTodaysContest(String status) {
		List<Contests> contestsList = null;
		Date today = new Date();
		try {
			String sql = "select top 1 * from contest with(nolock) where status in("+status+") ";

					sql += " AND DATEPART(day, startDateTime) = "+Util.extractDateElement(Util.formatDateToMonthDateYear(today),"DAY")+" ";
					sql += " AND DATEPART(month,startDateTime) = "+Util.extractDateElement(Util.formatDateToMonthDateYear(today),"MONTH")+" ";
					sql += " AND DATEPART(year,startDateTime) = "+Util.extractDateElement(Util.formatDateToMonthDateYear(today),"YEAR")+" ";
					sql += " order by startDateTime";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestName", Hibernate.STRING);
			query.addScalar("startDateTime", Hibernate.TIMESTAMP);
			query.addScalar("maxWinners", Hibernate.INTEGER);
			query.addScalar("prizeQty", Hibernate.INTEGER);
			query.addScalar("prizeText", Hibernate.STRING);
			query.addScalar("prizeValue", Hibernate.DOUBLE);
			query.addScalar("questionSize", Hibernate.INTEGER);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);
			query.addScalar("createdBy", Hibernate.STRING);
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("lastAction", Hibernate.STRING);
			query.addScalar("lastQuestionNo", Hibernate.INTEGER);
			query.addScalar("wpId", Hibernate.INTEGER);
			query.addScalar("isPasscodeAct", Hibernate.BOOLEAN);
			query.addScalar("isSponPublic", Hibernate.BOOLEAN);
			contestsList = query.setResultTransformer(Transformers.aliasToBean(Contests.class)).list();
			if(!contestsList.isEmpty()){
				return contestsList.get(0);
			}
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}


	public boolean resetContestDataById(Integer contestId,SharedProperty sharedProperty){
		String sql = null;
		try {
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}

			sql = "delete from contest_winners where contestId="+contestId;
			SQLQuery query1 = contestSession.createSQLQuery(sql);
			query1.executeUpdate();

			sql = "delete from contest_grand_winners where contesId="+contestId;
			SQLQuery query2 = contestSession.createSQLQuery(sql);
			query2.executeUpdate();

			return true;

		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<ContestWinner> getContestWinners(Integer contestId,Integer brandId) {
		List<ContestWinner> winners = null;
		try {
			String sql ="select * from contest_winner cw join package_details pd on cw.contestId = pd.contestId  where cw.contestId="+contestId+" AND pd.pdID="+brandId;
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("birthDate", Hibernate.STRING);
			query.addScalar("zipcode", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("barcode", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			winners = query.setResultTransformer(Transformers.aliasToBean(ContestWinner.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return winners;
	}

	@Override
	public List<ContestGrandWinners> getContestGrandWinners(Integer contestId,String passcode) {
		List<ContestGrandWinners> winners = null;
		try {
			String sql ="select * from contest_grand_winners contestId="+contestId+" AND barcode='"+passcode+"' ";
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("prizeText", Hibernate.STRING);
			query.addScalar("zipcode", Hibernate.STRING);
			query.addScalar("birthDate", Hibernate.STRING);
			query.addScalar("barcode", Hibernate.STRING);
			query.addScalar("prizeQty", Hibernate.INTEGER);
			query.addScalar("prizeValue", Hibernate.DOUBLE);
			
			winners = query.setResultTransformer(Transformers.aliasToBean(ContestGrandWinners.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return winners;
	}

	@Override
	public List<Customer> getContestCustomers(Integer contestId,SharedProperty sharedProperty) {
		List<Customer> customers = null;
		try {
			String SQLQuery = "select DISTINCT c.cust_name AS customerName,c.last_name AS lastName,c.user_id AS userId,c.email AS email,phone AS phone,c.signup_date as signupDate,"+
					"min(join_datetime) as joinDate from contest_participants wt join customer c "
					+ "on wt.customer_id=c.id where wt.contest_id="+contestId+" "
					+ "group by c.id,c.cust_name,c.last_name,c.user_id,c.email,c.phone,c.signup_date order by c.user_id";

			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(SQLQuery);
			query.addScalar("customerName", Hibernate.STRING);
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("userId", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("signupDate", Hibernate.TIMESTAMP);
			query.addScalar("joinDate", Hibernate.TIMESTAMP);


			customers = query.setResultTransformer(Transformers.aliasToBean(Customer.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return customers;
	}

	@Override
	public Contests getContestByWpId(Integer id) {
		return findSingle("From Contests WHERE wpId=? AND (status=? OR status=?)", new Object[]{id,"ACTIVE","EXPIRED"});
	}

	@Override
	public Boolean deleteContest(String ids) {
		try {
			if(ids.isEmpty()){
				return true;
			}
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery("UPDATE contest set status='DELETED' where wpId NOT in("+ids+") ");
			query.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Contests> getActiveContestByDateRange(Date frDate, Date enDate) {
		return find("FROM Contests WHERE status=? AND startDateTime > ? AND startDateTime < ?",new Object[]{"ACTIVE",frDate,enDate});
	}

	@Override
	public List<Contests> getDeletableContests(List<Integer> ids) {
		try {
			contestSession = ContestsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				ContestsDAO.setContestSession(contestSession);
			}
			org.hibernate.Query query = contestSession.createQuery("FROM Contests WHERE status='ACTIVE' AND wpId not in(:ids)");
			query.setParameterList("ids", ids);
			return query.list();
		} catch (Exception e) {
			e.printStackTrace();
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
		}
		return new ArrayList<Contests>();
	}
}
