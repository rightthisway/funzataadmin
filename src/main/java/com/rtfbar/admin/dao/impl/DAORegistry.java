package com.rtfbar.admin.dao.impl;

public class DAORegistry {
	private static ContestsDAO contestsDAO;
	private static ContestQuestionsDAO contestQuestionsDAO;
	private static QuestionBankDAO questionBankDAO;
	private static ContestGrandWinnersDAO contestGrandWinnersDAO;
	private static TrackerUserDAO trackerUserDAO;
	private static RoleDAO roleDAO;
	private static UserActionDAO userActionDAO;
	private static QueryManagerDAO queryManagerDAO;
	private static ContestWinnerDAO contestWinnerDAO;
	private static PackageDetailsDAO packageDetailsDAO;
	private static ContestParticipantsDAO contestParticipantsDAO;
	private static CustomerAnswerDAO customerAnswerDAO;
	
	
	public static ContestsDAO getContestsDAO() {
		return contestsDAO;
	}
	public final void setContestsDAO(ContestsDAO contestsDAO) {
		DAORegistry.contestsDAO = contestsDAO;
	}
	public static ContestQuestionsDAO getContestQuestionsDAO() {
		return contestQuestionsDAO;
	}
	public final void setContestQuestionsDAO(ContestQuestionsDAO contestQuestionsDAO) {
		DAORegistry.contestQuestionsDAO = contestQuestionsDAO;
	}
	public static QuestionBankDAO getQuestionBankDAO() {
		return questionBankDAO;
	}
	public final void setQuestionBankDAO(QuestionBankDAO questionBankDAO) {
		DAORegistry.questionBankDAO = questionBankDAO;
	}
	public static ContestGrandWinnersDAO getContestGrandWinnersDAO() {
		return contestGrandWinnersDAO;
	}
	public final void setContestGrandWinnersDAO(ContestGrandWinnersDAO contestGrandWinnersDAO) {
		DAORegistry.contestGrandWinnersDAO = contestGrandWinnersDAO;
	}
	public static TrackerUserDAO getTrackerUserDAO() {
		return trackerUserDAO;
	}
	public final void setTrackerUserDAO(TrackerUserDAO trackerUserDAO) {
		DAORegistry.trackerUserDAO = trackerUserDAO;
	}
	public static RoleDAO getRoleDAO() {
		return roleDAO;
	}
	public final void setRoleDAO(RoleDAO roleDAO) {
		DAORegistry.roleDAO = roleDAO;
	}
	public static UserActionDAO getUserActionDAO() {
		return userActionDAO;
	}
	public final void setUserActionDAO(UserActionDAO userActionDAO) {
		DAORegistry.userActionDAO = userActionDAO;
	}
	public static QueryManagerDAO getQueryManagerDAO() {
		return queryManagerDAO;
	}
	public final void setQueryManagerDAO(QueryManagerDAO queryManagerDAO) {
		DAORegistry.queryManagerDAO = queryManagerDAO;
	}
	public static ContestWinnerDAO getContestWinnerDAO() {
		return contestWinnerDAO;
	}
	public final void setContestWinnerDAO(ContestWinnerDAO contestWinnerDAO) {
		DAORegistry.contestWinnerDAO = contestWinnerDAO;
	}
	public static PackageDetailsDAO getPackageDetailsDAO() {
		return packageDetailsDAO;
	}
	public final void setPackageDetailsDAO(PackageDetailsDAO packageDetailsDAO) {
		DAORegistry.packageDetailsDAO = packageDetailsDAO;
	}
	public static ContestParticipantsDAO getContestParticipantsDAO() {
		return contestParticipantsDAO;
	}
	public final void setContestParticipantsDAO(ContestParticipantsDAO contestParticipantsDAO) {
		DAORegistry.contestParticipantsDAO = contestParticipantsDAO;
	}
	public static CustomerAnswerDAO getCustomerAnswerDAO() {
		return customerAnswerDAO;
	}
	public final void setCustomerAnswerDAO(CustomerAnswerDAO customerAnswerDAO) {
		DAORegistry.customerAnswerDAO = customerAnswerDAO;
	}
}
