package com.rtfbar.admin.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfbar.admin.data.PackageDetails;
import com.rtfbar.admin.utils.GridHeaderFilters;
import com.rtfbar.admin.utils.Util;

public class PackageDetailsDAO extends HibernateDAO<Integer, PackageDetails> implements com.rtfbar.admin.dao.services.PackageDetailsDAO{

	
	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return PackageDetailsDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		PackageDetailsDAO.contestSession = contestSession;
	}
	
	
	
	@Override
	public List<PackageDetails> getContestParticipantBrands(Integer coId,GridHeaderFilters filter) {
		List<PackageDetails> packages = null;
		try {
			String sql = "select * from package_details with(nolock)  WHERE status='ACTIVE' AND contestId= "+coId;
			
			if(filter.getText1()!=null){
				sql += " AND brandName like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND passCode like '%"+filter.getText2()+"%' ";
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND firstName like '%"+filter.getText3()+"%' ";
			}
			if(filter.getText4()!=null && !filter.getText4().isEmpty()){
				sql += " AND lastName like '%"+filter.getText4()+"%' ";
			}
			if(filter.getText5()!=null && !filter.getText5().isEmpty()){
				sql += " AND email like '%"+filter.getText5()+"%' ";
			}
			if(filter.getText6()!=null && !filter.getText6().isEmpty()){
				sql += " AND phone like '%"+filter.getText6()+"%' ";
			}
			if(filter.getText7()!=null && !filter.getText7().isEmpty()){
				if(filter.getText7().equalsIgnoreCase("YES")){
					sql += " AND isWinnerEmailSent = 1";
				}else if(filter.getText7().equalsIgnoreCase("YES")){
					sql += " AND isWinnerEmailSent = 0";
				}
			}
			
			if(filter.getWinnerPrize()!=null && !filter.getWinnerPrize().isEmpty()){
				sql += " AND winnerPrize like '%"+filter.getWinnerPrize()+"%' ";
			}
			if(filter.getText8()!=null && !filter.getText8().isEmpty()){
				sql += " AND finelistPrize like '%"+filter.getText8()+"%' ";
			}
			if(filter.getCount()!=null){
				sql += " AND userLimitCnt = "+filter.getCount();
			}
			if(filter.getqSerialNo()!=null){
				sql += " AND actUserCnt ="+filter.getqSerialNo();
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND crBy like '%"+filter.getCreatedBy()+"%' ";
			}
			if(filter.getUpdatedBy()!=null && !filter.getUpdatedBy().isEmpty()){
				sql += " AND updBy like '%"+filter.getUpdatedBy()+"%' ";
			}
			
			if(filter.getCreatedDate()!=null ){
				if(Util.extractDateElement(filter.getCreatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day,crDate) = "+Util.
							extractDateElement(filter.getCreatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,crDate) = "+Util.extractDateElement(filter.getCreatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,crDate) = "+Util.extractDateElement(filter.getCreatedDate(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, updDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month,updDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year,updDate) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			sql += " order by crDate";
			
			contestSession = PackageDetailsDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				PackageDetailsDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			

			query.addScalar("pdID", Hibernate.INTEGER);
			query.addScalar("contestId", Hibernate.INTEGER);
			query.addScalar("brandName", Hibernate.STRING);
			query.addScalar("passCode", Hibernate.STRING);
			query.addScalar("userLimitCnt", Hibernate.INTEGER);
			query.addScalar("actUserCnt", Hibernate.INTEGER);						
			query.addScalar("winnerPrize", Hibernate.STRING);
			query.addScalar("finelistPrize", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("crDate", Hibernate.TIMESTAMP);	
			query.addScalar("updDate", Hibernate.TIMESTAMP);	
			query.addScalar("firstName", Hibernate.STRING);			
			query.addScalar("lastName", Hibernate.STRING);
			query.addScalar("email", Hibernate.STRING);
			query.addScalar("crBy", Hibernate.STRING);
			query.addScalar("updBy", Hibernate.STRING);	
			query.addScalar("phone", Hibernate.STRING);
			query.addScalar("addr", Hibernate.STRING);
			query.addScalar("wpId", Hibernate.INTEGER);
			query.addScalar("isWinnerEmailSent", Hibernate.BOOLEAN);
			query.addScalar("isCancellationEmailSent", Hibernate.BOOLEAN);
			
			packages = query.setResultTransformer(Transformers.aliasToBean(PackageDetails.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return packages;
	}

	@Override
	public PackageDetails getPackageDetaisByBrandNameAndContestId(Integer coId, String passcode) {
		return findSingle("FROM PackageDetails WHERE status=? AND contestId=? AND passCode=?", new Object[]{"ACTIVE",coId,passcode});
	}

	@Override
	public List<PackageDetails> getAllWinnersBrandsToEmail() {
		return find("FROM PackageDetails WHERE status=? AND isWinnerEmailSent=? ",new Object[]{"ACTIVE",false});
	}

	@Override
	public List<PackageDetails> getAllActivePackageDetailsByCoIdAndWpId(Integer coId, Integer wpId) {
		return find("FROM PackageDetails WHERE status=? AND contestId=? AND wpId=?",new Object[]{"ACTIVE",coId,wpId});
	}

}
