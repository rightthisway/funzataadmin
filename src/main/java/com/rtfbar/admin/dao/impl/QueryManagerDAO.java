package com.rtfbar.admin.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class QueryManagerDAO {

	private static SessionFactory sessionFactory;
	private static Session staticSession = null;
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	public static Session getStaticSession() {
	   if(staticSession==null || !staticSession.isOpen() || !staticSession.isConnected()){
		   staticSession = getSessionFactory().openSession();
		   QueryManagerDAO.setStaticSession(staticSession);
	   }
	   return staticSession;
	}

	public static void setStaticSession(Session staticSession) {
		QueryManagerDAO.staticSession = staticSession;
	}

}