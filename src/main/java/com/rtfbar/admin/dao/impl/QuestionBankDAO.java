package com.rtfbar.admin.dao.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfbar.admin.data.QuestionBank;
import com.rtfbar.admin.utils.GridHeaderFilters;
import com.rtfbar.admin.utils.PaginationUtil;
import com.rtfbar.admin.utils.Util;


public class QuestionBankDAO extends HibernateDAO<Integer, QuestionBank> implements com.rtfbar.admin.dao.services.QuestionBankDAO{

	private static Session contestSession = null;
	
	public static Session getContestSession() {
		return QuestionBankDAO.contestSession;
	}

	public static void setContestSession(Session contestSession) {
		QuestionBankDAO.contestSession = contestSession;
	}
	
	
	/*@Override
	public List<QuestionBank> getQuestionBankList(GridHeaderFilters filter,Integer questionBankId, String pageNo,String status,Boolean isPagination) {
		List<QuestionBank> questionList = null;	
		Integer firstResult = 0;
		try {
			String sql = "select id as id, question as question, option_a as optionA, option_b as optionB, option_c as optionC, "+
						"answer as answer, question_reward as questionReward, status as status, category as category,dificulty_level AS dificultyLevel, "+
						"created_datetime as createdDate, updated_datetime as updatedDate, created_by as createdBy,fact_checked as factChecked, "+
						"updated_by as updatedBy from question_bank with(nolock) WHERE 1=1 and status in("+status+") ";
				
			if(questionBankId!=null){
				sql += " AND id ="+questionBankId;
			}			
			if(filter.getText7()!=null && !filter.getText7().isEmpty()){
				sql += " AND question like '%"+filter.getText7()+"%' ";
			}
			if(filter.getText4() != null && !filter.getText4().isEmpty()){
				if(filter.getText4().equalsIgnoreCase("Yes")){
					sql += " AND fact_checked = 1";
				}else{
					sql += " AND fact_checked = 0";
				}
			}
			if(filter.getCost()!=null){
				sql += " AND question_reward = "+filter.getCost();
			}			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND option_a like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND option_b like '%"+filter.getText2()+"%' ";
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND option_c like '%"+filter.getText3()+"%' ";
			}
			if(filter.getText6()!=null && !filter.getText6().isEmpty()){
				sql += " AND dificulty_level like '%"+filter.getText6()+"%' ";
			}
			if(filter.getText5()!=null && !filter.getText5().isEmpty()){
				sql += " AND answer like '%"+filter.getText5()+"%' ";
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getCategory()!=null && !filter.getCategory().isEmpty()){
				sql += " AND category like '%"+filter.getCategory()+"%' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND updated_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
			sql += " order by id";
			
			contestSession = QuestionBankDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				QuestionBankDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("question", Hibernate.STRING);
			query.addScalar("optionA", Hibernate.STRING);
			query.addScalar("optionB", Hibernate.STRING);						
			query.addScalar("optionC", Hibernate.STRING);
			//query.addScalar("optionD", Hibernate.STRING);
			query.addScalar("answer", Hibernate.STRING);
			query.addScalar("status", Hibernate.STRING);
			query.addScalar("category", Hibernate.STRING);
			query.addScalar("dificultyLevel", Hibernate.STRING);
			query.addScalar("questionReward", Hibernate.DOUBLE);
			query.addScalar("createdDate", Hibernate.TIMESTAMP);	
			query.addScalar("updatedDate", Hibernate.TIMESTAMP);	
			query.addScalar("createdBy", Hibernate.STRING);			
			query.addScalar("updatedBy", Hibernate.STRING);
			query.addScalar("factChecked", Hibernate.BOOLEAN);
			
			if(isPagination){
				firstResult = PaginationUtil.getNextPageStatFrom(pageNo);
				questionList = query.setResultTransformer(Transformers.aliasToBean(QuestionBank.class)).setFirstResult(firstResult).setMaxResults(PaginationUtil.PAGESIZE).list();
			}else{
				questionList = query.setResultTransformer(Transformers.aliasToBean(QuestionBank.class)).list();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questionList;
	}

	@Override
	public Integer getQuestionBankCount(GridHeaderFilters filter,Integer questionBankId,String status) {
		Integer count =0;
		try {
			String sql = "select count(*) as cnt from question_bank with(nolock) WHERE 1=1 and status in("+status+") ";
				
			if(questionBankId!=null){
				sql += " AND id ="+questionBankId;
			}			
			if(filter.getText7()!=null && !filter.getText7().isEmpty()){
				sql += " AND question like '%"+filter.getText7()+"%' ";
			}
			if(filter.getCost()!=null){
				sql += " AND question_reward = "+filter.getCost();
			}			
			if(filter.getText1()!=null && !filter.getText1().isEmpty()){
				sql += " AND option_a like '%"+filter.getText1()+"%' ";
			}
			if(filter.getText2()!=null && !filter.getText2().isEmpty()){
				sql += " AND option_b like '%"+filter.getText2()+"%' ";
			}
			if(filter.getText3()!=null && !filter.getText3().isEmpty()){
				sql += " AND option_c like '%"+filter.getText3()+"%' ";
			}
			if(filter.getText4() != null && !filter.getText4().isEmpty()){
				if(filter.getText4().equalsIgnoreCase("Yes")){
					sql += " AND fact_checked = 1";
				}else{
					sql += " AND fact_checked = 0";
				}
			}
			if(filter.getText5()!=null && !filter.getText5().isEmpty()){
				sql += " AND answer like '%"+filter.getText5()+"%' ";
			}
			if(filter.getStatus()!=null && !filter.getStatus().isEmpty()){
				sql += " AND status like '%"+filter.getStatus()+"%' ";
			}
			if(filter.getCategory()!=null && !filter.getCategory().isEmpty()){
				sql += " AND category like '%"+filter.getCategory()+"%' ";
			}
			if(filter.getCreatedBy()!=null && !filter.getCreatedBy().isEmpty()){
				sql += " AND created_by like '"+filter.getCreatedBy()+"' ";
			}
			if(filter.getLastUpdatedBy()!=null && !filter.getLastUpdatedBy().isEmpty()){
				sql += " AND updated_by like '"+filter.getLastUpdatedBy()+"' ";
			}
			
			if(filter.getCreatedDateStr()!=null ){
				if(Util.extractDateElement(filter.getCreatedDateStr(),"DAY") > 0){
					sql += " AND DATEPART(day,created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"MONTH") > 0){
					sql += " AND DATEPART(month,created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getCreatedDateStr(),"YEAR") > 0){
					sql += " AND DATEPART(year, created_datetime) = "+Util.extractDateElement(filter.getCreatedDateStr(),"YEAR")+" ";
				}
			}
			if(filter.getUpdatedDate()!=null ){
				if(Util.extractDateElement(filter.getUpdatedDate(),"DAY") > 0){
					sql += " AND DATEPART(day, updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"DAY")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"MONTH") > 0){
					sql += " AND DATEPART(month, updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"MONTH")+" ";
				}
				if(Util.extractDateElement(filter.getUpdatedDate(),"YEAR") > 0){
					sql += " AND DATEPART(year, updated_datetime) = "+Util.extractDateElement(filter.getUpdatedDate(),"YEAR")+" ";
				}
			}
//			sql += " order by id";
			
			contestSession = QuestionBankDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				QuestionBankDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(sql);
			
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
	@Override
	public Collection<QuestionBank> getAllQuestionBankCategory(String category){
		Session session = null;
		List<QuestionBank> questionList = null;
		
		try {
			String SQLQuery = "select distinct category from question_bank with(nolock) where 1=1 ";
			
			if(category!=null && !category.isEmpty()){
				SQLQuery += "AND category like '%"+category+"%' ";
			}
			SQLQuery += "order by category Asc";
			
			contestSession = QuestionBankDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				QuestionBankDAO.setContestSession(contestSession);
			}
			SQLQuery query = contestSession.createSQLQuery(SQLQuery);
			query.addScalar("category", Hibernate.STRING);
			
			questionList = query.setResultTransformer(Transformers.aliasToBean(QuestionBank.class)).list();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return questionList;
	}
	
	@Override
	public QuestionBank getQuestionBankByQuestion(String question){
		return findSingle("FROM QuestionBank WHERE question like ? and status in('ACTIVE','USED')",new Object[]{"%"+question+"%"});
	}
	
	@Override
	public Integer getUsedQuestionCount(String questionIdStr) {
		Integer count =0;		
		try {
			String sql = "select count(*) from Question_Bank with(nolock) where status = 'USED' and id in ( " + questionIdStr  + " )" ; 
			
			contestSession = QuestionBankDAO.getContestSession();
			if(contestSession==null || !contestSession.isOpen() || !contestSession.isConnected()){
				contestSession = getSession();
				QuestionBankDAO.setContestSession(contestSession);
			}
			
			SQLQuery query = contestSession.createSQLQuery(sql);			
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if(contestSession != null && contestSession.isOpen()){
				contestSession.close();
			}
			e.printStackTrace();
		}
		return count;	
	}*/
	
	
	
	
	
	
	
	
}
