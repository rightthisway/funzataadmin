package com.rtfbar.admin.dao.impl;

import com.rtfbar.admin.data.Role;


public class RoleDAO extends HibernateDAO<Integer, Role> implements com.rtfbar.admin.dao.services.RoleDAO {
	
	public Role getRoleByName(String roleName){
		return findSingle("From Role where name = ?", new Object[]{roleName});
	}
}
