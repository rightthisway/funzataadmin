package com.rtfbar.admin.dao.impl;

import java.util.Collection;
import java.util.List;

import com.rtfbar.admin.data.TrackerUser;
import com.rtfbar.admin.utils.GridHeaderFilters;

public class TrackerUserDAO extends HibernateDAO<Integer, TrackerUser> implements com.rtfbar.admin.dao.services.TrackerUserDAO {

	public TrackerUser getTrackerUserByUsernameOrEmail(String userName, String email){
		return findSingle("From TrackerUser where (userName = ? or email = ?) and status = ?", new Object[]{userName, email, true});
	}
	
	public TrackerUser getTrackerUserByUsernameOrEmailExceptUserId(String userName, String email, Integer userId){
		return findSingle("From TrackerUser where (userName = ? or email = ?) and id != ?", new Object[]{userName, email, userId});
	}
	
	/*public Collection<TrackerUser> getAllTrackerUser(GridHeaderFilters filter, String status, String pageNo){
		Collection<TrackerUser> list = new ArrayList<TrackerUser>();
		Integer startFrom = 0;
		try{
			String sqlQuery = "select u.id as id, u.firstname as firstName, u.lastname as lastName, u.username as userName, u.email as email, "+
					" u.phone as phone, u.broker_id as brokerId, b.company_name as company, u.status as status, u.promotional_code as promotionalCode"+
					" from tracker_users u left join tracker_brokers b on b.id = u.broker_id where 1=1";
			String sqlQuery = "From TrackerUser where 1=1";S
			
			if(filter.getUsername() != null){
				sqlQuery += " AND userName like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getCustomerName() != null){
				sqlQuery += " AND firstName like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				sqlQuery += " AND lastName like '%"+filter.getLastName()+"%' ";	
			}
			if(filter.getEmail() != null){
				sqlQuery += " AND email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				sqlQuery += " AND phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getUserId() != null){
				sqlQuery += " AND broker = "+filter.getUserId()+" ";
			}
			if(filter.getCompanyName() != null){
				sqlQuery += " AND broker.companyName like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getStatus() != null && filter.getStatus().equalsIgnoreCase("ACTIVE")){
				sqlQuery += " AND status = "+Boolean.TRUE;
			}
			if(filter.getStatus() != null && filter.getStatus().equalsIgnoreCase("DEACTIVE")){
				sqlQuery += " AND status = "+Boolean.FALSE;
			}
			if(filter.getReferrerCode() != null){
				sqlQuery += " AND promotionalCode like '%"+filter.getReferrerCode()+"%' ";
			}
			if(status != null && status.equalsIgnoreCase("ACTIVE")){
				sqlQuery += " AND status = "+Boolean.TRUE;
			}
			if(status != null && status.equalsIgnoreCase("INACTIVE")){
				sqlQuery += " AND status = "+Boolean.FALSE;
			}
			
			return find(sqlQuery);
			
			//list = query.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).setFirstResult(startFrom).setMaxResults(PaginationUtil.PAGESIZE).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}
	
	public Integer getAllTrackerUserCount(GridHeaderFilters filter){
		Integer count = 0;
		try{
			String sqlQuery = "select count(*) as cnt from tracker_users u with(nolock) "+
				" left join tracker_brokers b with(nolock) on b.id = u.broker_id where 1=1";
			
			if(filter.getUsername() != null){
				sqlQuery += " AND u.username like '%"+filter.getUsername()+"%' ";
			}
			if(filter.getCustomerName() != null){
				sqlQuery += " AND u.firstname like '%"+filter.getCustomerName()+"%' ";
			}
			if(filter.getLastName() != null){
				sqlQuery += " AND u.lastname like '%"+filter.getLastName()+"%' ";	
			}
			if(filter.getEmail() != null){
				sqlQuery += " AND u.email like '%"+filter.getEmail()+"%' ";
			}
			if(filter.getPhone() != null){
				sqlQuery += " AND u.phone like '%"+filter.getPhone()+"%' ";
			}
			if(filter.getUserId() != null){
				sqlQuery += " AND u.broker_id = "+filter.getUserId()+" ";
			}
			if(filter.getCompanyName() != null){
				sqlQuery += " AND b.company_name like '%"+filter.getCompanyName()+"%' ";
			}
			if(filter.getStatus() != null && filter.getStatus().equalsIgnoreCase("ACTIVE")){
				sqlQuery += " AND u.status = 1";
			}
			if(filter.getStatus() != null && filter.getStatus().equalsIgnoreCase("DEACTIVE")){
				sqlQuery += " AND u.status = 0";
			}
			if(filter.getReferrerCode() != null){
				sqlQuery += " AND u.promotional_code like '%"+filter.getReferrerCode()+"%' ";
			}
			Session session = getSession();
			SQLQuery query = session.createSQLQuery(sqlQuery);
			count = Integer.parseInt(String.valueOf(query.uniqueResult()));
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}*/
	
	
	/*public List<TrackerUser> getAllTrackerUsersByRole(String role) {
		List<TrackerUser> result = new ArrayList<TrackerUser>();
		Collection<TrackerUser> list  = getAllTrackerUser(new GridHeaderFilters(),"ACTIVE",null);
		for (TrackerUser user: list) {
			if (user.hasRole(role)) {
				result.add(user);
			}
		}
		return result;
	}
	
	public Collection<TrackerUser> getAllTrackerUsersByRole(String role, String status, GridHeaderFilters filter, String pageNo) {
		Collection<TrackerUser> result = new ArrayList<TrackerUser>();
		
		Collection<TrackerUser> trackerUsers = getAllTrackerUser(filter, status, pageNo);
		if(null == trackerUsers || trackerUsers.isEmpty()){
			return null;
		}
		
		for (TrackerUser user: trackerUsers ) {
			if (user.hasRole(role)) {
				result.add(user);
			}
		}
		return result;
	}
	
	public Collection<TrackerUser> getAllTrackerUsersByRoles(String role, String role1, String status, GridHeaderFilters filter,String pageNo) {
		Collection<TrackerUser> result = new ArrayList<TrackerUser>();
		
		Collection<TrackerUser> trackerUsers = getAllTrackerUser(filter, status, pageNo);
		if(null == trackerUsers || trackerUsers.isEmpty()){
			return null;
		}
		
		for (TrackerUser user: trackerUsers ) {
			if (user.hasRoles(role, role1)) {
				result.add(user);
			}
		}
		return result;
	}*/
	
	public Collection<TrackerUser> getAllTrackerUserByBrokerId(Integer brokerId){
		return find("FROM TrackerUser WHERE broker.id = ?", new Object[]{brokerId});
	}
	
	public Collection<TrackerUser> getAllTrackerUserExceptBroker(){
		return find("FROM TrackerUser WHERE broker.id is null");
	}

	@Override
	public List<TrackerUser> getAllTrackerUsersByRole(String role) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TrackerUser> getAllTrackerUsersByRole(String role, String status, GridHeaderFilters filter,
			String pageNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TrackerUser> getAllTrackerUsersByRoles(String role, String role1, String status,
			GridHeaderFilters filter, String pageNo) {
		// TODO Auto-generated method stub
		return null;
	}
}
