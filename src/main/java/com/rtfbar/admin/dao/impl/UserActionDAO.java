package com.rtfbar.admin.dao.impl;

import java.util.Collection;

import com.rtfbar.admin.data.UserAction;

public class UserActionDAO extends HibernateDAO<Integer, UserAction> implements com.rtfbar.admin.dao.services.UserActionDAO{

	public Collection<UserAction> getAllActionsByUserId(Integer userId){
		return find("From UserAction where trackerUser.id = ? order by timeStamp desc", new Object[]{userId});
	}
}
