package com.rtfbar.admin.dao.services;

import java.util.List;

import com.rtfbar.admin.data.ContestGrandWinners;
import com.rtfbar.admin.utils.GridHeaderFilters;

public interface ContestGrandWinnersDAO extends RootDAO<Integer, ContestGrandWinners>{
	
	public List<ContestGrandWinners> getGrandWinnersByContestId(Integer coId,GridHeaderFilters filter);
	public List<ContestGrandWinners> getGrandWinnersByContestIdAndBrandId(Integer coId,Integer brandId);
	public List<ContestGrandWinners> getWinnerstoEmail();
	public List<ContestGrandWinners> getWinnersByBrandToSendEmail(String passcode);
	
	
}
