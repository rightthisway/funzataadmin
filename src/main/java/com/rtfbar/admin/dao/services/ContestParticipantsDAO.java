package com.rtfbar.admin.dao.services;

import java.util.List;

import com.rtfbar.admin.data.ContestParticipants;

public interface ContestParticipantsDAO extends RootDAO<Integer, ContestParticipants>{

	public List<ContestParticipants> getContestParticipantsByContestAndBrandId(Integer coId, Integer brandId);
	public List<ContestParticipants> getContestParticipantsByContest(Integer coId,String passcode) ;
}
