package com.rtfbar.admin.dao.services;

import java.util.List;

import com.rtfbar.admin.data.ContestQuestions;
import com.rtfbar.admin.utils.GridHeaderFilters;

public interface ContestQuestionsDAO extends RootDAO<Integer, ContestQuestions>{

	public List<ContestQuestions> getContestQuestions(GridHeaderFilters filter,Integer questionId,Integer contestId);
	public ContestQuestions getContestQuestionsByNo(Integer contestId,Integer questionNo);
	public Integer getQuestionCount(Integer contestId);
	public ContestQuestions getContestQuestionByQuestion(Integer contestId,String question);
	public List<ContestQuestions> getContestQuestionstoReposition(Integer actualPosition,Integer contestId);
	
}
