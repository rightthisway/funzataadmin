package com.rtfbar.admin.dao.services;

import com.rtfbar.admin.data.ContestWinner;

public interface ContestWinnerDAO extends RootDAO<Integer, ContestWinner>{

}
