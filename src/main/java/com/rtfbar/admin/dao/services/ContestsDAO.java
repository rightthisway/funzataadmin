package com.rtfbar.admin.dao.services;

import java.util.Date;
import java.util.List;

import com.rtfbar.admin.data.ContestGrandWinners;
import com.rtfbar.admin.data.ContestWinner;
import com.rtfbar.admin.data.Contests;
import com.rtfbar.admin.data.Customer;
import com.rtfbar.admin.data.SharedProperty;
import com.rtfbar.admin.utils.GridHeaderFilters;

public interface ContestsDAO extends RootDAO<Integer, Contests>{

	public List<Contests> getAllContests(Integer id, GridHeaderFilters filter,String status);
	public List<Contests> getAllActiveContests();
	public Contests getTodaysContest(String status);
	public boolean resetContestDataById(Integer contestId,SharedProperty sharedProperty);
	public List<Contests> getAllStartedContests();
	public List<ContestWinner> getContestWinners(Integer contestId,Integer brandId);
	public List<ContestGrandWinners> getContestGrandWinners(Integer contestId,String passcode);
	public List<Customer> getContestCustomers(Integer contestId,SharedProperty sharedProperty);
	public Contests getContestByWpId(Integer id);
	public Boolean deleteContest(String ids);
	public List<Contests> getActiveContestByDateRange(Date frDate, Date enDate); 
	public List<Contests> getDeletableContests(List<Integer> ids);
	
}
