package com.rtfbar.admin.dao.services;

import java.util.List;

import com.rtfbar.admin.data.PackageDetails;
import com.rtfbar.admin.utils.GridHeaderFilters;

public interface PackageDetailsDAO extends RootDAO<Integer, PackageDetails>{

	public List<PackageDetails> getContestParticipantBrands(Integer coId,GridHeaderFilters filter);
	public PackageDetails getPackageDetaisByBrandNameAndContestId(Integer coId,String passcode);
	public List<PackageDetails> getAllWinnersBrandsToEmail();
	public List<PackageDetails> getAllActivePackageDetailsByCoIdAndWpId(Integer coId,Integer wpId);
	
}
