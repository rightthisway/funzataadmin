package com.rtfbar.admin.dao.services;

import com.rtfbar.admin.data.Role;

public interface RoleDAO extends RootDAO<Integer, Role> {
	
	public Role getRoleByName(String roleName);
}
