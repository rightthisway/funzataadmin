package com.rtfbar.admin.dao.services;

import java.io.Serializable;
import java.util.Collection;

public interface RootDAO<K extends Serializable, T extends Serializable> {
    T get(K id);
    Collection<T> getAll();
    K save(T t);
    void update(T t);
    void delete(T t);
    void deleteById(K id);
    void saveOrUpdate(T entity);
    void saveOrUpdateAll(Collection<T> list);
    void saveAll(Collection<T> list);
    boolean updateAll(Collection<T> list);
    void deleteAll(Collection<T> list);
    void flush();
}