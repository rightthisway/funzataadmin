package com.rtfbar.admin.dao.services;

import java.util.Collection;
import java.util.List;

import com.rtfbar.admin.data.TrackerUser;
import com.rtfbar.admin.utils.GridHeaderFilters;

public interface TrackerUserDAO extends RootDAO<Integer, TrackerUser> {
	public TrackerUser getTrackerUserByUsernameOrEmail(String userName, String email);
	public TrackerUser getTrackerUserByUsernameOrEmailExceptUserId(String userName, String email, Integer userId);
	/*public Collection<TrackerUser> getAllTrackerUser(GridHeaderFilters filter, String status, String pageNo);
	public Integer getAllTrackerUserCount(GridHeaderFilters filter);*/
	public  List<TrackerUser> getAllTrackerUsersByRole(String role) ;
	public Collection<TrackerUser> getAllTrackerUsersByRole(String role, String status, GridHeaderFilters filter, String pageNo);
	public Collection<TrackerUser> getAllTrackerUsersByRoles(String role, String role1, String status, GridHeaderFilters filter,String pageNo);
	public Collection<TrackerUser> getAllTrackerUserByBrokerId(Integer brokerId);
	public Collection<TrackerUser> getAllTrackerUserExceptBroker();
}
