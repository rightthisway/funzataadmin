package com.rtfbar.admin.dao.services;

import java.util.Collection;

import com.rtfbar.admin.data.UserAction;
import com.rtfbar.admin.utils.GridHeaderFilters;

public interface UserActionDAO extends RootDAO<Integer, UserAction> {

	public Collection<UserAction> getAllActionsByUserId(Integer userId);
}
