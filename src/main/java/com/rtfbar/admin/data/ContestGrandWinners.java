package com.rtfbar.admin.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtfbar.admin.utils.Util;

@Entity
@Table(name = "contest_grand_winners")
public class ContestGrandWinners implements Serializable{
	
	private Integer id;
	private Integer contestId;
	private String prizeText;
	private Integer prizeQty;
	private Double prizeValue;
	private String userId;
	private String email;
	private String phone;
	private String barcode;
	private String brandName;
	private Boolean isWinnerEmailSent;
	private Date crDate;
	
	private String birthDate;
	private String zipcode;
	
	
	private String crDateStr;
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contestId")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="prizeText")
	public String getPrizeText() {
		return prizeText;
	}
	public void setPrizeText(String prizeText) {
		this.prizeText = prizeText;
	}
	
	@Column(name="prizeQty")
	public Integer getPrizeQty() {
		return prizeQty;
	}
	public void setPrizeQty(Integer prizeQty) {
		this.prizeQty = prizeQty;
	}
	
	@Column(name="userId")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name="prizeValue")
	public Double getPrizeValue() {
		return prizeValue;
	}
	public void setPrizeValue(Double prizeValue) {
		this.prizeValue = prizeValue;
	}
	
	@Column(name="barcode")
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
	@Column(name="isWinnerEmailSent")
	public Boolean getIsWinnerEmailSent() {
		return isWinnerEmailSent;
	}
	public void setIsWinnerEmailSent(Boolean isWinnerEmailSent) {
		this.isWinnerEmailSent = isWinnerEmailSent;
	}
	
	@Column(name="brandName")
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
	@Column(name="crDate")
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	
	@Column(name="birthDate")
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	@Column(name="zipcode")
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	@Transient
	public String getCrDateStr() {
		this.crDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(crDate);
		return this.crDateStr;
	}
	public void setCrDateStr(String crDateStr) {
		this.crDateStr = crDateStr;
	}
	
	
	
	
}
