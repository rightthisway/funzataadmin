package com.rtfbar.admin.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtfbar.admin.utils.Util;

@Entity
@Table(name="contest_participants")
public class ContestParticipants implements Serializable{
	
	private Integer id;
	private Integer coId;
	private String email;
	private String phone;
	private String userId;
	private String ageGrp;
	private String passcode;
	private Boolean isJoined;
	private String deviceType;
	private String version;
	private Date crDate;
	
	private String birthDate;
	private String zipcode;
	
	
	private String crDateStr;
	private String brandName;
	
	@Id
	@Column(name="cpID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contestId")
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name="userId")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Column(name="ageGroup")
	public String getAgeGrp() {
		return ageGrp;
	}
	public void setAgeGrp(String ageGrp) {
		this.ageGrp = ageGrp;
	}
	
	@Column(name="isJoined")
	public Boolean getIsJoined() {
		return isJoined;
	}
	public void setIsJoined(Boolean isJoined) {
		this.isJoined = isJoined;
	}
	
	@Column(name="passCode")
	public String getPasscode() {
		return passcode;
	}
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}
	
	@Column(name="crDate")
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	
	
	
	@Column(name="deviceType")
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name="version")
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	
	@Column(name="birthDate")
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	@Column(name="zipcode")
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	@Transient
	public String getCrDateStr() {
		this.crDateStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(crDate);
		return this.crDateStr;
	}
	public void setCrDateStr(String crDateStr) {
		this.crDateStr = crDateStr;
	}
	
	@Transient
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
}
