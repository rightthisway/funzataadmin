package com.rtfbar.admin.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.rtfbar.admin.utils.Util;



@Entity
@Table(name="contest_questions")
public class ContestQuestions implements Serializable{

	private Integer id;
	private Integer contestId;
	private Integer serialNo;
	private String question;
	private String optionA;
	private String optionB;
	private String optionC;
	private String answer;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;
	
	private String createdDateTimeStr;
	private String updatedDateTimeStr;
	private String correctAnswer;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contestId")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="serialNo")
	public Integer getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}
	
	@Column(name="question")
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	@Column(name="optionA")
	public String getOptionA() {
		return optionA;
	}
	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}
	
	@Column(name="optionB")
	public String getOptionB() {
		return optionB;
	}
	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}
	
	@Column(name="optionC")
	public String getOptionC() {
		return optionC;
	}
	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}
	
	@Column(name="answer")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	@Column(name="createdDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updatedDate")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="updatedBy")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Transient
	public String getCreatedDateTimeStr() {
		this.createdDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		return this.createdDateTimeStr;
	}
	public void setCreatedDateTimeStr(String createdDateTimeStr) {
		this.createdDateTimeStr = createdDateTimeStr;
	}
	
	@Transient
	public String getUpdatedDateTimeStr() {
		this.updatedDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		return this.updatedDateTimeStr;
	}
	public void setUpdatedDateTimeStr(String updatedDateTimeStr) {
		this.updatedDateTimeStr = updatedDateTimeStr;
	}
	
	@Transient
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
}
