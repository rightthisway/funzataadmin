package com.rtfbar.admin.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtfbar.admin.utils.Util;


@Entity
@Table(name="contest")
public class Contests implements Serializable{

	private Integer id;
	private String contestName;
	private Date startDateTime;
	private String prizeText;
	private Integer prizeQty;
	private Double prizeValue;
	private Integer maxWinners;
	private Integer questionSize;
	private String status;
	private Date createdDate;
	private Date updatedDate;
	private Date actualStartTime;
	private Date actualEndTime;
	private String createdBy;
	private String updatedBy;
	private Boolean isCustomerStatUpdated;
	private Integer lastQuestionNo;
	private String lastAction;
	private Boolean isPasscodeAct;
	private Integer wpId;
	private Boolean isSponPublic;
	
	private String startDateTimeStr;
	private String createdDateTimeStr;
	private String updatedDateTimeStr;
	private String minutes;
	private String hours;
	private String startDateStr;
	private String startTimeStr;
	
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="contestName")
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	

	@Column(name="startDateTime")
	public Date getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	
	@Column(name="createdDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	

	@Column(name="updatedDate")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="actualStartTime")
	public Date getActualStartTime() {
		return actualStartTime;
	}
	public void setActualStartTime(Date actualStartTime) {
		this.actualStartTime = actualStartTime;
	}
	
	@Column(name="actualEndTime")
	public Date getActualEndTime() {
		return actualEndTime;
	}
	public void setActualEndTime(Date actualEndTime) {
		this.actualEndTime = actualEndTime;
	}

	
	@Column(name="questionSize")
	public Integer getQuestionSize() {
		return questionSize;
	}
	public void setQuestionSize(Integer questionSize) {
		this.questionSize = questionSize;
	}
	
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="updatedBy")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="isCustomerStatUpdated")
	public Boolean getIsCustomerStatUpdated() {
		return isCustomerStatUpdated;
	}
	public void setIsCustomerStatUpdated(Boolean isCustomerStatUpdated) {
		this.isCustomerStatUpdated = isCustomerStatUpdated;
	}
	
	
	
	@Column(name="lastQuestionNo")
	public Integer getLastQuestionNo() {
		return lastQuestionNo;
	}
	public void setLastQuestionNo(Integer lastQuestionNo) {
		this.lastQuestionNo = lastQuestionNo;
	}
	
	@Column(name="lastAction")
	public String getLastAction() {
		return lastAction;
	}
	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}
	
	
	@Column(name="prizeText")
	public String getPrizeText() {
		return prizeText;
	}
	public void setPrizeText(String prizeText) {
		this.prizeText = prizeText;
	}
	
	@Column(name="prizeQty")
	public Integer getPrizeQty() {
		return prizeQty;
	}
	public void setPrizeQty(Integer prizeQty) {
		this.prizeQty = prizeQty;
	}
	
	@Column(name="prizeValue")
	public Double getPrizeValue() {
		return prizeValue;
	}
	public void setPrizeValue(Double prizeValue) {
		this.prizeValue = prizeValue;
	}
	
	
	@Column(name="maxWinners")
	public Integer getMaxWinners() {
		return maxWinners;
	}
	public void setMaxWinners(Integer maxWinners) {
		this.maxWinners = maxWinners;
	}
	
	@Column(name="isPasscodeAct")
	public Boolean getIsPasscodeAct() {
		return isPasscodeAct;
	}
	public void setIsPasscodeAct(Boolean isPasscodeAct) {
		this.isPasscodeAct = isPasscodeAct;
	}
	
	@Column(name="wpId")
	public Integer getWpId() {
		return wpId;
	}
	public void setWpId(Integer wpId) {
		this.wpId = wpId;
	}
	
	
	
	@Column(name="isSponPublic")
	public Boolean getIsSponPublic() {
		return isSponPublic;
	}
	public void setIsSponPublic(Boolean isSponPublic) {
		this.isSponPublic = isSponPublic;
	}
	
	
	@Transient
	public String getStartDateTimeStr() {
		this.startDateTimeStr  = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(startDateTime);
		return this.startDateTimeStr;
	}
	public void setStartDateTimeStr(String startDateTimeStr) {
		this.startDateTimeStr = startDateTimeStr;
	}
	
	
	@Transient
	public String getStartDateStr() {
		this.startDateStr = Util.formatDateToMonthDateYear(startDateTime);
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	
	@Transient
	public String getStartTimeStr() {
		if(startDateTime!=null){
			startTimeStr = Util.formatTimeToHourMinutes(startDateTime);
		}
		return startTimeStr;
	}
	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}
	
	
	@Transient
	public String getCreatedDateTimeStr() {
		this.createdDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(createdDate);
		return createdDateTimeStr;
	}
	public void setCreatedDateTimeStr(String createdDateTimeStr) {
		this.createdDateTimeStr = createdDateTimeStr;
	}

	@Transient
	public String getUpdatedDateTimeStr() {
		this.updatedDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updatedDate);
		return updatedDateTimeStr;
	}
	public void setUpdatedDateTimeStr(String updatedDateTimeStr) {
		this.updatedDateTimeStr = updatedDateTimeStr;
	}
	
	@Transient
	public String getMinutes() {
		return minutes;
	}
	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}
	
	@Transient
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	
}