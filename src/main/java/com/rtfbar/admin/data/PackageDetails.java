package com.rtfbar.admin.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtfbar.admin.utils.Util;

@Entity
@Table(name="package_details")
public class PackageDetails  implements Serializable{

	private Integer pdID;
	private Integer contestId;
	private String brandName;
	private String email;
	private String phone;
	private String firstName;
	private String lastName;
	private String passCode;
	private Integer userLimitCnt;
	private Integer actUserCnt;
	private String winnerPrize;
	private String finelistPrize;
	private String status;
	private Date crDate;
	private Date updDate;
	private String crBy;
	private String updBy;
	private Boolean isWinnerEmailSent;
	private Boolean isCancellationEmailSent;
	private Integer wpId;
	private String addr;
	
	private String crDateTimeStr;
	private String updDateTimeStr;
	
	@Id
	@Column(name="pdID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getPdID() {
		return pdID;
	}
	public void setPdID(Integer pdID) {
		this.pdID = pdID;
	}
	
	@Column(name="brandName")
	public String getBrandName() {
		return brandName;
	}
	
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
	@Column(name="passCode")
	public String getPassCode() {
		return passCode;
	}
	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}
	
	@Column(name="contestId")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="userLimitCnt")
	public Integer getUserLimitCnt() {
		return userLimitCnt;
	}
	public void setUserLimitCnt(Integer userLimitCnt) {
		this.userLimitCnt = userLimitCnt;
	}
	
	@Column(name="actUserCnt")
	public Integer getActUserCnt() {
		return actUserCnt;
	}
	public void setActUserCnt(Integer actUserCnt) {
		this.actUserCnt = actUserCnt;
	}
	
	@Column(name="winnerPrize")
	public String getWinnerPrize() {
		return winnerPrize;
	}
	public void setWinnerPrize(String winnerPrize) {
		this.winnerPrize = winnerPrize;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="crDate")
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	
	@Column(name="updDate")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name="firstName")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="lastName")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name="isWinnerEmailSent")
	public Boolean getIsWinnerEmailSent() {
		return isWinnerEmailSent;
	}
	public void setIsWinnerEmailSent(Boolean isWinnerEmailSent) {
		this.isWinnerEmailSent = isWinnerEmailSent;
	}
	
	
	@Column(name="isCancellationEmailSent")
	public Boolean getIsCancellationEmailSent() {
		return isCancellationEmailSent;
	}
	public void setIsCancellationEmailSent(Boolean isCancellationEmailSent) {
		this.isCancellationEmailSent = isCancellationEmailSent;
	}
	
	
	@Column(name="wpId")	
	public Integer getWpId() {
		return wpId;
	}
	public void setWpId(Integer wpId) {
		this.wpId = wpId;
	}
	
	
	@Column(name="crBy")	
	public String getCrBy() {
		return crBy;
	}
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}
	
	
	@Column(name="updBy")	
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	
	
	@Column(name="finelistPrize")	
	public String getFinelistPrize() {
		return finelistPrize;
	}
	public void setFinelistPrize(String finelistPrize) {
		this.finelistPrize = finelistPrize;
	}
	
	
	
	@Column(name="addr")	
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	
	@Transient
	public String getCrDateTimeStr() {
		crDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(crDate);
		return crDateTimeStr;
	}
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}
	
	@Transient
	public String getUpdDateTimeStr() {
		updDateTimeStr = Util.formatDateTimeToMonthDateYearWithTwelveHourFormat(updDate);
		return updDateTimeStr;
	}
	public void setUpdDateTimeStr(String updDateTimeStr) {
		this.updDateTimeStr = updDateTimeStr;
	}
	
}
