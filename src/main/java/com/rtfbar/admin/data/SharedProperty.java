package com.rtfbar.admin.data;

public class SharedProperty {
	
	private String baseDirectory;
	private String firebaseDatabaseUrl;
	
	public String getFirebaseDatabaseUrl() {
		return firebaseDatabaseUrl;
	}
	public void setFirebaseDatabaseUrl(String firebaseDatabaseUrl) {
		this.firebaseDatabaseUrl = firebaseDatabaseUrl;
	}
	public String getBaseDirectory() {
		return baseDirectory;
	}
	public void setBaseDirectory(String baseDirectory) {
		this.baseDirectory = baseDirectory;
	}
	
}
