package com.rtfbar.admin.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



public final class MailManager {

	public static void sendMailNow(String to, String from, String subject, String text,String imgPath) throws Exception {
		// Get the session object
		Properties props = System.getProperties();
		/*props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", 465);
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.connectiontimeout", 30000);
		props.put("mail.smtp.timeout", 30000);
		props.put("mail.smtp.socketFactory.port", 465);
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");

		Session session;
		props.put("mail.smtp.auth", "true");*/
		
		
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", 587);
		props.put("mail.transport.protocol", "smtp");
		//props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.connectiontimeout", 30000);
		props.put("mail.smtp.timeout", 30000);
		props.put("mail.smtp.socketFactory.port", 587);
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");

		Session session;
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		
		Authenticator  auth = new MailAuthenticator("support@funzata.com", "Tango#1441");
		session = Session.getInstance(props, auth);

		// compose the message
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipients(Message.RecipientType.TO, getAddresses(to));
			message.setSubject(subject);
			
			
			MimeMultipart multipart = new MimeMultipart("related");
	         BodyPart messageBodyPart = new MimeBodyPart();
	         messageBodyPart.setContent(text, "text/html");
	         multipart.addBodyPart(messageBodyPart);

	         if(imgPath!=null && !imgPath.isEmpty()){
	        	 messageBodyPart = new MimeBodyPart();
		         DataSource fds = new FileDataSource(imgPath);

		         messageBodyPart.setDataHandler(new DataHandler(fds));
		         messageBodyPart.setHeader("Content-ID", "<image>");
		         multipart.addBodyPart(messageBodyPart);
	         }
	        
	         message.setContent(multipart);
	         
			// Send message
			Transport.send(message);
			System.out.println("message sent successfully....");

		} catch (Exception mex){
			mex.printStackTrace();
			throw mex;
		}
	}

	static class MailAuthenticator extends Authenticator {
		private final String username;

		private final String password;

		public MailAuthenticator(String username, String password) {
			this.username = username;
			this.password = password;
		}

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}
	
	
	private static Address[] getAddresses(String addr) throws Exception {
		String[] recipients = addr.split(",");
		List<Address> addressList = new ArrayList<Address>();
		
		for (String recipient: recipients) {
			if (!recipient.isEmpty()) {
				addressList.add(new InternetAddress(recipient));
			}
		}
		return (Address[])addressList.toArray(new Address[addressList.size()]);
	}
}
