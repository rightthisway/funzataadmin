package com.rtfbar.admin.filters;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.rtfbar.admin.dao.impl.DAORegistry;
import com.rtfbar.admin.data.TrackerUser;
import com.rtfbar.admin.data.UserAction;

public class UserActionLogFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest)request;
		//String path = httpServletRequest.getPathInfo();
		//System.out.println(path);		
		String path = httpServletRequest.getServletPath();
		if(path == null || path.contains("Logout") || path.contains("resources")){
			// skip this and do not track this
		}else{
			TrackerUser trackerUser = (TrackerUser)httpServletRequest.getSession().getAttribute("trackerUser");
			if(trackerUser != null){
				logUserAction(trackerUser, path, httpServletRequest);
			}
		}
		chain.doFilter(request, response);
	}
	
	public boolean logUserAction(TrackerUser trackerUser, String path, HttpServletRequest request){
		try{
			String userName = request.getParameter("trackerUserName");
			String userId = request.getParameter("trackerUserId");
			String clientIPAddress = request.getParameter("clientIPAddress");
			String actionURI = request.getParameter("actionURI");			
			String userActionMsg = request.getParameter("userActionMsg");
			String dataIdStr = request.getParameter("dataId");
			
			Integer dataId = null;
			if(dataIdStr != null && !dataIdStr.isEmpty()){
				dataId = Integer.parseInt(dataIdStr);
			}
			
			UserAction userActionAudit = new UserAction();
			userActionAudit.setUserName(userName);
			userActionAudit.setUserId(Integer.parseInt(userId));
			userActionAudit.setAction(actionURI);
			userActionAudit.setIpAddress(clientIPAddress);
			userActionAudit.setTimeStamp(new Date());
			userActionAudit.setMessage(userActionMsg);
			if(dataId != null){
				userActionAudit.setDataId(dataId);
			}
			
			DAORegistry.getUserActionDAO().save(userActionAudit);
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
