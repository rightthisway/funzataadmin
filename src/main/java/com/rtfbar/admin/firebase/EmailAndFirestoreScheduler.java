package com.rtfbar.admin.firebase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfbar.admin.dao.impl.DAORegistry;
import com.rtfbar.admin.data.ContestGrandWinners;
import com.rtfbar.admin.data.Contests;
import com.rtfbar.admin.data.PackageDetails;
import com.rtfbar.admin.utils.GridHeaderFilters;
import com.rtfbar.admin.utils.Util;
import com.rtfbar.admin.wpdb.DatabaseConnection;

public class EmailAndFirestoreScheduler  extends QuartzJobBean implements StatefulJob{
	
	//PROD
	private static String FUNZATA_LOGO = "/rtw/RewardTheFan/FUNZATA_IMAGE/7_1.png";
	//LOC
	//private static String FUNZATA_LOGO = "D:/RTF_OFFICE/RTF/FUNZATA/GRAPHICS_NEW_COLOR/LOGO/7_1.png";
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		
		try {
			Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'STARTED'");
			if(contest != null){
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			contestPasscodeUploader();
		} catch (Exception e) {
			System.out.println("PASSCODE: ERROR OCCURED WHILE UPLOADING PASSCODES");
			e.printStackTrace();
		}
		try {
			//winnerEmailNotification();
		} catch (Exception e) {
			System.out.println("EMAIL: ERROR OCCURED WHILE SENDING WINNER EMAIL");
			e.printStackTrace();
		}
		try {
			importContests();
		} catch (Exception e) {
			System.out.println("CONTEST: ERROR OCCURED WHILE IMPORTING CONTEST");
			e.printStackTrace();
		}
		try {
			importPackageDetails();
		} catch (Exception e) {
			System.out.println("PACKAGEDETAILS: ERROR OCCURED WHILE IMPORTING PACKAGE DETAILS");
			e.printStackTrace();
		}
		
		try {
			createMonthlySponsor();
		} catch (Exception e) {
			System.out.println("SPONSOR : ERROR OCCURED WHILE CREATING PASSCODE FOR MONTHLY SPONSOR.");
			e.printStackTrace();
		}
	}
	
	
	
	private static void contestPasscodeUploader(){
		try {
			Date currentTime = new Date();
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, 1);
			Date after30 = cal.getTime();
			Boolean isUploaded = false;
			Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'ACTIVE','STARTED'");
			if(contest == null){
				System.out.println("PASSCODE: CONTEST NOT FOUND");
				return;
			}
			if(contest.getStatus().equalsIgnoreCase("STARTED")){
				System.out.println("PASSCODE: NOT ALLOWED TO ACTIVATE PASSCODE WHEN CONTEST STARTED.");
				return;
			}
			if(contest.getIsPasscodeAct() != null && contest.getIsPasscodeAct()){
				System.out.println("PASSCODE: PASSCODE ALREADY UPLOADED : "+contest.getId());
				return;
			}
			if(after30.after(contest.getStartDateTime())  && currentTime.before(contest.getStartDateTime())){
				List<PackageDetails> packages = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(contest.getId(), new GridHeaderFilters());
				if(packages == null || packages.isEmpty()){
					System.out.println("PASSCODE: PARTICIPANT BRANDS NOT FOUND : "+contest.getId());
					return;
				}
				isUploaded = FirestoreUtil.uploadPasscodetoFirestore(packages);
				System.out.println("PASSCODE: UPLOADED SIZE : "+contest.getId()+" | "+packages.size()+" | "+isUploaded);
				contest.setIsPasscodeAct(true);
				DAORegistry.getContestsDAO().update(contest);
				
			}
		} catch (Exception e) {
			System.out.println("PASSCODE: ERROR OCCURED WHILE UPLOADING PASSCODE TO FIRESTORE");
			e.printStackTrace();
		}
	}
	
	
	
	private static void winnerEmailNotification(){
		try {
			List<PackageDetails> packs = DAORegistry.getPackageDetailsDAO().getAllWinnersBrandsToEmail();
			if(packs == null || packs.isEmpty()){
				return;
			}
			Boolean isSent = false;
			String winnerData = "";
			for(PackageDetails pack : packs){
				List<ContestGrandWinners> winners = DAORegistry.getContestGrandWinnersDAO().getWinnersByBrandToSendEmail(pack.getPassCode());
				if(winners == null || winners.isEmpty()){
					continue;
				}
				winnerData = "";
				isSent = false;
				String winEmails = "";
				for(ContestGrandWinners win : winners){
					isSent = false;
					if(win.getEmail() == null || win.getEmail().isEmpty()){
						System.out.println("EMAIL: EMAIL IS NULL FOR WINNER : "+win.getUserId()+" | "+win.getContestId());
						continue;
					}
					//winnerData += "<tr><td align='center'>"+win.getUserId()+" </td><td>"+win.getEmail()+")</td></tr>";
					winnerData += win.getUserId()+",";
					winEmails += win.getEmail()+",";
					try {
						//MailManager.sendMailNow(win.getEmail() , "sales@rewardthefan.com", "Funzata Winner!", emailBody.toString(),FUNZATA_LOGO);
						isSent = true;
					} catch (Exception e) {
						System.out.println("EMAIL: ERROR OCCURED WHILE SENDING EMAIL TO WINNER : "+win.getEmail()+" | "+win.getContestId());
						e.printStackTrace();
					}
					
					if(isSent){
						win.setIsWinnerEmailSent(true);
						//DAORegistry.getContestGrandWinnersDAO().update(win);
					}
				}
				
				isSent = false;
				if(winnerData == null || winnerData.isEmpty()){
					continue;
				}
				winnerData = winnerData.substring(0,winnerData.lastIndexOf(","));
				StringBuffer emailBody = new StringBuffer();
				emailBody.append("<html>");
				emailBody.append("<table align='center' width='700'>");
				//emailBody.append("<tr><td align='center'><img src='cid:image'  width='300'></td></tr>");
				emailBody.append("<tr><td>");
				emailBody.append("Congratulations <b>"+winnerData+"</b> for winning today's FUZATA GAME SHOW sponsored by<b> "+pack.getBrandName()+"</b>");
				emailBody.append("<br/><br/>");
				emailBody.append("Please contact "+pack.getEmail()+" for the details on how to redeem the <b>"+pack.getBrandName()+"</b> Grand Prize Offer.");
				emailBody.append("<br/><br/>");
				emailBody.append("Thanks again for joining todays FUNZATA LIVE GAME SHOW, sponsored by <b>"+pack.getBrandName()+"</b>");
				emailBody.append("</td></tr>");
				emailBody.append("</table>");
				emailBody.append("</html>");
				
				winEmails = winEmails.substring(0,winEmails.lastIndexOf(","));
				try {
					//MailManager.sendMailNow(pack.getEmail() , "support@funzata.com", "Funzata game winner!", emailBody.toString(),"");
					//MailManager.sendMailNow(winEmails , "support@funzata.com", "Funzata game winner!", emailBody.toString(),"");
					isSent = true;
				} catch (Exception e) {
					System.out.println("EMAIL: ERROR OCCURED WHILE SENDING WINNER EMAIL TO BRAND : "+pack.getBrandName()+" | "+pack.getContestId());
					e.printStackTrace();
				}
				if(isSent){
					pack.setIsWinnerEmailSent(true);
					DAORegistry.getPackageDetailsDAO().update(pack);
					DAORegistry.getContestGrandWinnersDAO().updateAll(winners);
				}
			}
		} catch (Exception e) {
			System.out.println("EMAIL: ERROR OCCURED WHILE SENDING WINNER EMAIL");
			e.printStackTrace();
		}
	}
	
	
	
	private static void importContests(){
		ResultSet rsObj = null;
		Connection connObj = null;
		Statement stmtObj = null;
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(-1);
		try {
			connObj = DatabaseConnection.getConnection();
			stmtObj = connObj.createStatement();
			rsObj = stmtObj.executeQuery("SELECT * FROM fun_new.wp3r_custom_product WHERE product_type='Individual' order by product_date	;");
			Timestamp coTime = null;
			while (rsObj.next()) {
				Integer wpId = rsObj.getInt("id");
				if(wpId == null || wpId == 0){
					continue;
				}
				ids.add(wpId);
				Contests contest = DAORegistry.getContestsDAO().getContestByWpId(wpId);
				try {
					coTime = rsObj.getTimestamp("product_date");
				} catch (Exception e) {
					System.out.println("CONTEST : ERROR OCCURED INVALID DATE VALUE WPID:"+wpId);
					e.printStackTrace();
				}
				if(coTime == null){
					continue;
				}
				if(contest != null){
					contest.setContestName(rsObj.getString("product_name"));
					contest.setStartDateTime(new Date(coTime.getTime()));
					contest.setUpdatedDate(new Date());
				}else{
					contest = new Contests();
					contest.setContestName(rsObj.getString("product_name"));
					contest.setStartDateTime(new Date(coTime.getTime()));
					contest.setCreatedDate(new Date());
					contest.setWpId(wpId);
					contest.setCreatedBy("WP");
					contest.setIsCustomerStatUpdated(false);
					contest.setIsPasscodeAct(false);
					contest.setMaxWinners(1);
					contest.setPrizeQty(0);
					contest.setPrizeValue(0.00);
					contest.setPrizeText("N/A");
					contest.setQuestionSize(0);
					contest.setStatus("ACTIVE");
					contest.setIsSponPublic(false);
				}
				try {
					DAORegistry.getContestsDAO().saveOrUpdate(contest);
				} catch (Exception e) {
					System.out.println("CONTEST : ERROR OCCURED SAVING CONTEST WPID:"+wpId);
					e.printStackTrace();
				}
			}
			
			List<Contests> contests = DAORegistry.getContestsDAO().getDeletableContests(ids);
			for(Contests co : contests){
				List<PackageDetails> packs = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(co.getId(), new GridHeaderFilters());
				for(PackageDetails pck : packs){
					pck.setIsCancellationEmailSent(false);
					pck.setStatus("DELETED");
				}
				DAORegistry.getPackageDetailsDAO().updateAll(packs);
				co.setStatus("DELETED");
			}
			DAORegistry.getContestsDAO().updateAll(contests);
			
			//DAORegistry.getContestsDAO().deleteContest(ids);
			FirestoreUtil.updateUpcomingContestDetails();
		} catch (Exception e) {
			System.out.println("CONTEST : ERROR OCCURED WHILE IMPORTING CONTEST");
			e.printStackTrace();
		}finally{
			DatabaseConnection.closeConnection(connObj);
		}
	}
	
	
	
	
	
	private static void importPackageDetails(){
		ResultSet rsObj = null;
		Connection connObj = null;
		Statement stmtObj = null;
		try {
			
			Contests co = DAORegistry.getContestsDAO().getTodaysContest("'ACTIVE'");
			if(co == null || co.getWpId() == null){
				return;
			}
			
			List<PackageDetails> packages = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(co.getId(), new GridHeaderFilters());
			Map<Integer, PackageDetails> packMap = new HashMap<Integer, PackageDetails>();
			for(PackageDetails pack : packages){
				packMap.put(pack.getWpId(), pack);
			}
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM fun_new.wp3r_sponsors WHERE product_id="+co.getWpId());
			connObj = DatabaseConnection.getConnection();
			stmtObj = connObj.createStatement();
			rsObj = stmtObj.executeQuery(sql.toString());
			while (rsObj.next()) {
				Integer wpId = rsObj.getInt("id");
				String passcode = rsObj.getString("sponsor_id");
				String userCntStr = rsObj.getString("allowed_users");
				if(userCntStr==null || userCntStr.isEmpty() || wpId == null || passcode == null || passcode.isEmpty()){
					continue;
				}
				
				int userCnt = 0;
				try {
					if(userCntStr.contains(" ")){
						userCntStr = userCntStr.substring(0,userCntStr.indexOf(" "));
					}
					userCnt = Integer.parseInt(userCntStr);
				} catch (Exception e) {
					System.out.println("PACKAGEDETAIL : ERROR OCCURED INVALID USER LIMIT VALUE:"+userCntStr);
					e.printStackTrace();
					continue;
				}
				PackageDetails pack = packMap.get(wpId);
				if(pack!= null){
					pack.setUpdDate(new Date());
				}else{
					pack = new PackageDetails();
					pack.setIsCancellationEmailSent(false); 
					pack.setIsWinnerEmailSent(false);
					Timestamp titme = null;
					try {
						titme = rsObj.getTimestamp("create_date");
					} catch (Exception e) {
						System.out.println("PACKAGEDETAIL : ERROR OCCURED INVALID DATE IN POST");
						e.printStackTrace();
					}
					pack.setCrDate(titme!=null?new Date(titme.getTime()):null);
					pack.setActUserCnt(0);
					
				}
				
				pack.setFirstName(rsObj.getString("first_name"));
				pack.setLastName(rsObj.getString("last_name"));
				pack.setEmail(rsObj.getString("email"));
				pack.setPhone(rsObj.getString("phone"));
				pack.setAddr(rsObj.getString("address"));
				pack.setFinelistPrize(rsObj.getString("finalist_prize"));
				pack.setBrandName(rsObj.getString("sponsor_name"));
				pack.setUserLimitCnt(userCnt);
				pack.setContestId(co.getId());
				pack.setWinnerPrize(rsObj.getString("winner_text"));
				pack.setPassCode(passcode.toUpperCase());
				pack.setStatus("ACTIVE");
				pack.setWpId(wpId);
				
				try {
					DAORegistry.getPackageDetailsDAO().saveOrUpdate(pack);
				} catch (Exception e) {
					System.out.println("PACKAGEDETAIL : ERROR OCCURED WHILE SAVE/UPDATE PACKAGE DETAILS");
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("PACKAGEDETAIL : ERROR OCCURED WHILE IMPORTING PACKAGE DETAILS");
			e.printStackTrace();
		}finally{
			DatabaseConnection.closeConnection(connObj);
		}
	}
	
	
	
	
	private void createMonthlySponsor() throws Exception{
		ResultSet rsObj = null;
		Connection connObj = null;
		Statement stmtObj = null;
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, 48);
			
			Date today = new Date();
			Date tomm = cal.getTime();
			List<Contests> contests = DAORegistry.getContestsDAO().getActiveContestByDateRange(today,tomm);
			if(contests == null || contests.isEmpty()){
				return;
			}
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT p.id as pId, sp.id as spId, p.product_name as prodName, p.product_date as prodDate, p.product_type as prodType, sp.order_id as orderId, ")
			.append("sp.user_id as userId,sp.sponsor_id as sponsorId, sp.allowed_users as allowedUsers, sp.winner_text as grandPrize, sp.create_date as crDate,")
			.append("sp.first_name as first_name, sp.last_name as last_name, sp.email as email, sp.phone as phone, sp.finalist_prize as finalist_prize, sp.address as address, sp.sponsor_name as sponsor_name  ")
			.append("FROM fun_new.wp3r_sponsors sp join fun_new.wp3r_custom_product p on sp.product_id=p.id WHERE p.product_type<>'Individual' order by p.product_date");
			
			connObj = DatabaseConnection.getConnection();
			stmtObj = connObj.createStatement();
			rsObj = stmtObj.executeQuery(sql.toString());
			for(Contests co : contests){
				while (rsObj.next()) {
					Integer wpId = rsObj.getInt("spId");
					String userCntStr = rsObj.getString("allowedUsers");
					if(userCntStr==null || userCntStr.isEmpty() || wpId == null){
						continue;
					}
					
					List<PackageDetails> packs = DAORegistry.getPackageDetailsDAO().getAllActivePackageDetailsByCoIdAndWpId(co.getId(), wpId);
					if(packs!=null && !packs.isEmpty()){
						continue;
					}
					PackageDetails pack = new PackageDetails();
					int userCnt = 0;
					try {
						if(userCntStr.contains(" ")){
							userCntStr = userCntStr.substring(0,userCntStr.indexOf(" "));
						}
						userCnt = Integer.parseInt(userCntStr);
					} catch (Exception e) {
						System.out.println("SPONSOR : ERROR OCCURED INVALID USER LIMIT VALUE:"+userCntStr);
						e.printStackTrace();
						continue;
					}
					
					Timestamp titme = null;
					try {
						titme = rsObj.getTimestamp("crDate");
					} catch (Exception e) {
						System.out.println("SPONSOR : ERROR OCCURED INVALID DATE IN POST");
						e.printStackTrace();
					}
					
					pack.setCrDate(titme!=null?new Date(titme.getTime()):null);
					pack.setActUserCnt(0);
					pack.setUserLimitCnt(userCnt);
					pack.setContestId(co.getId());
					pack.setIsWinnerEmailSent(false);
					pack.setWinnerPrize(rsObj.getString("grandPrize"));
					pack.setStatus("ACTIVE");
					pack.setWpId(wpId);
					pack.setIsCancellationEmailSent(false); 
					pack.setFirstName(rsObj.getString("first_name"));
					pack.setLastName(rsObj.getString("last_name"));
					pack.setEmail(rsObj.getString("email"));
					pack.setPhone(rsObj.getString("phone"));
					pack.setAddr(rsObj.getString("address"));
					pack.setFinelistPrize(rsObj.getString("finalist_prize"));
					pack.setBrandName(rsObj.getString("sponsor_name"));
					
					try {
						pack.setPassCode(Util.getAlphaNumericString(6));
						DAORegistry.getPackageDetailsDAO().saveOrUpdate(pack);
					} catch (Exception e) {
						System.out.println("SPONSOR : ERROR OCCURED WHILE SAVE/UPDATE PACKAGE DETAILS");
						e.printStackTrace();
						return;
					}
					
					
					StringBuffer emailBody = new StringBuffer();
					emailBody.append("<html>");
					emailBody.append("<table align='center' width='700'>");
					//emailBody.append("<tr><td align='center'><img src='cid:image'  width='300'></td></tr>");
					emailBody.append("<tr><td>");
					emailBody.append("Hello "+pack.getBrandName()+",<br/>");
					emailBody.append("<br/><br/>");
					emailBody.append("Here is your SPONSOR ID : <b>"+pack.getPassCode()+"</b> for <b> "+co.getContestName() +"</b> Scheduled on <b>"+ co.getStartDateTimeStr()+"</b>");
					emailBody.append("<br/><br/>");
					emailBody.append("Thanks you for joining FUNZATA LIVE GAME SHOW.");
					emailBody.append("</td></tr>");
					emailBody.append("</table>");
					emailBody.append("</html>");
					
					try {
						//MailManager.sendMailNow(pack.getEmail() , "sales@rewardthefan.com", "Funzata game Winner!", emailBody.toString(),"");
					} catch (Exception e) {
						System.out.println("SPONSOR: ERROR OCCURED WHILE SENDING SPONSOR ID EMAIL : "+pack.getBrandName()+" | "+pack.getContestId());
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			throw e;
		}finally{
			DatabaseConnection.closeConnection(connObj);
		}
	}

}
