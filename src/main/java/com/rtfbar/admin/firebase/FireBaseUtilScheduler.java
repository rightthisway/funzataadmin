package com.rtfbar.admin.firebase;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class FireBaseUtilScheduler  extends QuartzJobBean implements StatefulJob{

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			System.out.println("UPDATING UPCOMING CONTEST : "+new Date());
			FirestoreUtil.updateUpcomingContestDetails();
			System.out.println("UPCOMING CONTEST UPDATED : "+new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
