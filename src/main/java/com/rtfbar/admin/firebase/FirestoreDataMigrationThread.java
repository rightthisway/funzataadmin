package com.rtfbar.admin.firebase;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.rtfbar.admin.data.Contests;

public class FirestoreDataMigrationThread implements Runnable{
	
	private Contests co = null;
	private Map<Integer, String> ansMap = new HashMap<Integer, String>();
	public FirestoreDataMigrationThread(Contests co,Map<Integer, String> ansMap){
		this.co = co;
		this.ansMap = ansMap;
	}

	@Override
	public void run() {
		try {
			ExecutorService pool = Executors.newFixedThreadPool(3);
			
			CompletableFuture<Void> participantFuture = CompletableFuture.runAsync(() -> FirestoreUtil.migrateParticipantsData(co),pool);
			CompletableFuture<Void> answerFuture = CompletableFuture.runAsync(() -> FirestoreUtil.migrateCustomerAnswerData(co, ansMap),pool);
			CompletableFuture<Void> winnerFuture = CompletableFuture.runAsync(() -> FirestoreUtil.migrateWinnersData(co),pool);
			CompletableFuture<Void> combinedFuture  = CompletableFuture.allOf(participantFuture, answerFuture,winnerFuture);
			try {
				combinedFuture.get();
			} catch (Exception e) {
				System.err.println("MIGRATION: MIGRATING FIRESTORE DATA.");
				e.printStackTrace();
			}finally {
				pool.shutdown();
			}
			//FirestoreUtil.migrateParticipantsData(co);
		} catch (Exception e) {
			System.out.println("MIGRATION: ERROR OCCURED WHILE PARTICIPANT DATA MIGRATION");
			e.printStackTrace();
		}
		
		/*try {
			FirestoreUtil.migrateCustomerAnswerData(co, ansMap);
			//FirestoreUtil.clearAnswerCounts(co.getQuestionSize());
		} catch (Exception e) {
			System.out.println("MIGRATION: ERROR OCCURED WHILE ANSWER DATA MIGRATION");
			e.printStackTrace();
		}*/
	}

}
