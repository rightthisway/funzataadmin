package com.rtfbar.admin.firebase;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.Model;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.rtfbar.admin.dao.impl.DAORegistry;
import com.rtfbar.admin.data.ContestGrandWinners;
import com.rtfbar.admin.data.ContestParticipants;
import com.rtfbar.admin.data.ContestQuestions;
import com.rtfbar.admin.data.ContestWinner;
import com.rtfbar.admin.data.Contests;
import com.rtfbar.admin.data.CustomerAnswer;
import com.rtfbar.admin.data.PackageDetails;
import com.rtfbar.admin.data.SharedProperty;
import com.rtfbar.admin.utils.GridHeaderFilters;
import com.rtfbar.admin.utils.GridHeaderFiltersUtil;
import com.rtfbar.admin.utils.Util;

public class FirestoreUtil implements InitializingBean {

	public static Integer failedReqCnt = 0;
	public static Boolean isContestStarted = false;
	private static Firestore firestore = null; 
	private static CollectionReference rtfCollection = null;
	private static CollectionReference hostCollection = null;
	private static CollectionReference grandWinners = null;
	private static DocumentReference questionRef = null;
	private static DocumentReference contestDetails = null;;
	private static DocumentReference contestInfo = null;
	private static DocumentReference updateDashbord = null;
	
	//private static CollectionReference answerCounts = null;
	//private static CollectionReference participantList = null;
	
	public static List<String> list = new ArrayList<String>();
	
	
	public static Map<Integer,Map<String, List<ContestGrandWinners>>> grandWinnerMap = new HashMap<Integer,Map<String, List<ContestGrandWinners>>>();
	public static Map<String, Object> staticMap =new HashMap<String, Object>();
	
	private static Logger fireStoreLog = LoggerFactory.getLogger(FirestoreUtil.class);

	public SharedProperty sharedProperty;

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		initialiseFirebase();

	}
	
	public static  Map<String, List<ContestGrandWinners>> getGrandWinnerMap(Integer coId){
		try {
			if(grandWinnerMap.get(coId) == null){
				List<ContestGrandWinners> gWinners = DAORegistry.getContestGrandWinnersDAO().getGrandWinnersByContestId(coId,new GridHeaderFilters());
				List<ContestGrandWinners> list = null;
				Map<String, List<ContestGrandWinners>> map = new HashMap<String, List<ContestGrandWinners>>();
				for(ContestGrandWinners win : gWinners){
					if(map.get(win.getBarcode()) ==null){
						list = new ArrayList<ContestGrandWinners>();
						list.add(win);
						map.put(win.getBarcode(), list);
					}else{
						list = map.get(win.getBarcode());
						list.add(win);
						map.put(win.getBarcode(), list);
					}
				}
				grandWinnerMap.put(coId, map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return grandWinnerMap.get(coId);
	}

	public static void updateNextQuestion(ContestQuestions q, Model model) {
		Date timeStamp = new Date();
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			questionRef = rtfCollection.document("Question");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", q.getId());
			qMap.put("qNo", q.getSerialNo());
			qMap.put("text", q.getQuestion());
			qMap.put("optionA", q.getOptionA());
			qMap.put("optionB", q.getOptionB());
			qMap.put("optionC", q.getOptionC());
			qMap.put("answer", q.getAnswer());
			qMap.put("timeStamp", timeStamp.getTime());
			qMap.put("cAns", q.getCorrectAnswer());

			questionRef.set(qMap);
			fireStoreLog.info("CONTEST " + q.getAnswer() + ":  --qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("msg", "ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo());
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo() + " -- " + q.getAnswer());
			e.printStackTrace();
		}
	}

	

	public static void updateLiveStreamUrl(String url) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			contestInfo = rtfCollection.document("dashboardInfoNew");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("videoUrl", url);
			contestInfo.update(vMap);
			fireStoreLog.info("LIVE STREAM URL UPDATED: " + url);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING LIVE STREAM URL.");
		}
	}

	public static void setContestStarted(Boolean isStarted) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			contestInfo = rtfCollection.document("dashboardInfoNew");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("isContestStarted", isStarted);
			isContestStarted = isStarted;
			contestInfo.update(vMap);
			// setHoulryGameEnabled(false);
			fireStoreLog.info("CONTEST STARTED/STOPPED: " + isStarted);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING CONTEST STARTED FLAG.");
		}
	}

	public static void setContestDetails(Contests c, Model model) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			contestDetails = rtfCollection.document("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", 0);
			cMap.put("contestName", c.getContestName());
			cMap.put("prizeText", c.getPrizeText());
			cMap.put("prizeQty", c.getPrizeQty());
			cMap.put("questionSize", c.getQuestionSize());
			cMap.put("prizeVal", c.getPrizeValue());
			cMap.put("id", c.getId());
			contestDetails.set(cMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("CONTEST DETAILS UPDATED: -- coid: " + c.getId());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE SETTING CONTEST DETAILS.");
			fireStoreLog.error("ERROR OCCURED WHILE SETTING CONTEST DETAILS.");
			e.printStackTrace();
		}
	}

	public static void updateContestDetailsQNo(ContestQuestions q, Model model) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			contestDetails = rtfCollection.document("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", q.getSerialNo());
			contestDetails.update(cMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("UPDATED CONTEST DETAILS: -- qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE UPDATING CONTEST DETAILS QUESTION NO.");
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING CONTEST DETAILS QUESTION NO.");
			e.printStackTrace();
		}
	}

	
	public static Boolean declareGrandWinners(Contests co){
		List<ContestGrandWinners> allGrandWinners = new ArrayList<ContestGrandWinners>();
		Map<String,List<ContestGrandWinners>> gMap = new HashMap<String,List<ContestGrandWinners>>();
		Date today = new Date();
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			List<ContestWinner> allWinners = new ArrayList<ContestWinner>();
			Map<String,List<ContestWinner>> sumMap = new HashMap<String,List<ContestWinner>>();
			
			ApiFuture<QuerySnapshot> future =  firestore.collection("summary").get();
			List<QueryDocumentSnapshot> documents = future.get().getDocuments();
			
			for(QueryDocumentSnapshot doc  :documents){
				List<ContestWinner> sumWinners = new ArrayList<ContestWinner>();
				ApiFuture<QuerySnapshot> users =  firestore.collection("summary").document(doc.getId()).collection("users").get();
				List<QueryDocumentSnapshot> userDocs = users.get().getDocuments();
				for(QueryDocumentSnapshot d  :userDocs){
					ContestWinner win = new ContestWinner();
					win.setContestId(co.getId());
					win.setEmail(String.valueOf(d.get("email")));
					win.setUserId(d.getString("username"));
					win.setPhone(d.getString("phone"));
					win.setBarcode(doc.getId());
					win.setCrDate(today);
					sumWinners.add(win);
				}
				sumMap.put(doc.getId(), sumWinners);
				allWinners.addAll(sumWinners);
			}
			
			DAORegistry.getContestWinnerDAO().saveAll(allWinners);
			
			List<PackageDetails> packages = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(co.getId(), new GridHeaderFilters());
			Map<String,PackageDetails> packMap = new HashMap<String,PackageDetails>();
			if(packages!=null && !packages.isEmpty()){
				for(PackageDetails pack : packages){
					packMap.put(pack.getBrandName(), pack);
				}
			}
			
			Random rand = new Random();	
			List<ContestGrandWinners> gWinners = null;
			for(Entry<String, List<ContestWinner>> map : sumMap.entrySet()){
				List<ContestWinner> list = map.getValue();
				gWinners = new ArrayList<ContestGrandWinners>();
				if(list == null || list.isEmpty()){
					continue;
				}
				for(int i=0;i<co.getMaxWinners();i++){
					if(list.isEmpty()){
						continue;
					}
					int randomIndex = rand.nextInt(list.size());
					ContestWinner winner = list.get(randomIndex);
					
					ContestGrandWinners grandWin = new ContestGrandWinners();
					grandWin.setContestId(co.getId());
					grandWin.setEmail(winner.getEmail());
					grandWin.setPhone(winner.getPhone());
					grandWin.setUserId(winner.getUserId());
					grandWin.setIsWinnerEmailSent(false);
					grandWin.setCrDate(today);
					PackageDetails pack = packMap.get(winner.getBarcode());
					if(pack!=null){
						grandWin.setBarcode(pack.getPassCode());
						grandWin.setPrizeText(pack.getWinnerPrize());
						grandWin.setBrandName(pack.getBrandName());
					}
					//grandWin.setPrizeQty(co.getPrizeQty());
					//grandWin.setPrizeValue(co.getPrizeValue());
					gWinners.add(grandWin);
					list.remove(randomIndex);
				}
				gMap.put(map.getKey(), gWinners);
			}
			if(gWinners!=null){
				allGrandWinners.addAll(gWinners);
			}
			
			grandWinnerMap.put(co.getId(), gMap);
			DAORegistry.getContestGrandWinnersDAO().saveAll(allGrandWinners);
			return true;
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE CALCULATING GRAND WINNERS.");
			e.printStackTrace();
		}
		return false;
	}

	public static void setGrandWinners(Map<String,List<ContestGrandWinners>> winMap) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			grandWinners =  firestore.collection("grandwinner");
			
			if (winMap != null && !winMap.isEmpty()) {
				Map<String, Object> sMap = null;
				for(Entry<String,List<ContestGrandWinners>> win : winMap.entrySet()){
					List<ContestGrandWinners> winners = win.getValue();
					for(ContestGrandWinners w : winners){
						sMap = new HashMap<String, Object>();
						sMap.put("coId",w.getContestId());
						//sMap.put("cuId",w.getCustomerId());
						sMap.put("prizeQty", w.getPrizeQty());
						sMap.put("prizeVal", w.getPrizeValue());
						sMap.put("prizeText", w.getPrizeText());
						sMap.put("uId", w.getUserId());
						grandWinners.document(win.getKey()).set(staticMap);
						grandWinners.document(win.getKey()).collection("users").document().set(sMap);
					}
				}
				fireStoreLog.info("GRAND WINNERS UPDATED.");
			} else {
				fireStoreLog.info("NO GRAND WINNERS FOUND.");
			}
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED UPDATING GRAND WINNERS.");
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		/*Contests co = new Contests();
		co.setId(1);
		//declareGrandWinners(co);
		List<ContestGrandWinners> winners = new ArrayList<ContestGrandWinners>();
		ContestGrandWinners grandWin = new ContestGrandWinners();
		grandWin.setBarcode("ABCD");
		grandWin.setContestId(1);
		grandWin.setEmail("eemail1@gmail.com");
		grandWin.setPhone("868786868");
		grandWin.setUserId("mituk");
		grandWin.setPrizeQty(0);
		grandWin.setPrizeText("AMAZON GIFTCARD");
		grandWin.setPrizeValue(100.00);
		winners.add(grandWin);
		
		Map<String,List<ContestGrandWinners>> winMap = new HashMap<String,List<ContestGrandWinners>>();
		winMap.put("ABCD", winners);*/
		//setGrandWinners(winMap);
		clearAnswerCounts(10);
		//testAnswerLoad(1);
	}

	public static void endContest(Model model,String status) {
		Date timeStamp = new Date();
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			questionRef = rtfCollection.document("Question");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", 0);
			qMap.put("qNo", 0);
			qMap.put("text", "");
			qMap.put("cAns", "");
			qMap.put("optionA", "");
			qMap.put("optionB", "");
			qMap.put("optionC", "");
			qMap.put("answer", status);
			qMap.put("timeStamp", timeStamp.getTime());
			
			
			contestDetails = rtfCollection.document("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", "");
			cMap.put("contestName", "");
			cMap.put("questionSize", "");
			cMap.put("prizeText", "");
			cMap.put("prizeQty",0);
			cMap.put("prizeVal", 0);
			cMap.put("id",0);
			
			questionRef.set(qMap);
			contestDetails.set(cMap);
			
			ApiFuture<QuerySnapshot> userList =  firestore.collection("userList").get();
			List<QueryDocumentSnapshot> userDocs = userList.get().getDocuments();
			for(QueryDocumentSnapshot doc  :userDocs){
				DocumentReference docRef = firestore.collection("userList").document(doc.getId());
				ApiFuture<QuerySnapshot> userColl = docRef.collection("users").get();
				List<QueryDocumentSnapshot> uDocs = userColl.get().getDocuments();
				for(QueryDocumentSnapshot uDoc : uDocs){
					firestore.collection("userList").document(doc.getId()).collection("users").document(uDoc.getId()).delete();
				}
				firestore.collection("userList").document(doc.getId()).delete();
			}
			
			if(status.equalsIgnoreCase("END")){
				ApiFuture<QuerySnapshot> passCodeFuture =  firestore.collection("passcodes").get();
				List<QueryDocumentSnapshot> passCodeDocs = passCodeFuture.get().getDocuments();
				for(QueryDocumentSnapshot doc  :passCodeDocs){
					String barName = doc.getString("barname");
					if(barName != null && barName.equalsIgnoreCase("APPLE555")){
						continue;
					}
					firestore.collection("passcodes").document(doc.getId()).delete();
				}
			}
			model.addAttribute("status", 1);
			model.addAttribute("msg", "Contest ended sucessfully.");
			updateHostTracker("END");
			cleanupHostNode();
			fireStoreLog.info("CONTEST ENDED.");
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE ENDING CONTEST.");
			fireStoreLog.error("ERROR OCCURED WHILE ENDING CONTEST.");
			e.printStackTrace();
		}
	}
	
	
	
	public static boolean migrateParticipantsData(Contests co){
		boolean isMigrated = false;
		Date today = new Date();
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			Map<String, List<ContestParticipants>> partMap = new HashMap<String,List<ContestParticipants>>();
			
			ApiFuture<QuerySnapshot> authListFuture =  firestore.collection("AuthUserList").get();
			List<QueryDocumentSnapshot> authDocs = authListFuture.get().getDocuments();
			for(QueryDocumentSnapshot doc  :authDocs){
				List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
				ApiFuture<QuerySnapshot> users =  firestore.collection("AuthUserList").document(doc.getId()).collection("users").get();
				List<QueryDocumentSnapshot> userDocs = users.get().getDocuments();
				for(QueryDocumentSnapshot d  :userDocs){
					ContestParticipants cp = new ContestParticipants();
					cp.setCoId(co.getId());
					cp.setEmail(d.getString("email"));
					cp.setUserId(d.getString("username"));
					cp.setPhone(d.getString("phone"));
					cp.setPasscode(doc.getId());
					//cp.setAgeGrp(d.getString("ageGroup"));
					cp.setBirthDate(d.getString("dob"));
					cp.setZipcode(d.getString("zip"));
					cp.setDeviceType(d.getString("deviceType"));
					cp.setVersion(d.getString("version"));
					cp.setIsJoined(false);
					cp.setCrDate(today);
					participants.add(cp);
				}
				partMap.put(doc.getId(), participants);
			}
			
			
			
			ApiFuture<QuerySnapshot> participantFuture =  firestore.collection("ParticipantList").get();
			List<QueryDocumentSnapshot> partDocs = participantFuture.get().getDocuments();
			for(QueryDocumentSnapshot doc  :partDocs){
				List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
				ApiFuture<QuerySnapshot> users =  firestore.collection("ParticipantList").document(doc.getId()).collection("users").get();
				List<QueryDocumentSnapshot> userDocs = users.get().getDocuments();
				for(QueryDocumentSnapshot d  :userDocs){
					String email = d.getString("email");
					participants = partMap.get(doc.getId());
					if(participants == null){
						continue;
					}
					boolean flag = false;
					for(ContestParticipants parti : participants){
						if(email.equalsIgnoreCase(parti.getEmail())){
							parti.setIsJoined(true);
							flag = true;
							break;
						}
					}
					if(!flag){
						ContestParticipants cp = new ContestParticipants();
						cp.setCoId(co.getId());
						cp.setEmail(d.getString("email"));
						cp.setUserId(d.getString("username"));
						cp.setPhone(d.getString("phone"));
						cp.setPasscode(doc.getId());
						//cp.setAgeGrp(d.getString("ageGroup"));
						cp.setBirthDate(d.getString("dob"));
						cp.setZipcode(d.getString("zip"));
						cp.setIsJoined(true);
						cp.setCrDate(today);
						participants.add(cp);
					}
					partMap.put(doc.getId(), participants);
				}
			}
			
			try {
				for(Entry<String, List<ContestParticipants>> map : partMap.entrySet()){
					DAORegistry.getContestParticipantsDAO().saveAll(map.getValue());
				}
				isMigrated = true;
				
				
				/*
				ApiFuture<QuerySnapshot> userList =  firestore.collection("AuthUserList").get();
				List<QueryDocumentSnapshot> userDocs = userList.get().getDocuments();
				for(QueryDocumentSnapshot doc  :userDocs){
					DocumentReference docRef = firestore.collection("AuthUserList").document(doc.getId());
					ApiFuture<QuerySnapshot> userColl = docRef.collection("users").get();
					List<QueryDocumentSnapshot> uDocs = userColl.get().getDocuments();
					for(QueryDocumentSnapshot uDoc : uDocs){
						firestore.collection("AuthUserList").document(doc.getId()).collection("users").document(uDoc.getId()).delete();
					}
					firestore.collection("AuthUserList").document(doc.getId()).delete();
				}
				
				
				ApiFuture<QuerySnapshot> partiFuture =  firestore.collection("ParticipantList").get();
				List<QueryDocumentSnapshot> partiDocs = partiFuture.get().getDocuments();
				for(QueryDocumentSnapshot doc  :partiDocs){
					DocumentReference docRef = firestore.collection("ParticipantList").document(doc.getId());
					ApiFuture<QuerySnapshot> userColl = docRef.collection("users").get();
					List<QueryDocumentSnapshot> uDocs = userColl.get().getDocuments();
					for(QueryDocumentSnapshot uDoc : uDocs){
						firestore.collection("ParticipantList").document(doc.getId()).collection("users").document(uDoc.getId()).delete();
					}
					firestore.collection("ParticipantList").document(doc.getId()).delete();
				}*/
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isMigrated;
	}
	
	
	
	public static void clearWinnersData(){
		try {
			//REMOVE SUMMARY AND WINNER DATA FROM FIREASTORE
			ApiFuture<QuerySnapshot> summary =  firestore.collection("summary").get();
			List<QueryDocumentSnapshot> summaryDocs = summary.get().getDocuments();
			for(QueryDocumentSnapshot doc  :summaryDocs){
				DocumentReference docRef = firestore.collection("summary").document(doc.getId());
				ApiFuture<QuerySnapshot> sumColl = docRef.collection("users").get();
				List<QueryDocumentSnapshot> sumDocs = sumColl.get().getDocuments();
				for(QueryDocumentSnapshot sumDoc : sumDocs){
					firestore.collection("summary").document(doc.getId()).collection("users").document(sumDoc.getId()).delete();
				}
				firestore.collection("summary").document(doc.getId()).delete();
			}
			
			ApiFuture<QuerySnapshot> winner =  firestore.collection("grandwinner").get();
			List<QueryDocumentSnapshot> winnerDocs = winner.get().getDocuments();
			for(QueryDocumentSnapshot doc  :winnerDocs){
				DocumentReference docRef = firestore.collection("grandwinner").document(doc.getId());
				ApiFuture<QuerySnapshot> winColl = docRef.collection("users").get();
				List<QueryDocumentSnapshot> winDocs = winColl.get().getDocuments();
				for(QueryDocumentSnapshot winDoc : winDocs){
					firestore.collection("grandwinner").document(doc.getId()).collection("users").document(winDoc.getId()).delete();
				}
				firestore.collection("grandwinner").document(doc.getId()).delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public static void migrateCustomerAnswerData(Contests co, Map<Integer, String> ansMap) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			
			List<CustomerAnswer> answers = new ArrayList<CustomerAnswer>();
			CustomerAnswer ans = null;
			
			ApiFuture<QuerySnapshot> authListFuture =  firestore.collection("answerCounts").get();
			List<QueryDocumentSnapshot> authDocs = authListFuture.get().getDocuments();
			for(QueryDocumentSnapshot doc  :authDocs){
				DocumentReference docRef = firestore.collection("answerCounts").document(doc.getId());
				for(int i=1;i<=co.getQuestionSize();i++){
					String crtAns = ansMap.get(i);
					Boolean isCrt = false;
					answers.clear();
					ApiFuture<QuerySnapshot> aAnswers = docRef.collection(i+" A").get();
					List<QueryDocumentSnapshot> aDocs = aAnswers.get().getDocuments();
					if("A".equalsIgnoreCase(crtAns)){
						isCrt = true;
					}
					for(QueryDocumentSnapshot aDoc : aDocs){
						ans = new CustomerAnswer();
						ans.setAnswer("A");
						ans.setCoId(co.getId());
						ans.setIsCrt(isCrt);
						ans.setPasscode(doc.getId());
						ans.setqNo(i);
						ans.setUserId(aDoc.getString("username"));
						answers.add(ans);
					}
					
					DAORegistry.getCustomerAnswerDAO().saveAll(answers);
					
					ApiFuture<QuerySnapshot> bAnswers = docRef.collection(i+" B").get();
					List<QueryDocumentSnapshot> bDocs = bAnswers.get().getDocuments();
					answers.clear();
					isCrt = false;
					if("B".equalsIgnoreCase(crtAns)){
						isCrt = true;
					}
					for(QueryDocumentSnapshot bDoc : bDocs){
						ans = new CustomerAnswer();
						ans.setAnswer("B");
						ans.setCoId(co.getId());
						ans.setIsCrt(isCrt);
						ans.setPasscode(doc.getId());
						ans.setqNo(i);
						ans.setUserId(bDoc.getString("username"));
						answers.add(ans);
					}
					
					DAORegistry.getCustomerAnswerDAO().saveAll(answers);
						
					
					ApiFuture<QuerySnapshot> cAnswers = docRef.collection(i+" C").get();
					List<QueryDocumentSnapshot> cDocs = cAnswers.get().getDocuments();
					answers.clear();
					isCrt = false;
					if("C".equalsIgnoreCase(crtAns)){
						isCrt = true;
					}
					for(QueryDocumentSnapshot cDoc : cDocs){
						ans = new CustomerAnswer();
						ans.setAnswer("C");
						ans.setCoId(co.getId());
						ans.setIsCrt(isCrt);
						ans.setPasscode(doc.getId());
						ans.setqNo(i);
						ans.setUserId(cDoc.getString("username"));
						answers.add(ans);
					}
					
					DAORegistry.getCustomerAnswerDAO().saveAll(answers);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	public static void cleanParticipantData(){
		try {
			ApiFuture<QuerySnapshot> authFuture =  firestore.collection("AuthUserList").get();
			List<QueryDocumentSnapshot> authDocs = authFuture.get().getDocuments();
			for(QueryDocumentSnapshot doc  :authDocs){
				DocumentReference docRef = firestore.collection("AuthUserList").document(doc.getId());
				ApiFuture<QuerySnapshot> authColl = docRef.collection("users").get();
				List<QueryDocumentSnapshot> authSubDocs = authColl.get().getDocuments();
				for(QueryDocumentSnapshot authSubDoc : authSubDocs){
					firestore.collection("AuthUserList").document(doc.getId()).collection("users").document(authSubDoc.getId()).delete();
				}
				firestore.collection("AuthUserList").document(doc.getId()).delete();
			}
			
			
			
			ApiFuture<QuerySnapshot> partiFuture =  firestore.collection("ParticipantList").get();
			List<QueryDocumentSnapshot> partiDocs = partiFuture.get().getDocuments();
			for(QueryDocumentSnapshot doc  :partiDocs){
				DocumentReference docRef = firestore.collection("ParticipantList").document(doc.getId());
				ApiFuture<QuerySnapshot> partiColl = docRef.collection("users").get();
				List<QueryDocumentSnapshot> partiSubDocs = partiColl.get().getDocuments();
				for(QueryDocumentSnapshot partiSubDoc : partiSubDocs){
					firestore.collection("ParticipantList").document(doc.getId()).collection("users").document(partiSubDoc.getId()).delete();
				}
				firestore.collection("ParticipantList").document(doc.getId()).delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public static void clearAnswerCounts(Integer qSize) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			ApiFuture<QuerySnapshot> answerCounts =  firestore.collection("answerCounts").get();
			List<QueryDocumentSnapshot> answerCountsDocs = answerCounts.get().getDocuments();
			for(QueryDocumentSnapshot doc  :answerCountsDocs){
				DocumentReference docRef = firestore.collection("answerCounts").document(doc.getId());
				for(int i=1;i<=qSize;i++){
					ApiFuture<QuerySnapshot> aAnswers = docRef.collection(i+" A").get();
					List<QueryDocumentSnapshot> aDocs = aAnswers.get().getDocuments();
					for(QueryDocumentSnapshot aDoc : aDocs){
						firestore.collection("answerCounts").document(doc.getId()).collection(i+" A").document(aDoc.getId()).delete();
					}
					
					ApiFuture<QuerySnapshot> bAnswers = docRef.collection(i+" B").get();
					List<QueryDocumentSnapshot> bDocs = bAnswers.get().getDocuments();
					for(QueryDocumentSnapshot bDoc : bDocs){
						firestore.collection("answerCounts").document(doc.getId()).collection(i+" B").document(bDoc.getId()).delete();
					}
					
					ApiFuture<QuerySnapshot> cAnswers = docRef.collection(i+" C").get();
					List<QueryDocumentSnapshot> cDocs = cAnswers.get().getDocuments();
					for(QueryDocumentSnapshot cDoc : cDocs){
						firestore.collection("answerCounts").document(doc.getId()).collection(i+" C").document(cDoc.getId()).delete();
					}
				}
				 firestore.collection("answerCounts").document(doc.getId()).delete();
			}
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE CLEARING ANSWER COUNT COLLECTION.");
			e.printStackTrace();
		}
	}
	
	
	public static Boolean migrateWinnersData(Contests co){
		List<ContestGrandWinners> allGrandWinners = new ArrayList<ContestGrandWinners>();
		Map<String,List<ContestGrandWinners>> gMap = new HashMap<String,List<ContestGrandWinners>>();
		Date today = new Date();
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			List<ContestWinner> allWinners = new ArrayList<ContestWinner>();
			Map<String,List<ContestWinner>> sumMap = new HashMap<String,List<ContestWinner>>();
			
			ApiFuture<QuerySnapshot> future =  firestore.collection("summary").get();
			List<QueryDocumentSnapshot> documents = future.get().getDocuments();
			
			for(QueryDocumentSnapshot doc  :documents){
				List<ContestWinner> sumWinners = new ArrayList<ContestWinner>();
				ApiFuture<QuerySnapshot> users =  firestore.collection("summary").document(doc.getId()).collection("users").get();
				List<QueryDocumentSnapshot> userDocs = users.get().getDocuments();
				for(QueryDocumentSnapshot d  :userDocs){
					ContestWinner win = new ContestWinner();
					win.setContestId(co.getId());
					win.setEmail(String.valueOf(d.get("email")));
					win.setUserId(d.getString("username"));
					win.setPhone(d.getString("phone"));
					win.setBirthDate(d.getString("dob"));
					win.setZipcode(d.getString("zip"));
					win.setBarcode(doc.getId());
					win.setCrDate(today);
					sumWinners.add(win);
					
				}
				sumMap.put(doc.getId(), sumWinners);
				allWinners.addAll(sumWinners);
			}
			
			DAORegistry.getContestWinnerDAO().saveAll(allWinners);
			
			List<PackageDetails> packages = DAORegistry.getPackageDetailsDAO().getContestParticipantBrands(co.getId(), new GridHeaderFilters());
			Map<String,PackageDetails> packMap = new HashMap<String,PackageDetails>();
			if(packages!=null && !packages.isEmpty()){
				for(PackageDetails pack : packages){
					packMap.put(pack.getBrandName(), pack);
				}
			}
			
			Random rand = new Random();	
			List<ContestGrandWinners> gWinners = null;
			for(Entry<String, List<ContestWinner>> map : sumMap.entrySet()){
				List<ContestWinner> list = map.getValue();
				gWinners = new ArrayList<ContestGrandWinners>();
				if(list == null || list.isEmpty()){
					continue;
				}
				for(int i=0;i<co.getMaxWinners();i++){
					if(list.isEmpty()){
						continue;
					}
					int randomIndex = rand.nextInt(list.size());
					ContestWinner winner = list.get(randomIndex);
					
					ContestGrandWinners grandWin = new ContestGrandWinners();
					grandWin.setContestId(co.getId());
					grandWin.setEmail(winner.getEmail());
					grandWin.setPhone(winner.getPhone());
					grandWin.setUserId(winner.getUserId());
					grandWin.setBirthDate(winner.getBirthDate());
					grandWin.setZipcode(winner.getZipcode());
					grandWin.setIsWinnerEmailSent(false);
					grandWin.setCrDate(today);
					PackageDetails pack = packMap.get(winner.getBarcode());
					if(pack!=null){
						grandWin.setBarcode(pack.getPassCode());
						grandWin.setPrizeText(pack.getWinnerPrize());
						grandWin.setBrandName(pack.getBrandName());
					}
					//grandWin.setPrizeQty(co.getPrizeQty());
					//grandWin.setPrizeValue(co.getPrizeValue());
					gWinners.add(grandWin);
					list.remove(randomIndex);
				}
				gMap.put(map.getKey(), gWinners);
			}
			if(gWinners!=null){
				allGrandWinners.addAll(gWinners);
			}
			DAORegistry.getContestGrandWinnersDAO().saveAll(allGrandWinners);
			return true;
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE CALCULATING GRAND WINNERS.");
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	/*public static boolean testAnswerLoad(String docName,Integer qNo) {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			Date today = new Date();
			Random rand = new Random();	
			Map<String, Object> sMap = null;
			sMap = new HashMap<String, Object>();
			sMap.put("username", "TEST");
			answerCounts.document(docName).set(staticMap);
			answerCounts.document(docName).collection(qNo+" A").document().set(sMap);
			Date afDate = new Date();
			System.out.println("TIME :  " +new Date(afDate.getTime()-today.getTime()));
			return true;
		} catch (Exception e) {
			failedReqCnt++;
			e.printStackTrace();
			return false;
		}
	}*/
	
	public static Boolean testParticipantLoad(String docName){
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			//participantList = firestore.collection("AuthUserList");
			CollectionReference participantList1  = firestore.collection("ParticipantList");
			for(int i=0;i<500;i++){
				Map<String, Object> sMap = null;
				sMap = new HashMap<String, Object>();
				sMap.put("dob", "07/08/2020");
				sMap.put("zip", "1234");
				sMap.put("email", "test@gmail.com"+i);
				sMap.put("phone", "4856589542"+i);
				sMap.put("uid", "hkhkHkhKHkhyGYGIijLkIJihj"+i);
				sMap.put("username", "TEST"+i);
				//participantList1.document("FINALIST123").set(staticMap);
			//	participantList1.document("FINALIST123").collection("users").document().set(sMap);
				sMap.put("deviceType", "IOS");
				sMap.put("token", "");
				
				//participantList.document("FINALIST123").set(staticMap);
				//participantList.document("FINALIST123").collection("users").document().set(sMap);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	

	public static void updateUpcomingContestDetails() {
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			contestInfo = rtfCollection.document("dashboardInfoNew");
			updateDashbord = rtfCollection.document("UpdateDashBoard");

			GridHeaderFilters filter = GridHeaderFiltersUtil.getContestsFilter(null); 
			List<Contests> contestsList = DAORegistry.getContestsDAO().getAllContests(null, filter,"'ACTIVE','STARTED'");
			Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'STARTED'");
			
			List<Map<String, Object>> contestMapList = new ArrayList<Map<String,Object>>();
			if (contestsList != null && !contestsList.isEmpty()) {
				Contests c = contestsList.get(0);
				String nextGameTime = Util.getNextContestStartTimeMsg(c.getStartDateTime());
				Map<String, Object> cMap = new HashMap<String, Object>();
				cMap.put("joinButtonPopupMsg", Util.getJoinButtonMsg(nextGameTime));
				cMap.put("isContestStarted", contest==null?false:true);
				cMap.put("joinButtonLabel", "PLAY GAME");
				cMap.put("nextContestId", c.getId());
				cMap.put("isPassRequired", false);
				cMap.put("isSponPublic", c.getIsSponPublic());
				
				for (Contests co : contestsList) {
					if (contestMapList.size() < 4) {
						Map<String, Object> contestMap = new HashMap<String, Object>();
						contestMap.put("contestId", co.getId());
						contestMap.put("contestName", co.getContestName());
						contestMap.put("imageUrl", "");
						contestMap.put("nextGameTime", Util.getNextContestStartTimeMsg(co.getStartDateTime()));
						contestMap.put("shareText",Util.getShareText(co.getContestName()));
						contestMapList.add(contestMap);
					}
				}

				if (contestMapList.isEmpty()) {
					Map<String, Object> contestMap = new HashMap<String, Object>();
					contestMap.put("contestId", -1);
					contestMap.put("contestName", "Next Contest will be Announced Shortly.");
					contestMap.put("type", "NONE");
					contestMap.put("imageUrl", "");
					contestMap.put("nextGameTime", "TBD");
					contestMap.put("shareText","");
					contestMapList.add(contestMap);
				}
				cMap.put("contestArray", contestMapList);
				
				Map<String, Object> dMap = new HashMap<String, Object>();
				dMap.put("timeStamp", new Date().getTime());
				
				contestInfo.update(cMap);
				updateDashbord.update(dMap);
				fireStoreLog.info("UPCOMING CONTEST UPDATED:  -- " + contestMapList.size() + " -- special : "+ cMap.get("specialContestId"));
			} else {
				Map<String, Object> contestMap = new HashMap<String, Object>();
				contestMap.put("contestId", -1);
				contestMap.put("contestName", "Next Contest will be Announced Shortly.");
				contestMap.put("type", "NONE");
				contestMap.put("imageUrl", "");
				contestMap.put("nextGameTime", "TBD");
				contestMap.put("shareText","");
				contestMapList.add(contestMap);

				Map<String, Object> cMap = new HashMap<String, Object>();
				cMap.put("grandWinnerExpiryText", "");
				cMap.put("joinButtonPopupMsg", "Next Contest will be Announced Shortly.");
				cMap.put("isContestStarted", contest==null?false:true);
				cMap.put("joinButtonLabel", "PLAY GAME");
				cMap.put("nextContestId", 0);
				cMap.put("superFanText","");
				cMap.put("isPassRequired", false);

				Map<String, Object> webMap = new HashMap<String, Object>();
				webMap.put("nextGameTime", "TBD");
				webMap.put("contestName", "Next Contest will be Announced Shortly.");
				webMap.put("contestId", 0);

				cMap.put("contestArray", contestMapList);
				
				Map<String, Object> dMap = new HashMap<String, Object>();
				dMap.put("timeStamp", new Date().getTime());
				
				contestInfo.update(cMap);
				updateDashbord.set(dMap);
				fireStoreLog.info("UPCOMING CONTEST UPDATED:  -No Upcoming contest");
			}
			
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING UPCOMING CONTEST DETAILS.");
			e.printStackTrace();
		}
	}
	
	
	
	public static boolean uploadPasscodetoFirestore(List<PackageDetails> packages){
		boolean isUploded = false;
		try {
			if(firestore == null){
				initialiseFirebase();
			}
			Map<String, Object> codeMap = null;
			for(PackageDetails pack : packages){
				codeMap = new HashMap<String, Object>();
				codeMap.put("barname", pack.getBrandName());
				codeMap.put("limitMessage","We're sorry, the limited number of guests associated with your Sponsor ID has already been met.");
				codeMap.put("maxLimit", pack.getUserLimitCnt());
				codeMap.put("prizeText", pack.getWinnerPrize());
				codeMap.put("contestId", pack.getContestId());
				codeMap.put("contestId", pack.getPassCode());
				codeMap.put("addr", pack.getAddr());
				codeMap.put("contactInfo", pack.getPhone());
				codeMap.put("finelistPrizeText", pack.getFinelistPrize()!=null?pack.getFinelistPrize():"");
				DocumentReference doc = firestore.collection("passcodes").document(pack.getPassCode().toUpperCase());
				if(!doc.get().get().exists()){
					codeMap.put("currentCount", 0);
					doc.set(codeMap);
				}else{
					doc.update(codeMap);
				}
				
				/*Map<String, Object> codeMap1 = new HashMap<String, Object>();
				codeMap1.put("finelistPrizeText", pack.getFinelistPrize()!=null?pack.getFinelistPrize():"");
				DocumentReference doc1 = firestore.collection("passcodes").document(pack.getPassCode().toUpperCase()+"_TMP");
				doc1.set(codeMap1);*/
			}
			isUploded = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUploded;
	}
	
	
	public static List<String> getNotifictionTokens(){
		List<String> tokens = new ArrayList<String>();
		try {
			String token = "";
			ApiFuture<QuerySnapshot> authListFuture =  firestore.collection("AuthUserList").get();
			List<QueryDocumentSnapshot> authDocs = authListFuture.get().getDocuments();
			for(QueryDocumentSnapshot doc  :authDocs){
				ApiFuture<QuerySnapshot> users =  firestore.collection("AuthUserList").document(doc.getId()).collection("users").get();
				List<QueryDocumentSnapshot> userDocs = users.get().getDocuments();
				
				for(QueryDocumentSnapshot d  :userDocs){
					try {
						token = d.getString("token");
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(token == null || token.isEmpty()){
						continue;
					}
					tokens.add(token);
				}
			}
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE GETTING NOTIFICATION TOKENS.");
			e.printStackTrace();
		}
		return tokens;
	}
	

	public static void initialiseFirebase() {
		try {
			list.add("A");
			list.add("B");
			list.add("C");
			String MP_FOLDER = System.getProperty("java.io.tmpdir");
			System.out.println(MP_FOLDER);
			//FileInputStream serviceAccount = new FileInputStream("/rtw/RewardTheFan/FireBase/rtf_funzata_sandbox.json");
			//FileInputStream serviceAccount = new FileInputStream("C:/REWARDTHEFAN/FireBase/rtf_funzata_sandbox.json");
			//FileInputStream serviceAccount = new FileInputStream("C:/REWARDTHEFAN/FireBase/rtf_funzata.json");
			FileInputStream serviceAccount = new FileInputStream("/rtw/RewardTheFan/FireBase/rtf_funzata.json");
			FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();
			FirebaseApp mobileFireStore = FirebaseApp.initializeApp(options,"funzataSand1");
			firestore = FirestoreClient.getFirestore(mobileFireStore);
			rtfCollection = firestore.collection("rewardthefan");
			hostCollection = firestore.collection("hostNode");
			//answerCounts = firestore.collection("answerCounts");
			//participantList = firestore.collection("ParticipantList");
			fireStoreLog.info("FIRESTORE CONFIRGURATION INITIALISED.");
			staticMap.put("TEST", "TEST");
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE INITIALISING FIRESTORE.");
			e.printStackTrace();
		}
	}

	
	//HOST SCREEN METHODS
	private static DocumentReference tracker= null;
	private static DocumentReference hostQue = null;
	
	
	public static void updateHostTracker(String state){
		try {
			tracker = hostCollection.document("tracker");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("state", state);
			
			tracker.set(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void updateNextQuestionHost(ContestQuestions q, Model model) {
		Date timeStamp = new Date();
		try {
			if(q==null){
				return;
			}
			updateHostTracker("QUESTION");
			hostQue = hostCollection.document("que");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", q.getId());
			qMap.put("qNo", q.getSerialNo());
			qMap.put("txt", q.getQuestion());
			qMap.put("optA", q.getOptionA());
			qMap.put("optB", q.getOptionB());
			qMap.put("optC", q.getOptionC());
			qMap.put("ans", !q.getAnswer().equalsIgnoreCase("Z")?q.getAnswer():q.getCorrectAnswer());
			qMap.put("timeStamp", timeStamp.getTime());

			hostQue.set(qMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("HOST CONTEST " + q.getAnswer() + ":  --qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "HOST : ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo());
			fireStoreLog.error("HOST : ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo() + " -- " + q.getAnswer());
			e.printStackTrace();
		}
	}
	
	
	private static void cleanupHostNode(){
		hostQue = hostCollection.document("que");
		ContestQuestions q = new ContestQuestions();
		q.setAnswer("");
		Map<String, Object> qMap = new HashMap<String, Object>();
		qMap.put("qId", q.getId());
		qMap.put("qNo", q.getSerialNo());
		qMap.put("txt", q.getQuestion());
		qMap.put("optA", q.getOptionA());
		qMap.put("optB", q.getOptionB());
		qMap.put("optC", q.getOptionC());
		qMap.put("ans", !q.getAnswer().equalsIgnoreCase("Z")?q.getAnswer():q.getCorrectAnswer());
		qMap.put("timeStamp", new Date().getTime());
		hostQue.set(qMap);
		updateHostTracker("END");
		
	}
	

}
