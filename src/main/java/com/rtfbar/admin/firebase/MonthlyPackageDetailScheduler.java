package com.rtfbar.admin.firebase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfbar.admin.dao.impl.DAORegistry;
import com.rtfbar.admin.data.Contests;
import com.rtfbar.admin.data.PackageDetails;
import com.rtfbar.admin.utils.Util;
import com.rtfbar.admin.wpdb.DatabaseConnection;

public class MonthlyPackageDetailScheduler  extends QuartzJobBean implements StatefulJob{

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Contests contest = DAORegistry.getContestsDAO().getTodaysContest("'STARTED'");
			if(contest != null){
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			createSponsor();
		} catch (Exception e) {
			System.out.println("SPONSOR : ERROR OCCURED WHILE CREATING PASSCODE FOR MONTHLY SPONSOR.");
			e.printStackTrace();
		}
		
	}
	
	
	
	private void createSponsor() throws Exception{
		ResultSet rsObj = null;
		Connection connObj = null;
		Statement stmtObj = null;
		try {
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, 48);
			
			Date today = new Date();
			Date tomm = cal.getTime();
			System.out.println(today+"  --  "+tomm);
			List<Contests> contests = DAORegistry.getContestsDAO().getActiveContestByDateRange(today,tomm);
			if(contests == null || contests.isEmpty()){
				return;
			}
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT p.id as pId, sp.id as spId, p.product_name as prodName, p.product_date as prodDate, p.product_type as prodType, sp.order_id as orderId, ")
			.append("sp.user_id as userId,sp.sponsor_id as sponsorId, sp.allowed_users as allowedUsers, sp.winner_text as grandPrize, sp.create_date as crDate ")
			.append("sp.first_name as first_name, sp.last_name as last_name, sp.email as email, sp.phone as phone, sp.finalist_prize as finalist_prize, sp.address as address, sp.sponsor_name as sponsor_name  ")
			.append("FROM fun_new.wp3r_sponsors sp join fun_new.wp3r_custom_product p on sp.product_id=p.id WHERE p.product_type<>'Individual' by p.product_date");
			
			connObj = DatabaseConnection.getConnection();
			stmtObj = connObj.createStatement();
			rsObj = stmtObj.executeQuery(sql.toString());
			
			for(Contests co : contests){
				while (rsObj.next()) {
					Integer wpId = rsObj.getInt("spId");
					String userCntStr = rsObj.getString("allowedUsers");
					if(userCntStr==null || userCntStr.isEmpty() || wpId == null){
						continue;
					}
					
					List<PackageDetails> packs = DAORegistry.getPackageDetailsDAO().getAllActivePackageDetailsByCoIdAndWpId(co.getId(), wpId);
					if(packs!=null && !packs.isEmpty()){
						continue;
					}
					
					PackageDetails pack = new PackageDetails();
					int userCnt = 0;
					try {
						if(userCntStr.contains(" ")){
							userCntStr = userCntStr.substring(0,userCntStr.indexOf(" "));
						}
						userCnt = Integer.parseInt(userCntStr);
					} catch (Exception e) {
						System.out.println("SPONSOR : ERROR OCCURED INVALID USER LIMIT VALUE:"+userCntStr);
						e.printStackTrace();
						continue;
					}
					
					Timestamp titme = null;
					try {
						titme = rsObj.getTimestamp("crDate");
					} catch (Exception e) {
						System.out.println("SPONSOR : ERROR OCCURED INVALID DATE IN POST");
						e.printStackTrace();
					}
					
					
					pack.setCrDate(titme!=null?new Date(titme.getTime()):null);
					pack.setActUserCnt(0);
					pack.setUserLimitCnt(userCnt);
					pack.setContestId(co.getId());
					pack.setIsWinnerEmailSent(false);
					pack.setWinnerPrize(rsObj.getString("grandPrize"));
					pack.setStatus("ACTIVE");
					pack.setWpId(wpId);
					pack.setIsCancellationEmailSent(false); 
					pack.setFirstName(rsObj.getString("first_name"));
					pack.setLastName(rsObj.getString("last_name"));
					pack.setEmail(rsObj.getString("email"));
					pack.setPhone(rsObj.getString("phone"));
					pack.setAddr(rsObj.getString("address"));
					pack.setFinelistPrize(rsObj.getString("finalist_prize"));
					pack.setBrandName(rsObj.getString("sponsor_name"));
					
					try {
						pack.setPassCode(Util.getAlphaNumericString(6));
						DAORegistry.getPackageDetailsDAO().saveOrUpdate(pack);
					} catch (Exception e) {
						System.out.println("SPONSOR : ERROR OCCURED WHILE SAVE/UPDATE PACKAGE DETAILS");
						e.printStackTrace();
						return;
					}
					
					
					StringBuffer emailBody = new StringBuffer();
					emailBody.append("<html>");
					emailBody.append("<table align='center' width='700'>");
					//emailBody.append("<tr><td align='center'><img src='cid:image'  width='300'></td></tr>");
					emailBody.append("<tr><td>");
					emailBody.append("Hello "+pack.getBrandName()+",<br/>");
					emailBody.append("<br/><br/>");
					emailBody.append("Here is your SPONSOR ID : <b>"+pack.getPassCode()+"</b> for <b> "+co.getContestName() +"</b> Scheduled on <b>"+ co.getStartDateTimeStr()+"</b>");
					emailBody.append("<br/><br/>");
					emailBody.append("Thanks you for joining FUNZATA LIVE GAME SHOW.");
					emailBody.append("</td></tr>");
					emailBody.append("</table>");
					emailBody.append("</html>");
					
					try {
						//MailManager.sendMailNow(pack.getEmail() , "sales@rewardthefan.com", "Funzata game Winner!", emailBody.toString(),"");
					} catch (Exception e) {
						System.out.println("SPONSOR: ERROR OCCURED WHILE SENDING SPONSOR ID EMAIL : "+pack.getBrandName()+" | "+pack.getContestId());
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			throw e;
		}finally{
			DatabaseConnection.closeConnection(connObj);
		}
	}

}
