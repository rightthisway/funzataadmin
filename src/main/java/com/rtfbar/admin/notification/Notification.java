package com.rtfbar.admin.notification;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import io.github.jav.exposerversdk.ExpoMessageSound;
import io.github.jav.exposerversdk.ExpoPushMessage;
import io.github.jav.exposerversdk.ExpoPushMessageTicketPair;
import io.github.jav.exposerversdk.ExpoPushReceipt;
import io.github.jav.exposerversdk.ExpoPushTicket;
import io.github.jav.exposerversdk.PushClient;
import io.github.jav.exposerversdk.PushClientException;

public class Notification {

	public static boolean sendNotificationNow(List<String> tokens, String message, String title) {
		try {
			/*String recipient = "ExponentPushToken[thisisaninvalidtokennn]"; 
			if (!PushClient.isExponentPushToken(recipient))
				throw new Error("Token:" + recipient + " is not a valid token.");*/
			
			ExpoMessageSound sound = new ExpoMessageSound();
			sound.setName("default");
			//sound.setVolume(80);
			//sound.setCritical(false);

			ExpoPushMessage expoPushMessage = new ExpoPushMessage();
			expoPushMessage.addAllTo(tokens);
			expoPushMessage.setTitle(title);
			expoPushMessage.setBody(message);
			expoPushMessage.setSound(sound);
			expoPushMessage.setChannelId("FunzataChannel");
			//expoPushMessage.setBadge(0L);
			
			List<ExpoPushMessage> expoPushMessages = new ArrayList<>();
			expoPushMessages.add(expoPushMessage);

			PushClient client = new PushClient();
			List<List<ExpoPushMessage>> chunks = client.chunkPushNotifications(expoPushMessages);

			List<CompletableFuture<List<ExpoPushTicket>>> messageRepliesFutures = new ArrayList<>();

			for (List<ExpoPushMessage> chunk : chunks) {
				messageRepliesFutures.add(client.sendPushNotificationsAsync(chunk));
			}

			// Wait for each completable future to finish
			List<ExpoPushTicket> allTickets = new ArrayList<>();
			for (CompletableFuture<List<ExpoPushTicket>> messageReplyFuture : messageRepliesFutures) {
				try {
					for (ExpoPushTicket ticket : messageReplyFuture.get()) {
						allTickets.add(ticket);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}

			List<ExpoPushMessageTicketPair<ExpoPushMessage>> zippedMessagesTickets = client
					.zipMessagesTickets(expoPushMessages, allTickets);

			List<ExpoPushMessageTicketPair<ExpoPushMessage>> okTicketMessages = client.filterAllSuccessfulMessages(zippedMessagesTickets);
			String okTicketMessagesString = okTicketMessages.stream().map(p -> "Title: " + p.message.getTitle() + ", Id:" + p.ticket.getId())
					.collect(Collectors.joining(","));
			System.out.println("Recieved OK ticket for " + okTicketMessages.size() + " messages: " + okTicketMessagesString);

			List<ExpoPushMessageTicketPair<ExpoPushMessage>> errorTicketMessages = client.filterAllMessagesWithError(zippedMessagesTickets);
			String errorTicketMessagesString = errorTicketMessages.stream()
					.map(p -> "Title: " + p.message.getTitle() + ", Error: " + p.ticket.getDetails().getError())
					.collect(Collectors.joining(","));
			System.out.println("Recieved ERROR ticket for " + errorTicketMessages.size() + " messages: "+ errorTicketMessagesString);

			// Countdown 30s
			int wait = 15;
			for (int i = wait; i >= 0; i--) {
				System.out.print("Waiting for " + wait + " seconds. " + i + "s\r");
				Thread.sleep(1000);
			}
			System.out.println("Fetching reciepts...");

			List<String> ticketIds = (client.getTicketIdsFromPairs(okTicketMessages));
			CompletableFuture<List<ExpoPushReceipt>> receiptFutures = client.getPushNotificationReceiptsAsync(ticketIds);

			List<ExpoPushReceipt> receipts = new ArrayList<>();
			try {
				receipts = receiptFutures.get();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Recieved " + receipts.size() + " receipts:");

			for (ExpoPushReceipt reciept : receipts) {
				System.out.println("Receipt for id: " + reciept.getId() + " had status: " + reciept.getStatus());

			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void main(String[] args) throws PushClientException, InterruptedException {
		try {
			List<String> tokens = new ArrayList<String>();
			//tokens.add("ExponentPushToken[acBjbTEW_eZilwGgDoRl2g]");
			tokens.add("ExponentPushToken[hKyKozC8gfFvQgDT73LcjI]");
			sendNotificationNow(tokens, "test notification1", "MITUL1");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
