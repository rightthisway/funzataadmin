package com.rtfbar.admin.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtfbar.admin.dao.impl.DAORegistry;
import com.rtfbar.admin.data.Role;
import com.rtfbar.admin.data.TrackerUser;
import com.rtfbar.admin.data.UserAction;
import com.rtfbar.admin.utils.Constants;
import com.rtfbar.admin.utils.GsonCustomConfig;
import com.rtfbar.admin.utils.Util;


public class UserDetailsServiceImpl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
//		System.out.println(userName);
//		TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, userName);
//		if(trackerUser == null){
//			return null;
//		}
//		System.out.println(trackerUser);
		try{
			Map<String, String> map = Util.getParameterMap(null);
			
			
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, userName);
			
			
			HttpSession session = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
									.getRequest().getSession();
			session.setAttribute("trackerUser", trackerUser);
			
	
			Boolean isAdmin =false;
			Boolean isBroker =false;
			Boolean isUser =false;
			Boolean isAffiliates =false;
			Boolean isContest = false;
			
			
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			if(trackerUser.getRoles() != null && !trackerUser.getRoles().isEmpty()){
				for(Role role : trackerUser.getRoles()){				
					authorities.add(new SimpleGrantedAuthority(role.getName()));
				    if(role.getName().equals("ROLE_SUPER_ADMIN")){
				    	isAdmin = true;
				     //session.setAttribute("isAdmin",true);
				    }else if(role.getName().equals("ROLE_BROKER")){
				    	isBroker = true;
				    // session.setAttribute("isBroker",true);
				    }else if(role.getName().equals("ROLE_USER")){
				    	isUser = true;
				     //session.setAttribute("isUser",true);
				    }else if(role.getName().equals("ROLE_AFFILIATES")){
				    	isAffiliates = true;
				    // session.setAttribute("isAffiliates",true);
				    }else if(role.getName().equals("ROLE_CONTEST")){
				    	isContest = true;
				    }
				}
			}else{
				authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
				isUser = true;
			}
			
			session.setAttribute("isAdmin",isAdmin);
			session.setAttribute("isBroker",isBroker);
			session.setAttribute("isUser",isUser);
			session.setAttribute("isAffiliates",isAffiliates);
			session.setAttribute("isContest",isContest);
			session.setAttribute("userName",trackerUser.getUserName());
			
			try {
				UserAction userActionAudit = new UserAction();
				userActionAudit.setUserName(userName);
				userActionAudit.setUserId(trackerUser.getId());
				userActionAudit.setAction("login");
				userActionAudit.setIpAddress("1.1.1.1");
				userActionAudit.setTimeStamp(new Date());
				userActionAudit.setMessage("User Logged in.");
				userActionAudit.setDataId(trackerUser.getId());
				
				DAORegistry.getUserActionDAO().save(userActionAudit);
			} catch (Exception e) {
				e.printStackTrace();
			}
			//Util.userActionAudit(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest(), null, userActionMsg);
			
			UserDetails userDetails = new org.springframework.security.core.userdetails.
					User(trackerUser.getUserName(), trackerUser.getPassword(), true, true, true, true, authorities);
			
			return userDetails;
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

}
