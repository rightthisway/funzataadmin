package com.rtfbar.admin.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

public class CustomerDateAndTimeDeserialize extends JsonDeserializer<Date>{

	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	private SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");

	@Override
	public Date deserialize(JsonParser paramJsonParser,DeserializationContext paramDeserializationContext)
	     throws IOException, JsonProcessingException {
		 String str = paramJsonParser.getText().trim();
		 try {
		     return dateFormat.parse(str);
		 } catch (ParseException e) {
			 try {
			     return dateFormat1.parse(str);
			 } catch (ParseException e1) {
				 e1.printStackTrace();
			 }
		 }
		 return null;
	}
}
