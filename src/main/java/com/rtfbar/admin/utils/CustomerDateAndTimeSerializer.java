package com.rtfbar.admin.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomerDateAndTimeSerializer extends JsonSerializer<Date> {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    @Override
    public void serialize(Date value, JsonGenerator jgen,
        SerializerProvider provider) throws IOException,JsonProcessingException {
    	try {
			 jgen.writeString(dateFormat.format(value));
		 } catch (Exception e) {
			 try {
				 jgen.writeString(dateFormat1.format(value));
			 } catch (Exception e1) {
				 e1.printStackTrace();
			 }
		 }
    }
}
