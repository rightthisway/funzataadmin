package com.rtfbar.admin.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class ExcelUtil {
	
	private static DecimalFormat df2 = new DecimalFormat(".##");

	public static void generateExcelHeaderRow(List<String> headerList, Sheet ssSheet) {
		int j = 0;
		Row rowhead = ssSheet.createRow((int) 0);
		for (String header : headerList) {

			rowhead.createCell((int) j).setCellValue(header);
			j++;
		}
	}

	public static void generateExcelData(List<Object[]> dataList, Sheet ssSheet) {
		int j = 0;
		int rowCount = 1;
		for (Object[] dataObj : dataList) {
			Row rowhead = ssSheet.createRow((int) rowCount);
			rowCount++;
			j = 0;
			for (Object data : dataObj) {
				if (data != null) {
					rowhead.createCell((int) j).setCellValue(data.toString());
				} else {
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
		}
	}
	public static CellStyle getHeaderColumnStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        style.setFont(font);
        //font.setFontHeightInPoints((short) 18);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        return style;
    }
	public static void generateExcelHeaderRowWithStyles(List<String> headerList, Sheet ssSheet,
			SXSSFWorkbook workbook) {
		int j = 0;
		Row rowhead = ssSheet.createRow((int) 0);
		CellStyle cstyl = getReportTableHeaderStyle(workbook);
		for (String header : headerList) {
			Cell cell = rowhead.createCell((int) j);
			cell.setCellStyle(cstyl);
			cell.setCellValue(header);
			j++;
		}
	}

	public static Map<String, Object> getReportNameStyleProps() {

		HashMap<String, Object> properties = new HashMap<String, Object>();
		// Set border around the cell
		properties.put(CellUtil.BORDER_TOP, BorderStyle.MEDIUM);
		properties.put(CellUtil.BORDER_BOTTOM, BorderStyle.MEDIUM);
		properties.put(CellUtil.BORDER_LEFT, BorderStyle.MEDIUM);
		properties.put(CellUtil.BORDER_RIGHT, BorderStyle.MEDIUM);
		// Set color Red
		properties.put(CellUtil.TOP_BORDER_COLOR, IndexedColors.RED.getIndex());
		properties.put(CellUtil.BOTTOM_BORDER_COLOR, IndexedColors.RED.getIndex());
		properties.put(CellUtil.LEFT_BORDER_COLOR, IndexedColors.RED.getIndex());
		properties.put(CellUtil.RIGHT_BORDER_COLOR, IndexedColors.RED.getIndex());

		// Apply the borders to the cell
		/*
		 * Row row = sheet.createRow(2); Cell cell = row.createCell(2);
		 * CellUtil.setCellStyleProperties(cell, properties);
		 */

		return properties;
	}

	public static CellStyle getReportNameHeaderStyle(SXSSFWorkbook wb) {

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Arial");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		font.setItalic(false);
		cellStyle.setFont(font);
		return cellStyle;
	}

	public static CellStyle getReportTableHeaderStyle(SXSSFWorkbook wb) {
		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Arial");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		font.setItalic(false);
		cellStyle.setFont(font);
		return cellStyle;
	}

	public static void addReportTitle(Map<String, String> reportNameMap, Sheet ssSheet, SXSSFWorkbook workbook,
			int rowSartAt) {		
		int j = 0;
		
		  
		Row rowhead = ssSheet.createRow(rowSartAt);
		CellStyle cstyl = getReportTableHeaderStyle(workbook);
		for (Map.Entry<String, String> set : reportNameMap.entrySet()) {
			System.out.println(set.getKey() + " = " + set.getValue());
			Cell cell = rowhead.createCell((int) j);
			cell.setCellStyle(cstyl);
			cell.setCellValue(set.getKey());
			//firstRow, lastRow, firstCol, lastCol
			ssSheet.addMergedRegion(new CellRangeAddress(0,0,0,1));
			j=2;
			Cell cell2 = rowhead.createCell((int) j);
			cell2.setCellStyle(cstyl);
			cell2.setCellValue(set.getValue());
			//firstRow, lastRow, firstCol, lastCol
			ssSheet.addMergedRegion(new CellRangeAddress(0,0,2,5));
		}

	}

	public static void addSummaryTitle(String summaryHeader, Sheet ssSheet, SXSSFWorkbook workbook, int rowSartAt) {
		
		int j = 0;
		Row rowhead = ssSheet.createRow(rowSartAt);
		CellStyle cstyl = getReportTableHeaderStyle(workbook);
		Cell cell = rowhead.createCell((int) j);
		cell.setCellStyle(cstyl);
		cell.setCellValue(summaryHeader);
		ssSheet.addMergedRegion(new CellRangeAddress(rowSartAt,rowSartAt,0,2));

	}

	public static void addReportSummaryDescription(Map<String, String> reportSummaryMap, Sheet ssSheet,
			SXSSFWorkbook workbook, int rowSartAt) {
		int j = 0;
		int rowCount = rowSartAt;
		for (Map.Entry<String, String> set : reportSummaryMap.entrySet()) {
			Row rowhead = ssSheet.createRow((int) rowCount);			
			j = 0;
			rowhead.createCell((int) j).setCellValue(set.getKey());
			ssSheet.addMergedRegion(new CellRangeAddress(rowCount,rowCount,0,2));
			j=3;
			rowhead.createCell((int) j).setCellValue(set.getValue());
			ssSheet.addMergedRegion(new CellRangeAddress(rowCount,rowCount,3,5));
			rowCount++;
		}		
	}
	
	public static void generateExcelHeaderRowWithStyles(List<String> headerList, Sheet ssSheet,
			SXSSFWorkbook workbook ,  int rowSartAt) {
		int j = 0;
		Row rowhead = ssSheet.createRow(rowSartAt);
		CellStyle cstyl = getReportTableHeaderStyle(workbook);
		for (String header : headerList) {
			Cell cell = rowhead.createCell((int) j);
			cell.setCellStyle(cstyl);
			cell.setCellValue(header);
			j++;
		}
	}

	public static void generateErrorMessageExcel(Sheet ssSheet, SXSSFWorkbook workbook, int rownum, String msg) {
		int j = 0;
		Row rowhead = ssSheet.createRow(rownum);
		CellStyle cstyl = getReportTableHeaderStyle(workbook);
		cstyl.setFillForegroundColor(IndexedColors.DARK_GREEN.getIndex());
		Cell cell = rowhead.createCell((int) j);
		cell.setCellStyle(cstyl);
		cell.setCellValue(msg);
		ssSheet.addMergedRegion(new CellRangeAddress(rownum,rownum,0,7));

		
	}
	
	
	
	
	public static void getExcelStringCell(Object o,int j,Row rowhead){
		try {
			if(o!=null){
				rowhead.createCell((int) j).setCellValue(o.toString());
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelStringCellRed(Object o,int j,Row rowhead,CellStyle style){
		try {
			if(o!=null){
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue(o.toString());
			}else{
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue("");
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getExcelDateCell(Object o,int j,Row rowhead,CellStyle style){
		try {
			
			if(o!=null){
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelDecimalCell(Object o,int j,Row rowhead){
		try {
			if(o!=null){
				rowhead.createCell((int) j).setCellValue(getRoundedValue(Double.parseDouble(o.toString().trim())));
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getExcelDecimalCellRed(Object o,int j,Row rowhead,CellStyle style){
		try {
			if(o!=null){
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(getRoundedValue(Double.parseDouble(o.toString().trim())));
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(Double.parseDouble(o.toString().trim())));
			}else{
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue("");
				cell.setCellStyle(style);
				//rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelHeaderCellWithBG(Object o,int j,Row rowhead,CellStyle style){
		try {
			Cell cell = rowhead.createCell((int) j);
			cell.setCellValue(o.toString());
			cell.setCellStyle(style);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getExcelIntegerCell(Object o,int j,Row rowhead){
		try {
			if(o!=null){
				rowhead.createCell((int) j).setCellValue(Integer.parseInt(o.toString().trim()));
			}else{
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static CellRangeAddress getMergedRegion(Cell c){
		try {
			Sheet sheet = c.getSheet();
			for(CellRangeAddress range: sheet.getMergedRegions()){
				if(range.isInRange(c.getRowIndex(), c.getColumnIndex())){
					return range;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}
	
	

}
