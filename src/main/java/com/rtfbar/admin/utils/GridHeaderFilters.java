package com.rtfbar.admin.utils;

public class GridHeaderFilters {
	
	private String sortingColumn;
	private String sortingOrder;
	
	private String createdBy;
	private String createdDate;
	private String updatedBy;
	private String updatedDate;
	
	private Integer customerId;
	private String userId;	
	private String userRole;
	private String email;
	private String phone;
	private String ageGroup;
	
	private Integer contestId;	
	private String contestName;
	private String winnerPrize;
	private Integer maxWinners;
	private Integer prizeQty;
	private Double prizeValue;
	private String contestDate;
	private String status;
	private Integer qSerialNo;
	
	
	private String text1;
	private String text2;
	private String text3;
	private String text4;
	private String text5;
	private String text6;
	private String text7;
	private String text8;
	private String text9;
	private String text10;
	private String text11;
	private Integer count;
	public String getSortingColumn() {
		return sortingColumn;
	}
	public void setSortingColumn(String sortingColumn) {
		this.sortingColumn = sortingColumn;
	}
	public String getSortingOrder() {
		return sortingOrder;
	}
	public void setSortingOrder(String sortingOrder) {
		this.sortingOrder = sortingOrder;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAgeGroup() {
		return ageGroup;
	}
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public String getWinnerPrize() {
		return winnerPrize;
	}
	public void setWinnerPrize(String winnerPrize) {
		this.winnerPrize = winnerPrize;
	}
	public Integer getPrizeQty() {
		return prizeQty;
	}
	public void setPrizeQty(Integer prizeQty) {
		this.prizeQty = prizeQty;
	}
	public Double getPrizeValue() {
		return prizeValue;
	}
	public void setPrizeValue(Double prizeValue) {
		this.prizeValue = prizeValue;
	}
	public String getContestDate() {
		return contestDate;
	}
	public void setContestDate(String contestDate) {
		this.contestDate = contestDate;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getText2() {
		return text2;
	}
	public void setText2(String text2) {
		this.text2 = text2;
	}
	public String getText3() {
		return text3;
	}
	public void setText3(String text3) {
		this.text3 = text3;
	}
	public String getText4() {
		return text4;
	}
	public void setText4(String text4) {
		this.text4 = text4;
	}
	public String getText5() {
		return text5;
	}
	public void setText5(String text5) {
		this.text5 = text5;
	}
	public String getText6() {
		return text6;
	}
	public void setText6(String text6) {
		this.text6 = text6;
	}
	public String getText7() {
		return text7;
	}
	public void setText7(String text7) {
		this.text7 = text7;
	}
	public String getText8() {
		return text8;
	}
	public void setText8(String text8) {
		this.text8 = text8;
	}
	public String getText9() {
		return text9;
	}
	public void setText9(String text9) {
		this.text9 = text9;
	}
	public String getText10() {
		return text10;
	}
	public void setText10(String text10) {
		this.text10 = text10;
	}
	public String getText11() {
		return text11;
	}
	public void setText11(String text11) {
		this.text11 = text11;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getMaxWinners() {
		return maxWinners;
	}
	public void setMaxWinners(Integer maxWinners) {
		this.maxWinners = maxWinners;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getqSerialNo() {
		return qSerialNo;
	}
	public void setqSerialNo(Integer qSerialNo) {
		this.qSerialNo = qSerialNo;
	}
		
}
