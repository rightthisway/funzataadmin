package com.rtfbar.admin.utils;


public class GridHeaderFiltersUtil {

	public static GridHeaderFilters getContestsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("contestName")){
							filter.setContestName(value);
						}else if(name.equalsIgnoreCase("startDate")){
							filter.setContestDate(value);
						}else if(name.equalsIgnoreCase("prizeQty")){
							filter.setPrizeQty(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("maxWinners")){
							filter.setMaxWinners(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("prizeValue")){
							filter.setPrizeValue(Double.parseDouble(value));
						}else if(name.equalsIgnoreCase("prizeText")){
							filter.setWinnerPrize(value);
						}else if(name.equalsIgnoreCase("questionSize")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("status")){
							filter.setStatus(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDate(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	public static GridHeaderFilters getContestQuestionFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("question")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("optionA")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("optionB")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("optionC")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("optionD")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("answer")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDate(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updatedBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("serialNo")){
							filter.setqSerialNo(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getContestPackageDetailsFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("brandName")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("passCode")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("firstName")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("lastName")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setText5(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setText6(value);
						}else if(name.equalsIgnoreCase("isWinnerEmailSent")){
							filter.setText7(value);
						}else if(name.equalsIgnoreCase("finelistPrize")){
							filter.setText8(value);
						}else if(name.equalsIgnoreCase("winnerPrize")){
							filter.setWinnerPrize(value);
						}else if(name.equalsIgnoreCase("userLimitCnt")){
							filter.setCount(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("actUserCnt")){
							filter.setqSerialNo(Integer.parseInt(value));
						}else if(name.equalsIgnoreCase("createdDate")){
							filter.setCreatedDate(value);
						}else if(name.equalsIgnoreCase("updatedDate")){
							filter.setUpdatedDate(value);
						}else if(name.equalsIgnoreCase("createdBy")){
							filter.setCreatedBy(value);
						}else if(name.equalsIgnoreCase("updBy")){
							filter.setUpdatedBy(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
	
	public static GridHeaderFilters getContestWinnersFilter(String headerFilter){
		GridHeaderFilters filter = new GridHeaderFilters();
		try {
			if(headerFilter!=null && !headerFilter.isEmpty() && headerFilter.contains(":")
					&& headerFilter.contains(",")){
				String params[] = headerFilter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length >= 2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(nameValue.length > 2){
							if(nameValue[2] != null && !nameValue[2].isEmpty()){
								value = nameValue[1]+":"+nameValue[2];
							}
						}
						
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("passCode")){
							filter.setText1(value);
						}else if(name.equalsIgnoreCase("brandName")){
							filter.setText4(value);
						}else if(name.equalsIgnoreCase("userId")){
							filter.setUserId(value);
						}else if(name.equalsIgnoreCase("prizeText")){
							filter.setText2(value);
						}else if(name.equalsIgnoreCase("email")){
							filter.setEmail(value);
						}else if(name.equalsIgnoreCase("phone")){
							filter.setPhone(value);
						}else if(name.equalsIgnoreCase("isWinnerEmailSent")){
							filter.setText3(value);
						}else if(name.equalsIgnoreCase("SORTINGCOLUMN")){
							filter.setSortingColumn(value);
						}else if(name.equalsIgnoreCase("SORTINGORDER")){
							filter.setSortingOrder(value);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return filter;
	}
	
	
		
	
	private static boolean validateParamValues(String name,String value){
		if(name==null || name.isEmpty() || name.equalsIgnoreCase("undefined") ||
				value==null || value.isEmpty() || value.equalsIgnoreCase("undefined")){
			return false;
		}
		return true;	
	}
}



