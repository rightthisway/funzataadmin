package com.rtfbar.admin.utils;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonCustomConfig {

	public static Gson getGsonBuilder(){
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonCustomDateSerializer()).create();
		return gson;
	}
	
}
