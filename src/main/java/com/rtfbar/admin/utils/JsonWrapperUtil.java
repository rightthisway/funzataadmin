package com.rtfbar.admin.utils;

public class JsonWrapperUtil {
	
	/*public static JSONArray getOpenOrderArray(Collection<OpenOrderStatus> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(OpenOrderStatus order : list){
					object = new JSONObject();
					object.put("id",order.getId() );
					object.put("invoiceNo",order.getInvoiceNo());
					if(order.getProductType().equalsIgnoreCase(ProductType.REWARDTHEFAN.toString())){
						object.put("orderId",order.getOrderId());
					}else{
						object.put("orderId",order.getTicketRequestId());
					}
					object.put("invoiceDateStr",order.getInvoiceDateStr() );
					object.put("eventName",order.getEventName());
					object.put("eventDateStr",order.getEventDateStr() );
					object.put("eventTimeStr", order.getEventTimeStr());
					object.put("venueName", order.getVenueName());
					object.put("venueCity", order.getVenueCity());
					object.put("venueState", order.getVenueState());
					object.put("venueCountry", order.getVenueCountry());
					object.put("productType",order.getProductType() );
					object.put("internalNotes",order.getInternalNotes() );
					object.put("soldQty",order.getSoldQty() );
					object.put("customerName",order.getCustomerName());
					object.put("zone", order.getZone());
					object.put("zoneTixQty", order.getZoneTixQty());
					object.put("zoneCheapestPrice",order.getZoneCheapestPrice()!=null?String.format("%.2f",order.getZoneCheapestPrice()):"-");
					object.put("zoneTixGroupCount", order.getZoneTixGroupCount());
					object.put("section", order.getSection());
					object.put("row",order.getRow());
					object.put("actualSoldPrice",order.getActualSoldPrice()!=null?String.format("%.2f",order.getActualSoldPrice()):"-");
					object.put("marketPrice",order.getMarketPrice()!=null?String.format("%.2f",order.getMarketPrice()):"-");
					object.put("lastUpdatedPrice",order.getLastUpdatedPrice()!=null?String.format("%.2f",order.getLastUpdatedPrice()):"-");
					object.put("sectionTixQty",order.getSectionTixQty());
					object.put("eventTixQty",order.getEventTixQty());
					object.put("totalActualSoldPrice",order.getTotalActualSoldPrice()!=null?String.format("%.2f",order.getTotalActualSoldPrice()):"-");
					object.put("totalMarketPrice",order.getTotalMarketPrice()!=null?String.format("%.2f",order.getTotalMarketPrice()):"-");
					object.put("profitAndLoss",order.getProfitAndLoss()!=null&&order.getProfitAndLoss()!=0?String.format("%.2f",order.getProfitAndLoss()):"-");
					object.put("netTotalSoldPrice",order.getNetTotalSoldPrice()!=null?String.format("%.2f",order.getNetTotalSoldPrice()):"-");
					object.put("netSoldPrice",order.getNetSoldPrice()!=null?String.format("%.2f",order.getNetSoldPrice()):"-");
					object.put("sectionMargin",order.getSectionMargin()!=null&&order.getSectionMargin()!=0?String.format("%.2f",order.getSectionMargin()):"-");
					object.put("zoneTotalPrice",order.getZoneTotalPrice()!=null&&order.getZoneTotalPrice()!=0?String.format("%.2f",order.getZoneTotalPrice()):"-");
					object.put("zoneProfitAndLoss",order.getZoneProfitAndLoss()!=null&&order.getZoneProfitAndLoss()!=0?String.format("%.2f",order.getZoneProfitAndLoss()):"-");
					object.put("zoneMargin",order.getZoneMargin()!=null&&order.getZoneMargin()!=0?String.format("%.2f",order.getZoneMargin()):"-");
					object.put("priceUpdateCount",order.getPriceUpdateCount());
					object.put("price",order.getPrice()!=null?String.format("%.2f",order.getPrice()):"-");
					object.put("discountCouponPrice",order.getDiscountCouponPrice()!=null?String.format("%.2f", order.getDiscountCouponPrice()):"-");
					object.put("url", order.getUrl());
					object.put("shippingMethod", order.getShippingMethod());
					object.put("trackingNo",order.getTrackingNo() );
					object.put("secondaryOrderType", order.getSecondaryOrderType());
					object.put("poId",order.getPoId());
					object.put("poDateStr", order.getPoDateStr());
					object.put("secondaryOrderId",order.getSecondaryOrderId() );
					object.put("lastUpdateStr",order.getLastUpdateStr() );
					object.put("tmatEventId",order.getTmatEventId() );
					object.put("brokerId", order.getBrokerId()!=null ? order.getBrokerId():"");
					object.put("companyName", order.getCompanyName()!=null ? order.getCompanyName(): "");
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getEventArray(Collection<EventDetails> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(EventDetails event : list ){
					object = new JSONObject();
					object.put("eventId",event.getEventId());
					object.put("eventName",event.getEventName());
					object.put("eventDateStr",event.getEventDateStr());
					object.put("eventTimeStr",event.getEventTimeStr());
					object.put("dayOfWeek",event.getDayOfWeek());
					object.put("building",event.getBuilding());
					object.put("venueId",event.getVenueId());
					object.put("noOfTixCount",event.getNoOfTixCount()==null?0:event.getNoOfTixCount());
					object.put("noOfTixSoldCount",event.getNoOfTixSoldCount()==null?0:event.getNoOfTixSoldCount());
					object.put("city",event.getCity());
					object.put("state",event.getState());
					object.put("country",event.getCountry());
					object.put("grandChildCategoryName",event.getGrandChildCategoryName());
					object.put("childCategoryName",event.getChildCategoryName());
					object.put("parentCategoryName",event.getParentCategoryName());
					object.put("eventCreationStr",event.getEventCreationStr());
					object.put("eventUpdatedStr",event.getEventUpdatedStr());
					object.put("notes",event.getNotes());
					array.put(object);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONObject getEventObject(EventDetails event){
		JSONObject object = new JSONObject();
		try {
			if(event!=null){
				object.put("eventId",event.getEventId());
				object.put("eventName",event.getEventName());
				object.put("eventDateStr",event.getEventDateStr());
				object.put("eventTimeStr",event.getEventTimeStr());
				object.put("dayOfWeek",event.getDayOfWeek());
				object.put("building",event.getBuilding());
				object.put("venueId",event.getVenueId());
				object.put("noOfTixCount",event.getNoOfTixCount()==null?0:event.getNoOfTixCount());
				object.put("noOfTixSoldCount",event.getNoOfTixSoldCount()==null?0:event.getNoOfTixSoldCount());
				object.put("city",event.getCity());
				object.put("state",event.getState());
				object.put("country",event.getCountry());
				object.put("grandChildCategoryName",event.getGrandChildCategoryName());
				object.put("childCategoryName",event.getChildCategoryName());
				object.put("parentCategoryName",event.getParentCategoryName());
				object.put("eventCreationStr",event.getEventCreationStr());
				object.put("eventUpdatedStr",event.getEventUpdatedStr());
				object.put("notes",event.getNotes());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
	public static JSONObject getEventObject1(Event event,Venue venue){
		JSONObject object = new JSONObject();
		try {
			if(event!=null){
				object.put("eventId",event.getEventId());
				object.put("eventName",event.getEventName());
				object.put("eventDateStr",event.getEventDateStr());
				object.put("eventTimeStr",event.getEventTimeStr());
				object.put("building",venue.getBuilding());
				object.put("venueId",event.getVenueId());
				object.put("city",venue.getCity());
				object.put("state",venue.getState());
				object.put("country",venue.getCountry());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
	
	
	public static JSONArray getTicketGroupArray(Collection<TicketGroup> list,Map<Integer, String> shippingMap,Event eventDtls){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for (TicketGroup tx : list) {
					object = new JSONObject();
					object.put("id", tx.getId());
					object.put("eventId", tx.getEventId());
					object.put("section", tx.getSection());
					object.put("row", tx.getRow());
					object.put("quantity", tx.getQuantity());
					object.put("price", String.format("%.2f", tx.getPrice()));
					object.put("taxAmount", String.format("%.2f", 0.0));
					object.put("sectionRange", "");
					object.put("rowRange", "");
					object.put("shippingMethod", shippingMap.get(tx.getShippingMethodId()));
					object.put("productType", "REWARDTHEFAN");
					object.put("retailPrice", String.format("%.2f", tx.getPrice()));//tx.getPrice());
					object.put("wholeSalePrice", String.format("%.2f", tx.getPrice()));//tx.getWholeSalePrice());
					object.put("facePrice", String.format("%.2f", 0.0));// tx.getFacePrice());
					object.put("loyalFanPrice",String.format("%.2f",tx.getLoyalFanPrice()));
					object.put("cost", String.format("%.2f", 0.0));//tx.getCost());
					object.put("broadcast", tx.getBroadcast()!=null?tx.getBroadcast():0);//tx.getBroadcast());
					object.put("internalNotes","ZTP");// tx.getInternalNotes());
					object.put("externalNotes", "");
					object.put("marketPlaceNotes", "");
					object.put("maxShowing", "");
					object.put("seatLow", tx.getSeatLow());
					object.put("seatHigh", tx.getSeatHigh());
					object.put("nearTermDisplayOption", "");
					if(eventDtls!=null){
						object.put("eventName", eventDtls.getEventName());
						object.put("eventDate", eventDtls.getEventDateStr());
						object.put("eventTime", eventDtls.getEventTimeStr());
						object.put("venue", eventDtls.getBuilding());
					}
					object.put("brokerId", tx.getBrokerId());
					object.put("ticketType","Real");
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getCategoryTicketGroupArray(Collection<CategoryTicketGroup> list,Map<Integer, String> shippingMap,Event eventDtls,JSONArray array){
		if(array==null || array.length()==0){
			array = new JSONArray();
		}
		try {
			if(list!=null){
				JSONObject object = null;
				for (CategoryTicketGroup tx : list) {
					object = new JSONObject();
					object.put("id", tx.getId());
					object.put("eventId", tx.getEventId());
					object.put("section", tx.getSection());
					object.put("row", tx.getRow()==null?"":tx.getRow());
					object.put("quantity", tx.getQuantity());
					object.put("price", String.format("%.2f",tx.getPrice()));
					object.put("taxAmount", String.format("%.2f",tx.getTaxAmount()));
					object.put("sectionRange", tx.getSectionRange());
					object.put("rowRange", tx.getRowRange());
					object.put("shippingMethod",shippingMap.get(tx.getShippingMethodId())==null?"":shippingMap.get(tx.getShippingMethodId()));
					object.put("productType", tx.getProducttype());
					object.put("retailPrice", String.format("%.2f",tx.getPrice()));//tx.getPrice());
					object.put("wholeSalePrice", String.format("%.2f",tx.getPrice()));//tx.getWholeSalePrice());
					object.put("facePrice",String.format("%.2f",0.0));// tx.getFacePrice());
					object.put("loyalFanPrice",String.format("%.2f",tx.getLoyalFanPrice()));
					object.put("cost", String.format("%.2f",0.0));//tx.getCost());
					object.put("broadcast", tx.getBroadcast()!=null?tx.getBroadcast():0);//tx.getBroadcast());
					object.put("internalNotes","ZTP");// tx.getInternalNotes());
					object.put("externalNotes", tx.getExternalNotes());
					object.put("marketPlaceNotes", tx.getMarketPlaceNotes());
					object.put("maxShowing", tx.getMaxShowing());
					object.put("seatLow", tx.getSeatLow());
					object.put("seatHigh", tx.getSeatHigh());
					object.put("nearTermDisplayOption", tx.getNearTermDisplayOption());
					if(eventDtls!=null){
						object.put("eventName", eventDtls.getEventName());
						object.put("eventDate", eventDtls.getEventDateStr());
						object.put("eventTime", eventDtls.getEventTimeStr());
						object.put("venue", eventDtls.getBuilding());
					}
					object.put("brokerId", tx.getBrokerId());
					object.put("ticketType","Category");
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONObject getTicketGroupObject(TicketGroup tx){
		JSONObject object = new JSONObject();
		try {
			object.put("id", tx.getId());
			object.put("eventId", tx.getEventId());
			object.put("section", tx.getSection());
			object.put("row", tx.getRow());
			object.put("quantity", tx.getQuantity());
			object.put("price", String.format("%.2f",tx.getPrice()));
			object.put("taxAmount","0.00");
			//object.put("taxAmount","20.00");
			object.put("productType","REWARDTHEFAN");
			object.put("retailPrice", String.format("%.2f",tx.getPrice()));//tx.getPrice());
			object.put("wholeSalePrice", String.format("%.2f",tx.getPrice()));//tx.getWholeSalePrice());
			object.put("facePrice",String.format("%.2f",0.0));// tx.getFacePrice());
			object.put("cost", String.format("%.2f",0.0));//tx.getCost());
			object.put("broadcast", tx.getBroadcast()!=null?tx.getBroadcast():0);//tx.getBroadcast());
			object.put("internalNotes","ZTP");// tx.getInternalNotes());
			object.put("seatLow", tx.getSeatLow());
			object.put("seatHigh", tx.getSeatHigh());
			object.put("brokerId", tx.getBrokerId());
			object.put("ticketType","Category");
			object.put("loyalFanPrice",String.format("%.2f",tx.getLoyalFanPrice()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
	
	public static JSONObject getCategoryTicketGroupObject(CategoryTicketGroup tx){
		JSONObject object = new JSONObject();
		try {
			if(tx!=null){
				object.put("id", tx.getId());
				object.put("eventId", tx.getEventId());
				object.put("section", tx.getSection());
				object.put("row", tx.getRow());
				object.put("quantity", tx.getQuantity());
				object.put("price", String.format("%.2f",tx.getPrice()));
				object.put("taxAmount", String.format("%.2f",tx.getTaxAmount()));
				//object.put("taxAmount","0.00");
				object.put("sectionRange", tx.getSectionRange());
				object.put("rowRange", tx.getRowRange());
				object.put("productType", tx.getProducttype());
				object.put("retailPrice", String.format("%.2f",tx.getPrice()));//tx.getPrice());
				object.put("wholeSalePrice", String.format("%.2f",tx.getPrice()));//tx.getWholeSalePrice());
				object.put("facePrice",String.format("%.2f",0.0));// tx.getFacePrice());
				object.put("cost", String.format("%.2f",0.0));//tx.getCost());
				object.put("broadcast", tx.getBroadcast()!=null?tx.getBroadcast():0);//tx.getBroadcast());
				object.put("internalNotes","ZTP");// tx.getInternalNotes());
				object.put("externalNotes", tx.getExternalNotes());
				object.put("marketPlaceNotes", tx.getMarketPlaceNotes());
				object.put("maxShowing", tx.getMaxShowing());
				object.put("seatLow", tx.getSeatLow());
				object.put("seatHigh", tx.getSeatHigh());
				object.put("nearTermDisplayOption", tx.getNearTermDisplayOption());
				object.put("brokerId", tx.getBrokerId());
				object.put("ticketType","Category");
				object.put("loyalFanPrice",String.format("%.2f",tx.getLoyalFanPrice()));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
	
	public static JSONArray getInvoiceArray(Collection<OpenOrders> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for (OpenOrders order : list) {
					object = new JSONObject();
					object.put("productType", order.getProductType());
					object.put("invoiceId", order.getInvoiceId());
					object.put("customerId", order.getCustomerId());
					object.put("customerName", order.getCustomerName());
					object.put("username", order.getUsername());
					object.put("customerType", order.getCustomerType());
					object.put("custCompanyName", order.getCustCompanyName());
					object.put("ticketCount", order.getTicketCount());
					object.put("invoiceTotal", String.format("%.2f", order.getInvoiceTotal()));
					object.put("addressLine1", order.getAddressLine1());
					object.put("country", order.getCountry());
					object.put("state", order.getState());
					object.put("city", order.getCity());
					object.put("zipCode", order.getZipCode());
					object.put("phone", order.getPhone());
					object.put("invoiceAged", order.getInvoiceAged());
					object.put("createdDateStr", order.getCreatedDateStr());
					object.put("createdBy", order.getCreatedBy());
					object.put("csr", order.getCsr());
					object.put("secondaryOrderType", order.getSecondaryOrderType());
					object.put("secondaryOrderId", order.getSecondaryOrderId());
					object.put("voidedDateStr", order.getVoidedDateStr());
					object.put("isInvoiceEmailSent", order.getIsInvoiceEmailSent());
					object.put("purchaseOrderNo", order.getPurchaseOrderNo());
					object.put("shippingMethod", order.getShippingMethod());
					object.put("trackingNo", order.getTrackingNo());
					object.put("platform", order.getPlatform());
					if(order.getFedexLabelCreated()!= null && order.getFedexLabelCreated()){
						object.put("fedexLabelCreated", "Yes");
					}else{
						object.put("fedexLabelCreated", "No");
					}					
					object.put("customerOrderId", order.getCustomerOrderId());
					object.put("lastUpdatedDateStr", order.getLastUpdatedDateStr());
					object.put("lastUpdatedBy", order.getLastUpdatedBy());
					object.put("status", order.getStatus());
					object.put("eventId", order.getEventId());
					object.put("eventName", order.getEventName());
					object.put("eventDateStr", order.getEventDateStr());
					object.put("eventTimeStr", order.getEventTimeStr());
					object.put("brokerId", order.getBrokerId() != null ? order.getBrokerId() : "");
					if(order.getOrderType()!= null && order.getOrderType().equalsIgnoreCase("LOYALFAN")){
						object.put("loyalFanOrder", "Yes");
					}else{
						object.put("loyalFanOrder", "No");
					}
					object.put("primaryPaymentMethod", order.getPrimaryPaymentMethod());
					if(order.getSecondaryPaymentMethod() != null && !order.getSecondaryPaymentMethod().equalsIgnoreCase("NULL")){
						object.put("secondaryPaymentMethod", order.getSecondaryPaymentMethod());
					}else{
						object.put("secondaryPaymentMethod", "");
					}
					if(order.getThirdPaymentMethod() != null && !order.getThirdPaymentMethod().equalsIgnoreCase("NULL")){
						object.put("thirdPaymentMethod", order.getThirdPaymentMethod());
					}else{
						object.put("thirdPaymentMethod", "");
					}
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getPuchaseOrderArray(Collection<PurchaseOrders> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for (PurchaseOrders order : list) {
					object = new JSONObject();
					object.put("id", order.getId());
					object.put("customerName", order.getCustomerName());
					object.put("customerId", order.getCustomerId());
					object.put("customerType", order.getCustomerType());
					object.put("poTotal", String.format("%.2f", order.getPoTotal()));
					object.put("ticketQty", order.getTicketQty());
					object.put("eventTicketQty", order.getEventTicketQty());
					object.put("eventTicketCost",order.getEventTicketCost()!=null?String.format("%.2f",order.getEventTicketCost()):"");
					object.put("createdDateStr", order.getCreatedDateStr());
					object.put("createdBy", order.getCreatedBy());
					object.put("shippingType", order.getShippingType());
					object.put("status", order.getStatus());
					object.put("productType", order.getProductType());
					object.put("lastUpdatedStr", order.getLastUpdatedStr());
					object.put("poAged", order.getPoAged());
					object.put("csr", order.getCsr());
					object.put("consignmentPoNo", order.getConsignmentPoNo());
					object.put("purchaseOrderType", order.getPurchaseOrderType());
					object.put("transactionOffice", order.getTransactionOffice());
					object.put("trackingNo", order.getTrackingNo());
					object.put("shippingNotes", order.getShippingNotes());
					object.put("externalNotes", order.getExternalNotes());
					object.put("internalNotes", order.getInternalNotes());
					object.put("eventId", order.getEventId());
					object.put("eventName", order.getEventName());
					object.put("eventDateStr", order.getEventDateStr());
					object.put("eventTimeStr", order.getEventTimeStr());
					object.put("posProductType", order.getPosProductType());
					object.put("posPOId", order.getPosPOId());
					if(order.getIsEmailed()!= null && order.getIsEmailed()){
						object.put("isEmailed", "Yes");
					}else{
						object.put("isEmailed", "No");
					}
					if(order.getFedexLabelCreated()!= null && order.getFedexLabelCreated()){
						object.put("fedexLabelCreated", "Yes");
					}else{
						object.put("fedexLabelCreated", "No");
					}
					object.put("brokerId", order.getBrokerId() != null ? order.getBrokerId() : "");
					array.put(object);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getCustomerArray(Collection<Customers> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(Customers cust :list){
					object = new JSONObject();
					object.put("customerId", cust.getCustomerId());
					object.put("customerType", cust.getCustomerType());
					object.put("productType", cust.getProductType());
					object.put("signupType", cust.getSignupType());
					object.put("customerStatus", cust.getCustomerStatus());
					object.put("companyName", cust.getCompanyName());
					object.put("customerName", cust.getCustomerName());
					object.put("lastName", cust.getLastName());
					object.put("customerEmail", cust.getCustomerEmail());
					object.put("addressLine1", cust.getAddressLine1());
					object.put("addressLine2", cust.getAddressLine2());
					object.put("city", cust.getCity());
					object.put("billingAddressId", cust.getBillingAddressId());
					object.put("state", cust.getState());
					object.put("country", cust.getCountry());
					object.put("zipCode", cust.getZipCode());
					object.put("client", cust.getIsClient()==true?"Yes":"No");
					object.put("broker", cust.getIsBroker()==true?"Yes":"No");
					object.put("phone", cust.getPhone());
					object.put("referrerCode", cust.getReferrerCode());
					object.put("rewardPoints", cust.getRewardPoints());
					if(cust.getIsMailSent()==null || cust.getIsMailSent() == false){
						object.put("emailSent","No");
					}else{
						object.put("emailSent","Yes");
					}
					
					if(cust.getIsRetailCust()==null || cust.getIsRetailCust() == false){
						object.put("retailCust","No");
					}else{
						object.put("retailCust","Yes");
					}
					
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getCrownJewelOrderArray(List<CrownJewelCustomerOrder> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(CrownJewelCustomerOrder order : list){
					object = new JSONObject();
					object.put("id",order.getId());
					object.put("eventName",order.getFantasyEventName());
					object.put("teamId", order.getTeamId());
					object.put("teamName",order.getTeamName());
					if(order.getIsPackageSelected()){
						object.put("isPackageSelected","Yes");
					}else{
						object.put("isPackageSelected","No");
					}					
					object.put("packageCost", order.getPackageCost());
					object.put("packageNote", order.getPackageNote());
					object.put("eventId",order.getEventId());
					if(order.getIsRealTicket()){
						object.put("isRealTicket", "Yes");
					}else{
						object.put("isRealTicket", "No");
					}
					object.put("ticketId",order.getTicketId());
					object.put("zone",order.getTicketSelection());
					object.put("ticketQty", order.getTicketQty());
					object.put("ticketPrice",order.getTicketPrice());
					object.put("requirePoints",order.getRequiredPoints());
					object.put("customerId", order.getCustomerId());
					object.put("status",order.getStatus() );
					object.put("createdDateStr",order.getCreatedDateStr());
					object.put("lastUpdatedStr",order.getLastUpdatedStr());
					object.put("acceptRejectReason", order.getAcceptRejectReson()!=null?order.getAcceptRejectReson():"");
					object.put("regularOrderId",order.getRegularOrderId());
					object.put("teamZoneId", order.getTeamZoneId());
					object.put("venue",order.getVenueName());
					object.put("city",order.getVenueCity());
					object.put("state",order.getVenueState());
					object.put("country",order.getVenueCountry());
					object.put("platform", order.getPlatform());
					object.put("grandChildCategoryName", order.getGrandChildName());
					object.put("customerName", order.getCustomerName());
					//object.put("leagueName",order.getLeagueName());
					//object.put("leagueCity",order.getLeagueCity());
					//object.put("ticketGroupId",order.getTicketGroupId());
					//object.put("ticketSelection",order.getTicketSelection());
					//object.put("ticketPoints",order.getTicketPoints());
					//object.put("packagePoints", order.getPackagePoints());
					//object.put("remainingPoints", order.getRemainingPoints());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getSeatGeekOrderArray(List<SeatGeekOrders> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(SeatGeekOrders order : list){
					object = new JSONObject();
					object.put("id",order.getId() );
					object.put("orderId",order.getOrderId());
					object.put("orderDateStr",order.getOrderDateStr());
					object.put("eventName",order.getEventName());
					object.put("eventDateStr",order.getEventDateStr());										
					object.put("eventTimeStr", order.getEventTimeStr());
					object.put("venueName", order.getVenueName());
					object.put("quantity",order.getQuantity());
					object.put("section",order.getSection());
					object.put("row",order.getRow());
					object.put("totalSaleAmt", String.format("%.2f", order.getTotalSaleAmt()));
					object.put("totalPaymentAmt", String.format("%.2f", order.getTotalPaymentAmt()));
					object.put("status",order.getStatus());
					object.put("lastUpdatedDateStr",order.getLastUpdatedDateStr());
					object.put("ticTrackerOrderId", order.getTicTrackerOrderId());
					object.put("ticTrackerInvoiceId", order.getTicTrackerInvoiceId());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getWebServiceTrackingArray(Collection<WebServiceTracking> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(WebServiceTracking tracking : list){
					object = new JSONObject();
					object.put("id",tracking.getId());
					object.put("customerIpAddress",tracking.getCustomerIpAddress());
					object.put("webConfigId",tracking.getWebConfigId());
					object.put("actionType",tracking.getActionType());
					object.put("hittingDateStr",tracking.getHittingDateStr());										
					object.put("actionResult", tracking.getActionResult());
					object.put("productType", tracking.getProductType());
					object.put("authenticatedHit",tracking.getAuthenticatedHit());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getPopArtistArray(Collection<PopularArtist> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(PopularArtist popArtist : list){
					object = new JSONObject();
					object.put("artistId", popArtist.getArtistId());
					object.put("artistName", popArtist.getArtistName());
					object.put("grandChildCategory", popArtist.getGrandChildCategoryName());
					object.put("childCategory", popArtist.getChildCategoryName());
					object.put("parentCategory", popArtist.getParentType());	
					object.put("createdBy", popArtist.getCreatedBy());
					object.put("createdDate",popArtist.getCreatedDateStr());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}*/
	
	
	/*public static JSONArray getMangeUsersArray(Collection<TrackerUser> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;				
				for(TrackerUser user : list){
					object = new JSONObject();
					object.put("id", user.getId());
					object.put("userName", user.getUserName());
					object.put("firstName", user.getFirstName());
					object.put("lastName", user.getLastName());
					object.put("email", user.getEmail());										
					object.put("phone", user.getPhone());
					for(Role role: user.getRoles()){
						object.put("role", role.getName());
					}					
					object.put("brokerId", user.getBroker()!= null ? user.getBroker().getId(): "");
					object.put("companyName", user.getBroker()!= null ? user.getBroker().getCompanyName() : "");
					object.put("status", user.getStatus()==true?"ACTIVE":"DEACTIVE");
					object.put("promotionalCode", user.getPromotionalCode());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getShippingAddressArray(List<CustomerAddress> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;				
				for(CustomerAddress customer : list){
					object = new JSONObject();
					object.put("id", customer.getId());
					object.put("firstName", customer.getFirstName());
					object.put("lastName", customer.getLastName());
					object.put("addressLine1", customer.getAddressLine1());
					object.put("addressLine2", customer.getAddressLine2());										
					object.put("country", customer.getCountry().getName());
					object.put("state", customer.getState().getName());
					object.put("city", customer.getCity());
					object.put("zipCode", customer.getZipCode());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getPaypalTrackingArray(Collection<PayPalTracking> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;				
				for(PayPalTracking customer : list){
					object = new JSONObject();
					object.put("id", customer.getId());
					object.put("customerId", customer.getCustomerId());
					object.put("payerFirstName", customer.getPayerFirstName());
					object.put("payerLastName", customer.getPayerLastName());
					object.put("payerEmail", customer.getPayerEmail());
					object.put("payerPhone", customer.getPayerPhone());
					object.put("orderId", customer.getOrderId());
					object.put("orderTotal", String.format("%.2f", customer.getOrderTotal()));
					object.put("orderType", customer.getOrderType());
					object.put("eventId", customer.getEventId());
					object.put("quantity", customer.getQuantity());
					object.put("zone", customer.getZone());
					object.put("paypalTransactionId", customer.getPaypalTransactionId());
					object.put("transactionId", customer.getTransactionId());
					object.put("paymentId", customer.getPaymentId());
					object.put("platform", customer.getPlatform());
					object.put("status", customer.getStatus());
					object.put("createdDateStr", customer.getCreatedDateStr());
					object.put("lastUpdatedStr", customer.getLastUpdatedStr());
					object.put("transactionDateStr", customer.getTransactionDateStr());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getAllArtistArray(List<PopularArtist> artistList){
		JSONArray array = new JSONArray();
		try {
			if(artistList!=null){
				JSONObject object = null;
				for(PopularArtist artist : artistList){
					object = new JSONObject();
					object.put("artistId",artist.getArtistId());
					object.put("artistName",artist.getArtistName());
					object.put("grandChildCategory", artist.getGrandChildCategoryName());
					object.put("childCategory", artist.getChildCategoryName());
					object.put("parentCategory", artist.getParentType());
					object.put("imageFileUrl",artist.getImageFileUrl());
					if(artist.getImageFileUrl() != null && artist.getImageFileUrl() != ""){
						object.put("imgUploaded","Yes");
					}else{
						object.put("imgUploaded","No");
					}
					if(artist.getEventCount()==null){
						object.put("eventCount",0);
					}else{
						object.put("eventCount",artist.getEventCount());
					}
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getFantasyChildCategoryArray(List<FantasyChildCategory> childCategoryList){
		JSONArray array = new JSONArray();
		try {
			if(childCategoryList!=null){
				JSONObject object = null;
				for(FantasyChildCategory childCategory : childCategoryList){
					object = new JSONObject();
					object.put("id",childCategory.getId());
					object.put("name",childCategory.getName());
					object.put("parentCategoryName", childCategory.getParentCategoryName());
					object.put("lastUpdatedBy", childCategory.getLastUpdatedBy());
					object.put("lastUpdatedStr", childCategory.getLastUpdatedStr());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}*/
	
	
	
	
	/*public static JSONArray getInvoiceAuditJsonArray(List<InvoiceAudit> auditList,List<CustomerTicketDownloads> customerTicketDownloads){
		JSONArray array = new JSONArray();
		try {
			if(auditList!=null && !auditList.isEmpty()){
				JSONObject object = null;
				for(InvoiceAudit audit : auditList){
					object = new JSONObject();
					object.put("id", audit.getId());
					object.put("invoiceId", audit.getInvoiceId());
					object.put("action", audit.getAction());
					object.put("createdDate", audit.getCreatedDateStr());
					object.put("createdBy", audit.getCreateBy());
					object.put("note", audit.getNote());
					array.put(object);
				}
			}
			
			if(customerTicketDownloads!=null && !customerTicketDownloads.isEmpty()){
				JSONObject object = null;
				for(CustomerTicketDownloads download : customerTicketDownloads){
					object = new JSONObject();
					object.put("id", download.getId());
					object.put("invoiceId", download.getInvoiceId());
					object.put("action",InvoiceAuditAction.TICKET_DOWNLOADED.toString());
					object.put("createdDate", download.getDownloadDateTimeStr());
					object.put("createdBy", DAORegistry.getCustomerDAO().get(download.getCustomerId()).getUserName());
					object.put("note",download.getTicketName());
					array.put(object);
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getRewardPointsArray(List<RewardDetails> rewardDetailsList){
		JSONArray array = new JSONArray();
		try {
			if(rewardDetailsList!=null){
				JSONObject object = null;
				for(RewardDetails rewardDetail : rewardDetailsList){
					object = new JSONObject();
					object.put("orderNo", rewardDetail.getOrderNo());
					object.put("rewardPoint", String.format("%.2f", rewardDetail.getRewards()));
					object.put("orderDate", rewardDetail.getOrderDateStr());					
					object.put("orderTotal", String.format("%.2f", rewardDetail.getOrderTotal()));
					object.put("orderType", rewardDetail.getOrderType());
					object.put("eventName", rewardDetail.getEventName());
					object.put("eventDate", rewardDetail.getEventDateStr());
					object.put("eventTime", rewardDetail.getEventTimeStr());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONObject getSoldTicketDetailsArray(CategoryTicketGroup tx, TicketGroup ticket){
		JSONObject object = null;
		try {
			if(tx!=null){
				object = new JSONObject();
				object.put("id", tx.getId());
				object.put("invoiceId", tx.getInvoiceId());
				if(ticket != null){
					object.put("section", ticket.getSection());
					object.put("row", ticket.getRow());
					object.put("seatLow", ticket.getSeatLow());
					object.put("seatHigh", ticket.getSeatHigh());						
				}else{
					object.put("section", tx.getSection());
					object.put("row", tx.getRow());
					object.put("seatLow", tx.getSeatLow());
					object.put("seatHigh", tx.getSeatHigh());
				}
				object.put("quantity", tx.getQuantity());
				object.put("retailPrice", String.format("%.2f",tx.getPrice()));//tx.getPrice());
				object.put("wholeSalePrice", String.format("%.2f",tx.getPrice()));//tx.getWholeSalePrice());
				object.put("facePrice",String.format("%.2f",0.0));// tx.getFacePrice());
				object.put("price", String.format("%.2f",tx.getPrice()));
				object.put("cost", String.format("%.2f",0.0));//tx.getCost());
				object.put("shippingMethod", tx.getShippingMethod());
				object.put("nearTermDisplayOption", tx.getNearTermDisplayOption());
				object.put("broadcast", tx.getNearTermDisplayOption());//tx.getBroadcast());
				object.put("sectionRange", tx.getSectionRange());
				object.put("rowRange", tx.getRowRange());
				object.put("internalNotes", tx.getInternalNotes());// ZTP
				object.put("externalNotes", tx.getExternalNotes());
				object.put("marketPlaceNotes", tx.getMarketPlaceNotes());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
	public static JSONArray getTicketArray(List<Ticket> tickets,TicketGroup tg){
		JSONArray array = new JSONArray();
		try {
			if(tickets.isEmpty()){
				return array;
			}
			JSONObject object = null;
			for(Ticket ticket : tickets){
				object = new JSONObject();
				object.put("id", ticket.getId());
				object.put("cost",String.format("%.2f",ticket.getCost()!=null?ticket.getCost():0.00));
				object.put("actualSoldPrice",String.format("%.2f", ticket.getActualSoldPrice()));
				object.put("facePrice",String.format("%.2f",ticket.getFacePrice()));
				object.put("wholeSalePrice",String.format("%.2f",ticket.getWholesalePrice()));
				object.put("retailPrice",String.format("%.2f",ticket.getRetailPrice()));
				object.put("section",tg.getSection());
				object.put("row",tg.getRow());
				object.put("seatNo", ticket.getSeatNo());
				object.put("status",ticket.getTicketStatus().toString());
				if(ticket.getOnHand()!=null && ticket.getOnHand()){
					object.put("onHandStatus","Yes");
				}else{
					object.put("onHandStatus","No");
				}
				
				array.put(object);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getHoldTicketsArray(List<InventoryHold> inventoryHoldList){
		JSONArray array = new JSONArray();
		try {
			if(inventoryHoldList!=null){
				JSONObject object = null;
				for(InventoryHold inventoryHold : inventoryHoldList){
					object = new JSONObject();
					object.put("customerId", inventoryHold.getCustomerId());
					object.put("customerName", inventoryHold.getCustomerName());
					object.put("salePrice", String.format("%.2f", inventoryHold.getSalePrice()));
					object.put("createdBy", inventoryHold.getcSR());					
					object.put("expirationDate", inventoryHold.getExpirationDateStr());
					object.put("expirationMinutes", inventoryHold.getExpirationMinutes());
					object.put("shippingMethodId", inventoryHold.getShippingMethodId());
					object.put("shippingMethod", inventoryHold.getShippingMethod());
					object.put("holdDate", inventoryHold.getHoldDateTimeStr());
					object.put("internalNote", inventoryHold.getInternalNote());
					object.put("externalNote", inventoryHold.getExternalNote());
					object.put("ticketIds", inventoryHold.getTicketIds());
					object.put("externalPo", inventoryHold.getExternalPo());
					object.put("ticketGroupId", inventoryHold.getTicketGroupId());
					object.put("status", inventoryHold.getStatus());
					object.put("brokerId", inventoryHold.getBrokerId());
					object.put("section", inventoryHold.getSection());
					object.put("row", inventoryHold.getRow());
					object.put("seatLow", inventoryHold.getSeatLow());
					object.put("seatHigh", inventoryHold.getSeatHigh());
					array.put(object);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getLockedTicketsArray(List<CategoryTicketGroup> lockedTicketList, EventDetails eventDtls){
		JSONArray array = new JSONArray();
		try {
			if(lockedTicketList!=null){
				JSONObject object = null;
				for(CategoryTicketGroup lockedTicket : lockedTicketList){
					object = new JSONObject();
					object.put("section", lockedTicket.getSection());
					object.put("row", lockedTicket.getRow());
					object.put("quantity", lockedTicket.getQuantity());
					object.put("price", String.format("%.2f",lockedTicket.getPrice()));
					object.put("taxAmount", String.format("%.2f",lockedTicket.getTaxAmount()));
					object.put("sectionRange", lockedTicket.getSectionRange());
					object.put("rowRange", lockedTicket.getRowRange());
					object.put("shippingMethod", lockedTicket.getShippingMethod());
					object.put("productType", lockedTicket.getProducttype());
					object.put("retailPrice", String.format("%.2f",lockedTicket.getPrice()));//lockedTicket.getPrice());
					object.put("wholeSalePrice", String.format("%.2f",lockedTicket.getPrice()));//lockedTicket.getWholeSalePrice());
					object.put("facePrice",String.format("%.2f",0.0));// lockedTicket.getFacePrice());
					object.put("cost", String.format("%.2f",0.0));//lockedTicket.getCost());
					object.put("broadcast", 1);//lockedTicket.getBroadcast());
					object.put("internalNotes","ZTP");// lockedTicket.getInternalNotes());
					object.put("externalNotes", lockedTicket.getExternalNotes());
					object.put("marketPlaceNotes", lockedTicket.getMarketPlaceNotes());
					object.put("maxShowing", lockedTicket.getMaxShowing());
					object.put("seatLow", lockedTicket.getSeatLow());
					object.put("seatHigh", lockedTicket.getSeatHigh());
					object.put("nearTermDisplayOption", lockedTicket.getNearTermDisplayOption());
					if(eventDtls!=null){
						object.put("eventName", eventDtls.getEventName());
						object.put("eventDate", eventDtls.getEventDateStr());
						object.put("eventTime", eventDtls.getEventTimeStr());
						object.put("venue", eventDtls.getBuilding());
					}
					object.put("ticketType","Category");
					object.put("categoryTicketGroupId", lockedTicket.getCatgeoryTicketGroupId());
					object.put("ipAddress", lockedTicket.getIpAddress());
					object.put("platform", lockedTicket.getPlatform());					
					object.put("creationDate", lockedTicket.getCreationDate());
					object.put("lockStatus", lockedTicket.getLockStatus());
					array.put(object);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getAffiliatesArray(Collection<AffiliateCashRewardHistory> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(AffiliateCashRewardHistory history :list){
					object = new JSONObject();
					object.put("orderId", history.getOrder().getId());					
					object.put("firstName", history.getCustomer().getCustomerName());
					object.put("lastName", history.getCustomer().getLastName());
					object.put("eventName", history.getOrder().getEventName());
					object.put("eventDate", history.getOrder().getEventDateStr());
					object.put("eventTime", history.getOrder().getEventTimeStr());
					object.put("ticketQty", history.getOrder().getQty());
					object.put("orderTotal", history.getOrderTotal());
					object.put("cashCredit", history.getCreditedCash());
					object.put("status", history.getRewardStatus());
					object.put("promoCode", history.getPromoCode());
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getManageAffiliatesArray(Collection<TrackerUser> list,Map<Integer, AffiliateCashReward> cashRewardMapByUserId){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(TrackerUser user :list){
					object = new JSONObject();
					object.put("userId", user.getId());
					object.put("userName", user.getUserName());
					object.put("firstName", user.getFirstName());
					object.put("lastName", user.getLastName());
					object.put("email", user.getEmail());
					object.put("phone", user.getPhone());
					object.put("status", user.getStatus()==true?"ACTIVE":"DEACTIVE");
					object.put("promotionalCode", user.getPromotionalCode());
					object.put("activeCash", cashRewardMapByUserId.get(user.getId())!=null? cashRewardMapByUserId.get(user.getId()).getActiveCash(): 0.00);
					object.put("pendingCash", cashRewardMapByUserId.get(user.getId())!=null? cashRewardMapByUserId.get(user.getId()).getPendingCash(): 0.00);
					object.put("lastCreditedCash", reward.getLastCreditedCash());
					object.put("lastDebitedCash", reward.getLastDebitedCash());
					object.put("totalCreditedCash", reward.getTotalCreditedCash());
					object.put("totalDebitedCash", reward.getTotalDebitedCash());
					object.put("lastUpdate", reward.getLastUpdatedDateStr());
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	
	public static JSONArray getRTFPromotionalCodeArray(List<RTFPromoOffers> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(RTFPromoOffers code :list){
					object = new JSONObject();
					object.put("id", code.getId());
					object.put("promocode", code.getPromoCode());
					object.put("discountPerc", code.getDiscountPer());
					object.put("mobileDiscountPerc", code.getMobileDiscountPerc());
					object.put("startDate", code.getStartDateStr());
					object.put("endDate", code.getEndDateStr());
					object.put("maxOrder", code.getMaxOrders());
					object.put("orders",code.getNoOfOrders());
					if(code.getPromoType() != null && code.getPromoType().equalsIgnoreCase("GRANDCHILD")){
						object.put("promoType", "GRAND");
					}else{
						object.put("promoType", code.getPromoType());
					}
					object.put("artist",code.getArtistName());
					object.put("venue",code.getVenueName());
					object.put("parent",code.getParentCategory());
					object.put("child", code.getChildCategory());
					object.put("grandChild",code.getGrandChildCategory());
					object.put("artistId",code.getArtistId());
					object.put("venueId",code.getVenueId());
					object.put("parentId",code.getParentId());
					object.put("childId", code.getChildId());
					object.put("grandChildId",code.getGrandChildId());
					object.put("status",code.getStatus());
					object.put("createdBy", code.getCreatedBy());
					object.put("createdDate",code.getCreatedDateStr());
					object.put("modifiedBy",code.getModifiedBy());
					object.put("modifiedDate",code.getModifiedDateStr());
					String startDate[] = code.getStartDateStr().split(" ");
					object.put("fromDate", startDate[0]);
					String endDate[] = code.getEndDateStr().split(" ");
					object.put("toDate", endDate[0]);
					object.put("orderThreshold", (code.getFlatOfferOrderThreshold()!=null?code.getFlatOfferOrderThreshold():""));
					if(code.getIsFlatDiscount() != null && code.getIsFlatDiscount()){
						object.put("isFlatDiscount", "Yes");
					}else{
						object.put("isFlatDiscount", "No");
					}
					object.put("isAutoGenerate", "No"); //Default set Auto Generate when Edit
					
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	

	public static JSONArray getRelatedPOArray(List<PurchaseOrder> purchaseOrderList, List<POSPurchaseOrder> posPurchaseOrderList){
		JSONArray array = new JSONArray();
		try {
			if(purchaseOrderList!=null){
				JSONObject object = null;
				for(PurchaseOrder purchaseOrder : purchaseOrderList){
					object = new JSONObject();
					object.put("id", purchaseOrder.getId());
					object.put("poTotal", String.format("%.2f", purchaseOrder.getPoTotal()));
					object.put("totalQuantity", purchaseOrder.getTotalQuantity());					
					object.put("usedQunatity", purchaseOrder.getUsedQunatity());
					object.put("customerName", purchaseOrder.getCustomerName());
					object.put("csr", "");
					object.put("createTimeStr", purchaseOrder.getCreateTimeStr());
					object.put("internalNotes", purchaseOrder.getInternalNotes());
					object.put("externalNotes", purchaseOrder.getExternalNotes());
					array.put(object);
				}
			}
			if(posPurchaseOrderList!=null){
				JSONObject object = null;
				for(POSPurchaseOrder purchaseOrder : posPurchaseOrderList){
					object = new JSONObject();
					object.put("id", purchaseOrder.getPurchaseOrderId());
					object.put("poTotal", String.format("%.2f", purchaseOrder.getPoTotal()));
					object.put("totalQuantity", purchaseOrder.getTotalQuantity());					
					object.put("usedQunatity", purchaseOrder.getUsedQunatity());
					object.put("customerName", purchaseOrder.getCustomerName());
					object.put("csr", "");
					object.put("createTimeStr", purchaseOrder.getCreateTimeStr());
					object.put("internalNotes", purchaseOrder.getNotes());
					object.put("externalNotes", purchaseOrder.getDisplayedNotes());
					array.put(object);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getRelatedInvoicesArray(List<Ticket> ticketList, List<POSTicket> posTicketList){
		JSONArray array = new JSONArray();
		try {
			if(ticketList!=null){
				JSONObject object = null;
				for(Ticket ticket : ticketList){
					object = new JSONObject();
					object.put("id", ticket.getId());
					object.put("invoiceId", ticket.getInvoiceId());
					object.put("eventName", ticket.getEventNameString());					
					object.put("section", ticket.getSection());
					object.put("row", ticket.getRow());
					object.put("seat", ticket.getSeatNo());
					object.put("retailPrice", ticket.getRetailPrice());
					object.put("facePrice", ticket.getFacePrice());
					object.put("cost", ticket.getCost());
					object.put("wholesalePrice", ticket.getWholesaleSalePrice());
					object.put("actualSoldPrice", ticket.getActualSoldPrice());
					object.put("purchasedBy", "");
					array.put(object);
				}
			}
			if(posTicketList!=null){
				JSONObject object = null;
				for(POSTicket posTicket : posTicketList){
					object = new JSONObject();
					object.put("id", posTicket.getTicketId());
					object.put("invoiceId", posTicket.getInvoiceId());
					object.put("eventName", "");					
					object.put("section", posTicket.getSection());
					object.put("row", posTicket.getRow());
					object.put("seat", posTicket.getSeatNumber());
					object.put("retailPrice", posTicket.getRetailPrice());
					object.put("facePrice", posTicket.getFacePrice());
					object.put("cost", posTicket.getCost());
					object.put("wholesalePrice", posTicket.getWholesalePrice());
					object.put("actualSoldPrice", posTicket.getActualSoldPrice());
					object.put("purchasedBy", "");
					array.put(object);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getPaymentHistoryArray(List<PaymentHistory> paymentHistoryList, List<PaymentHistory> rtwPaymentHistoryList){
		JSONArray array = new JSONArray();
		try {
			if(paymentHistoryList!=null){
				JSONObject object = null;
				for(PaymentHistory paymentHistory : paymentHistoryList){
					object = new JSONObject();
					object.put("primaryPaymentMethod", paymentHistory.getPrimaryPaymentMethod());
					object.put("secondaryPaymentMethod", paymentHistory.getSecondaryPaymentMethod());
					object.put("thirdPaymentMethod", paymentHistory.getThirdPaymentMethod()!=null?paymentHistory.getThirdPaymentMethod():"");					
					object.put("primaryAmount", paymentHistory.getPrimaryAmount());
					object.put("secondaryAmount", paymentHistory.getSecondaryAmount());
					object.put("thirdPayAmt", paymentHistory.getThirdPayAmt());
					object.put("paidOnStr", paymentHistory.getPaidOnStr());
					object.put("secondaryTransaction", paymentHistory.getSecondaryTransaction());
					object.put("primaryTransaction", paymentHistory.getPrimaryTransaction());
					object.put("thirdTransactionId", paymentHistory.getThirdTransactionId());
					object.put("csr", paymentHistory.getCsr());
					array.put(object);
				}
			}
			if(rtwPaymentHistoryList!=null){
				JSONObject object = null;
				for(PaymentHistory paymentHistory : rtwPaymentHistoryList){
					object = new JSONObject();
					object.put("creditCardNumber", paymentHistory.getCreditCardNumber());
					object.put("checkNumber", paymentHistory.getCheckNumber());
					object.put("amount", paymentHistory.getAmount());					
					object.put("note", paymentHistory.getNote());
					object.put("paidOnStr", paymentHistory.getPaidOnStr());
					object.put("systemUser", paymentHistory.getSystemUser());
					object.put("paymentType", paymentHistory.getPaymentType());
					object.put("transOffice", paymentHistory.getTransOffice());
					array.put(object);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getPOPaymentHistoryArray(List<PurchaseOrderPaymentDetails> paymentHistoryList){
		JSONArray array = new JSONArray();
		try {
			if(paymentHistoryList!=null){
				JSONObject object = null;
				for(PurchaseOrderPaymentDetails paymentHistory : paymentHistoryList){
					object = new JSONObject();
					object.put("id", paymentHistory.getId());
					object.put("name", paymentHistory.getName());
					object.put("paymentType", paymentHistory.getPaymentType());					
					object.put("cardType", paymentHistory.getCardType());
					object.put("cardLastFourDigit", paymentHistory.getCardLastFourDigit());
					object.put("routingLastFourDigit", paymentHistory.getRoutingLastFourDigit());
					object.put("accountLastFourDigit", paymentHistory.getAccountLastFourDigit());
					object.put("chequeNo", paymentHistory.getChequeNo());
					object.put("paymentStatus", paymentHistory.getPaymentStatus());
					object.put("amount", paymentHistory.getAmount());
					object.put("paymentDate", paymentHistory.getPaymentDateStr());
					object.put("paymentNote", paymentHistory.getPaymentNote());
					array.put(object);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getRTFCustomerPromotionalOfferArray(List<RTFCustomerPromotionalOffer> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(RTFCustomerPromotionalOffer promoOffer :list){
					object = new JSONObject();
					object.put("id", promoOffer.getId());
					object.put("customerId", promoOffer.getCustomerId());
					object.put("firstName", promoOffer.getFirstName());
					object.put("lastName", promoOffer.getLastName());
					object.put("email", promoOffer.getEmail());
					object.put("promoCode", promoOffer.getPromoCode());
					object.put("discount", promoOffer.getDiscountStr());
					object.put("startDate", promoOffer.getStartDateStr());
					object.put("endDate", promoOffer.getEndDateStr());					
					object.put("status", promoOffer.getStatus());
					object.put("createdDate", promoOffer.getCreatedDateStr());
					object.put("modifiedDate", promoOffer.getModifiedDateStr());					
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getRTFPromotionalOfferTrackingArray(List<RTFPromotionalOfferTracking> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(RTFPromotionalOfferTracking promoOffer :list){
					object = new JSONObject();
					object.put("id", promoOffer.getId());
					object.put("customerId", promoOffer.getCustomerId());
					object.put("firstName", promoOffer.getFirstName());
					object.put("lastName", promoOffer.getLastName());
					object.put("email", promoOffer.getEmail());
					object.put("promoCode", promoOffer.getPromoCode());
					object.put("discount", promoOffer.getDiscount());
					object.put("orderId", promoOffer.getOrderId());
					object.put("orderTotal", promoOffer.getOrderTotal());
					object.put("quantity", promoOffer.getQuantity());
					object.put("ipAddress", promoOffer.getIpAddress()!=null?promoOffer.getIpAddress():"");
					if(promoOffer.getPrimaryPaymentMethod() == null || promoOffer.getPrimaryPaymentMethod().equalsIgnoreCase("NULL")){
						object.put("primaryPaymentMethod", "");
					}else{
						object.put("primaryPaymentMethod", promoOffer.getPrimaryPaymentMethod());
					}
					if(promoOffer.getSecondaryPaymentMethod() == null || promoOffer.getSecondaryPaymentMethod().equalsIgnoreCase("NULL")){
						object.put("secondaryPaymentMethod", "");
					}else{
						object.put("secondaryPaymentMethod", promoOffer.getSecondaryPaymentMethod());
					}
					if(promoOffer.getThirdPaymentMethod() == null || promoOffer.getThirdPaymentMethod().equalsIgnoreCase("NULL")){
						object.put("thirdPaymentMethod", "");
					}else{
						object.put("thirdPaymentMethod", promoOffer.getThirdPaymentMethod());
					}
					object.put("platform", promoOffer.getPlatForm()!=null?promoOffer.getPlatForm():"");
					object.put("status", promoOffer.getStatus());
					object.put("createdDate", promoOffer.getCreatedDateStr());
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getManualFedexGenerationArray(List<ManualFedexGeneration> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(ManualFedexGeneration manualFedex :list){
					object = new JSONObject();
					object.put("id", manualFedex.getId());
					object.put("blFirstName", manualFedex.getBlFirstName());
					object.put("blLastName", manualFedex.getBlLastName());
					object.put("blCompanyName", manualFedex.getBlCompanyName());
					object.put("blAddress1", manualFedex.getBlAddress1());
					object.put("blAddress2", manualFedex.getBlAddress2());
					object.put("blCity", manualFedex.getBlCity());
					object.put("blState", manualFedex.getBlState());
					object.put("blStateName", manualFedex.getBlStateName());					
					object.put("blCountry", manualFedex.getBlCountry());
					object.put("blCountryName", manualFedex.getBlCountryName());
					object.put("blZipCode", manualFedex.getBlZipCode());
					object.put("blPhone", manualFedex.getBlPhone());
					object.put("shCustomerName", manualFedex.getShCustomerName());
					object.put("shCompanyName", manualFedex.getShCompanyName());
					object.put("shAddress1", manualFedex.getShAddress1());
					object.put("shAddress2", manualFedex.getShAddress2());
					object.put("shCity", manualFedex.getShCity());
					object.put("shState", manualFedex.getShState());
					object.put("shStateName", manualFedex.getShStateName());					
					object.put("shCountry", manualFedex.getShCountry());
					object.put("shCountryName", manualFedex.getShCountryName());
					object.put("shZipCode", manualFedex.getShZipCode());
					object.put("shPhone", manualFedex.getShPhone());
					object.put("serviceType", manualFedex.getServiceType());
					object.put("signatureType", manualFedex.getSignatureType());
					object.put("fedexLabelPath", manualFedex.getFedexLabelPath());
					object.put("trackingNo", manualFedex.getTrackingNumber());
					if(manualFedex.getFedexLabelCreated()!= null && manualFedex.getFedexLabelCreated()){
						object.put("fedexLabelCreated", "Yes");
					}else{
						object.put("fedexLabelCreated", "No");
					}
					object.put("createdDate", manualFedex.getCreatedDateStr());
					object.put("createdBy", manualFedex.getCreatedBy());
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getDiscountCodeTrackingArray(List<DiscountCodeTracking> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(DiscountCodeTracking discountCodeTracking :list){
					object = new JSONObject();
					object.put("customerId", discountCodeTracking.getCustomerId());
					object.put("firstName", discountCodeTracking.getFirstName());
					object.put("lastName", discountCodeTracking.getLastName());
					object.put("email", discountCodeTracking.getEmail());
					object.put("referrerCode", discountCodeTracking.getReferrerCode());
					object.put("facebookCnt", discountCodeTracking.getFaceBookCnt());
					object.put("twitterCnt", discountCodeTracking.getTwitterCnt());
					object.put("linkedinCnt", discountCodeTracking.getLinkedinCnt());
					object.put("whatsappCnt", discountCodeTracking.getWhatsappCnt());					
					object.put("androidCnt", discountCodeTracking.getAndroidCnt());
					object.put("imessageCnt", discountCodeTracking.getImessageCnt());
					object.put("googleCnt", discountCodeTracking.getGoogleCnt());
					object.put("outlookCnt", discountCodeTracking.getOutlookCnt());
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONArray getContestArray(List<Contest> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(Contest cont :list){
					object = new JSONObject();
					object.put("id", cont.getId());
					object.put("name", cont.getName());
					object.put("minPurchaseAmt", cont.getMinimumPurchaseAmount());
					object.put("startDate", cont.getStartDateStr());
					object.put("endDate", cont.getEndDateStr());
					if(cont.getStatus()!= null && cont.getStatus()){
						object.put("status", "Yes");
					}else{
						object.put("status", "No");
					}
					object.put("artistName", cont.getArtistName());
					object.put("artistNameStr", cont.getArtistNameStr());
					object.put("artistId", cont.getArtistId());
					object.put("status",cont.getContestStatus());
					object.put("createdBy", cont.getCreatedBy());
					object.put("createdDate", cont.getCreatedDateStr());
					object.put("updatedBy", cont.getUpdatedBy());
					object.put("updatedDate", cont.getUpdatedDateStr());
					String startDate[] = cont.getStartDateStr().split(" ");
					object.put("fromDate", startDate[0]);
					String endDate[] = cont.getEndDateStr().split(" ");
					object.put("toDate", endDate[0]);
					
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static JSONObject getContestWinner(ContestWinner winner){
		JSONObject object = new JSONObject();
		try {
			if(winner!=null){
				object.put("id", winner.getId());
				object.put("winnerName", winner.getWinnerName());
				object.put("winnerEmail", winner.getWinnerEmail());
				object.put("winnerCustomerId", winner.getWinnerCustomerId());
				object.put("createdBy", winner.getCreatedBy());
				object.put("createdDate", winner.getCreatedOnStr());
				object.put("startDate", winner.getStartDateStr());
				object.put("endDate", winner.getEndDateStr());
				object.put("contestName", winner.getContestName());
				object.put("winnerCity", "N/A");
				if(winner.getCity() != null && !winner.getCity().isEmpty()){
					object.put("winnerCity", winner.getCity());
				}
				object.put("isEmailed","No");
				if(winner.getIsEmailed()){
					object.put("isEmailed","Yes");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
	public static JSONArray getContestParticipantsArray(List<ContestParticipants> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;
				for(ContestParticipants contParticipant :list){
					object = new JSONObject();
					object.put("id", contParticipant.getId());
					object.put("contestId", contParticipant.getContestId());
					object.put("customerId", contParticipant.getCustomerId());
					object.put("customerName", contParticipant.getCustomerName());
					object.put("customerEmail", contParticipant.getCustomerEmail());
					object.put("purchaseCustomerId", contParticipant.getPurchaseCustomerId());
					object.put("purchaseCustomerName", contParticipant.getPurchaseCustomerName());
					object.put("purchaseCustomerEmail", contParticipant.getPurchaseCustomerEmail());
					object.put("referralCode", contParticipant.getReferralCode());
					object.put("createdDate", contParticipant.getCreatedDateStr());
					
					array.put(object);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
	public static AffiliateSetting getAffiliateInfoObject(AffiliatePromoCodeHistory promoHistory, AffiliateSetting setting){
		
		try {
			if(promoHistory != null){
				setting.setPromoCode(promoHistory.getPromoCode());
				String fromDate[] = promoHistory.getFromDateStr().split(" ");
				setting.setFromDate(fromDate[0]);
				String toDate[] = promoHistory.getToDateStr().split(" ");
				setting.setToDate(toDate[0]);
			}
			if(setting != null){
				setting.setStatus(setting.getStatus());
				if(setting.getIsRepeatBusiness()){
					setting.setRepeatBusiness("YES");
				}else{
					setting.setRepeatBusiness("NO");
				}
				setting.setCashDiscount(setting.getCashDiscount()*100);
				setting.setCustomerDiscount(setting.getCustomerDiscount()*100);
				setting.setPhoneCashDiscount(setting.getPhoneCashDiscount()*100);
				setting.setPhoneCustomerDiscount(setting.getPhoneCustomerDiscount()*100);
				if(setting.getIsEarnRewardPoints()){
					setting.setEarnRewardPoints("YES");
				}else{
					setting.setEarnRewardPoints("NO");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return setting;
	}

	public static JSONArray getUserActionsArray(Collection<UserAction> list){
		JSONArray array = new JSONArray();
		try {
			if(list!=null){
				JSONObject object = null;				
				for(UserAction user : list){
					object = new JSONObject();
					object.put("id", user.getId());
					object.put("userName", user.getUserName());
					object.put("userId", user.getUserId());
					object.put("userAction", user.getAction());
					object.put("timeStampStr", user.getTimeStampStr());										
					object.put("clientIPAddress", user.getIpAddress());
					object.put("userMessage", user.getMessage());
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}*/	

	
	
	/*public static JSONArray getInvoiceTicketAttachmentsArray(List<InvoiceTicketAttachment> invoiceTicketList){
		JSONArray array = new JSONArray();
		try {
			if(invoiceTicketList!=null){
				JSONObject object = null;
				for(InvoiceTicketAttachment invoice : invoiceTicketList){
					object = new JSONObject();
					object.put("id", invoice.getId());
					object.put("invoiceId", invoice.getInvoiceId());
					object.put("fileName", Util.getFileNameFromPath(invoice.getFilePath()));
					object.put("fileType", invoice.getType());
					object.put("position", invoice.getPosition());
					if(Util.getFileNameFromPath(invoice.getFilePath()) != null){
						object.put("downloadTicket", "Yes");
					}else{
						object.put("downloadTicket", "No");
					}
					array.put(object);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}*/
}
