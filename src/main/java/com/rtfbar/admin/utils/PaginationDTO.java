package com.rtfbar.admin.utils;

public class PaginationDTO {

	private String pageNum;
	private Integer totalRows;
	private Integer pageSize;
	private Double totalPages;
	
	public String getPageNum() {
		return pageNum;
	}
	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}
	public Integer getTotalRows() {
		return totalRows;
	}
	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Double getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(Double totalPages) {
		this.totalPages = totalPages;
	}	
	
}
