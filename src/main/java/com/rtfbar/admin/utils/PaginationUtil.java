package com.rtfbar.admin.utils;

import org.json.JSONObject;



public class PaginationUtil {
	
	public static final Integer PAGESIZE = 50;
	
	public static JSONObject calculatePaginationParameters(Integer count,String pageNo){
		JSONObject paginationObject  = new JSONObject();
		try {
			if(pageNo==null || pageNo.isEmpty()){
				pageNo = "0";
			}
			if(count==null){
				count =0;
			}
			paginationObject.put("pageSize", PAGESIZE);
			paginationObject.put("pageNum", pageNo);
			paginationObject.put("totalRows", count);
			Double size =  ((double)count/(double)PAGESIZE);
			paginationObject.put("totalPages", Math.ceil(size));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paginationObject;
	}
	
	public static JSONObject getDummyPaginationParameters(Integer count){
		JSONObject paginationObject  = new JSONObject();
		try {
			paginationObject.put("pageSize", count);
			paginationObject.put("pageNum", 0);
			paginationObject.put("totalRows", count);
			paginationObject.put("totalPages",1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paginationObject;
	}
	
	public static PaginationDTO getDummyPaginationParameter(Integer count){
		PaginationDTO paginationDTO = new PaginationDTO();
		try {
			paginationDTO.setPageSize(count);
			paginationDTO.setPageNum(String.valueOf(0));
			paginationDTO.setTotalRows(count);
			paginationDTO.setTotalPages(1D);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paginationDTO;
	}
	
	
	public static Integer getNextPageStatFrom(String pageNo){
		Integer startFrom = 0;
		if(pageNo !=null && !pageNo.isEmpty() && !pageNo.equals("0")){
			startFrom = (Integer.parseInt(pageNo) * PaginationUtil.PAGESIZE)+1;
		}
		return startFrom;
	}

}
