package com.rtfbar.admin.wpdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;


public class DatabaseConnection {

	// JDBC Driver Name & Database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String JDBC_DB_URL = "jdbc:mysql://107.180.12.40:3306/funzataAdmin";

	// JDBC Database Credentials
	static final String JDBC_USER = "funzataAdmin";
	static final String JDBC_PASS = "funzataAdmin";

	private static GenericObjectPool gPool = null;

	@SuppressWarnings("unused")
	public static DataSource setUpPool() throws Exception {
		Class.forName(JDBC_DRIVER);

		// Creates an Instance of GenericObjectPool That Holds Our Pool of Connections Object!
		gPool = new GenericObjectPool();
		gPool.setMaxActive(0);

		// Creates a ConnectionFactory Object Which Will Be Use by the Pool to Create the Connection Object!
		ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, JDBC_USER, JDBC_PASS);

		// Creates a PoolableConnectionFactory That Will Wraps the Connection Object Created by the ConnectionFactory to Add Object Pooling Functionality!
		PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
		return new PoolingDataSource(gPool);
	}

	public static DataSource getDataSource() {
		DataSource dataSrc = null;
		try {
			if(gPool == null){
				dataSrc = DatabaseConnection.setUpPool();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataSrc;
	}
	
	
	public static GenericObjectPool getConnectionPool() {
		return gPool;
	}
	

	// This Method Is Used To Print The Connection Pool Status
	private static void printDbStatus() {
		System.out.println("Max.: " + getConnectionPool().getMaxActive() + "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
	}

	public static void main(String[] args) {
		ResultSet rsObj = null;
		Connection connObj = null;
		PreparedStatement pstmtObj = null;
		try {	
			connObj = getConnection();
			pstmtObj = connObj.prepareStatement("SELECT * FROM fun_new.wp3r_sponsors WHERE product_id=725");
			rsObj = pstmtObj.executeQuery();
			while (rsObj.next()) {
				System.out.println("Action ID : " + rsObj.getString("email"));
			}
		} catch(Exception sqlException) {
			sqlException.printStackTrace();
		} finally {
			closeConnection(connObj);
		}
		DatabaseConnection.printDbStatus();
	}
	
	public static Connection getConnection(){
		Connection connObj = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");  
			connObj=DriverManager.getConnection("jdbc:mysql://107.180.12.40:3306/fun_new","funzataAdmin","funzataAdmin");
			/*DataSource dataSource = DatabaseConnection.getDataSource();
			DatabaseConnection.printDbStatus();
			connObj = dataSource.getConnection();
			DatabaseConnection.printDbStatus(); */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connObj;
	}


	
	public static void closeConnection(Connection connection){
		try {
			if(connection != null) {
				connection.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
