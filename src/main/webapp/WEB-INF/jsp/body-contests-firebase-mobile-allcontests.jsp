<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="/funzata/resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="/funzata/resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="/funzata/resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="/funzata/resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="/funzata/resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="/funzata/resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="/funzata/resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="/funzata/resources/js/slick/slick.core.js"></script>
<script src="/funzata/resources/js/slick/slick.formatters.js"></script>
<script src="/funzata/resources/js/slick/slick.editors.js"></script>
<script src="/funzata/resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="/funzata/resources/js/slick/slick.grid.js"></script>
<script src="/funzata/resources/js/slick/slick.dataview.js"></script>
<script src="/funzata/resources/js/slick/controls/slick.pager.js"></script>
<script src="/funzata/resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="/funzata/resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="/funzata/resources/js/slick/plugins/slick.autotooltips.js"></script>


<style>
#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenu1 {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu1 li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu1 li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var questionArray = [];
var j = 0;
$(document).ready(function() {
	$('#contest_fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});

	$('#contest_toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$('#expiryDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	
	$("#allContest1").click(function(){
		callTabOnChange('ALL');
	});
	$("#expiredContest1").click(function(){
		callTabOnChange('EXPIRED');
	});
		
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(contestsGrid != null && contestsGrid != undefined){
			contestsGrid.resizeCanvas();
		}
		if(questionGrid != null && questionGrid != undefined){
			questionGrid.resizeCanvas();
		}
		/* if(contestQuesBankGrid != null && contestQuesBankGrid != undefined){
			contestQuesBankGrid.resizeCanvas();
		} */
	});
	
	$('#contestQuestionTab').click(function(){				
		var contestsId = $('#contestIdStr').val();
		
		setTimeout(function(){
			getQuestionGridData(contestsId, 0);},10);
	});
	<c:choose>
	<c:when test="${status == 'ALL'}">	
		$('#allContest').addClass('active');
		$('#allContestTab').addClass('active');
		$('#expiredContest').removeClass('active');
		$('#expiredContestTab').removeClass('active');
	</c:when>
	<c:when test="${status == 'EXPIRED'}">
		$('#allContest').removeClass('active');
		$('#allContestTab').removeClass('active');
		$('#expiredContest').addClass('active');
		$('#expiredContestTab').addClass('active');
	</c:when>
	</c:choose>
	
});

function openAddContestModal(){
	resetModal();
	$('#myModal-2').modal('show');
}


function resetModal(){
	$('#contestForm :input').attr('disabled',false);
	$('#contest_id').val('');
	$('#contest_name').val('');
	//$('#prizeText').val('');
	$('#contest_fromDate').val('');
	$('#startDateHour').val('0');
	$('#startDateMinute').val('0');
	$('#maxWinners').val('');	
	$('#questionSize').val('');	
	$('#isPublicSponsor').val('false');
	//$('#prizeValue').val('');
	
	$('#saveBtn').show();
	$('#updateBtn').hide();
}

function resetQPositionModal(){
	var contestId = $('#q_contest_id').val();
	if(contestId == ''){
		jAlert("Please select Contest to update questions position.");
		return;
	}
	
	var tempContestRowIndex = contestsGrid.getSelectedRows([0])[0];
	if (tempContestRowIndex == null) {
		jAlert("Plese select Contest/Question to update position.", "info");
		return false;
	}
	
	var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
	if (tempQuestionRowIndex == null) {
		jAlert("Plese select Question to update position.", "info");
		return false;
	}	
	
	var questionText = questionGrid.getDataItem(tempQuestionRowIndex).question;
	var questionSize = contestsGrid.getDataItem(tempContestRowIndex).questionSize;
	$('#changeQPositionModal').modal('show');
	$('#changePQText').text(questionText);
	$('#changePositionSelect').empty();
	for(var i =1;i<=questionSize;i++){
		$('#changePositionSelect').append($('<option>', { 
	        value: i,
	        text : i 
	    }));
	}
	
}

function resetQModal(contestId){
	if(contestId == ''){
		jAlert("Please select Contest to add questions.");
		return;
	}
	$('#questionModal').modal('show');
	$('#questionId').val('');
	$('#questionText').val('');
	$('#optionA').val('');
	$('#optionB').val('');
	$('#optionC').val('');
	$('#answer').val('');
	
	$('#qSaveBtn').show();
	$('#qUpdateBtn').hide();	
}


function questionSave(action){
	var text = $('#questionText').val();
	var oA = $('#optionA').val();
	var oB = $('#optionB').val();
	var oC = $('#optionC').val();
	var answer = $('#answer').val();
	
	if(text == ''){
		jAlert("Question Text is Mandatory.");
		return;
	}
	if(oA == ''){
		jAlert("Option A is Mandatory.");
		return;
	}
	if(oB == ''){
		jAlert("Option B is Mandatory.");
		return;
	}
	if(oC == ''){
		jAlert("Option C is Mandatory.");
		return;
	}
	if(answer == ''){
		jAlert("Answer is Mandatory.");
		return;
	}
		
	var requestUrl = "${pageContext.request.contextPath}/UpdateQuestion";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#questionForm').serialize()+"&action=SAVE";
	}else if(action == 'update'){
		dataString = $('#questionForm').serialize()+"&action=UPDATE";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#questionModal').modal('hide');
				questionPagingInfo = jsonData.questionPagingInfo;
				questionColumnFilters = {};
				refreshContestQuestionGridValues(jsonData.questionList);
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function addSponsor(){
	$('#sSaveBtn').show();			
	$('#sUpdateBtn').hide();
	$('#pdId').val('');
	$('#brandName').val('');
	$('#sponsorId').val('');
	$('#userLimit').val(100);
	$('#winPrize').val('');
	$('#spfName').val('');
	$('#splName').val();
	$('#spEmail').val('');
	$('#spPhone').val('');
	$('#spAddr').val('');
	$('#finalistPrize').val('');
	$('#sponsorModal').modal('show');
}


function sponsorSave(action){
	var brandName = $('#brandName').val();
	var sponsorId = $('#sponsorId').val();
	var userLimit = $('#userLimit').val();
	var winPrize = $('#winPrize').val();
	var spfName = $('#spfName').val();
	var splName = $('#splName').val();
	var spEmail = $('#spEmail').val();
	var spPhone = $('#spPhone').val();
	
	if(brandName == ''){
		jAlert("Sponsor name is Mandatory.");
		return;
	}
	if(sponsorId == ''){
		jAlert("Sponsor ID is Mandatory.");
		return;
	}
	if(sponsorId.length < 6){
		jAlert("Sponsor ID can be alpha numeric string with minimum lenth 6 charater.");
		return;
	}
	if(userLimit == ''){
		jAlert("Max user limit is Mandatory.");
		return;
	}
	if(Number.isNaN(userLimit)){
		jAlert("Max user limit should be only numeric value.");
		return;
	}
	if(winPrize == ''){
		jAlert("Winner prize is Mandatory.");
		return;
	}
	if(spfName == ''){
		jAlert("Sponsor first name is Mandatory.");
		return;
	}
	if(splName == ''){
		jAlert("Sponsor last name Mandatory.");
		return;
	}
	if(spEmail == ''){
		jAlert("Sponsor Email is Mandatory.");
		return;
	}
	if(spPhone == ''){
		jAlert("Sponsor Phone is Mandatory.");
		return;
	}
		
	var requestUrl = "${pageContext.request.contextPath}/UpdateSponsor";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#sponsorForm').serialize()+"&action=SAVE";
	}else if(action == 'update'){
		dataString = $('#sponsorForm').serialize()+"&action=UPDATE";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#sponsorModal').modal('hide');
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
			contestBrandsPagingInfo = jsonData.packagePagingInfo;
			refreshContestBrandsGridValues(jsonData.packageList);
			clearAllSelections();
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}



function contestSave(action){
	
	var contestName = $('#contest_name').val();
	var fromDate = $('#contest_fromDate').val();
	var maxWinners = $('#maxWinners').val();	
	var prizeValue = $('#prizeValue').val();
	var prizeQty = $('#prizeQty').val();
	var prizeText = $('#prizeText').val();
	var questionSize = $('#questionSize').val();
	
	if(contestName == ''){
		jAlert("Contest Name is Mandatory.");
		return;
	}
	if(maxWinners == ''){
		jAlert("Please enter No of winners(Max. No. of users can win prize)");
		return;
	}
	if(questionSize == ''){
		jAlert("No. of questions are mendatory.");
		return;
	}
	if(prizeQty == ''){
		jAlert("Per winner prize Qunatity is mendatory.");
		return;
	}
	if(prizeText == ''){
		jAlert("Winner prize is mendatory.");
		return;
	}
	if(prizeValue == ''){
		jAlert("Winner prize value is mendatory.");
		return;
	}
	if(fromDate == ''){
		jAlert("Start Date is Mandatory.");
		return;
	}
	
	$('#contestForm :input').attr('disabled',false);
	var requestUrl = "${pageContext.request.contextPath}/UpdateContest";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#contestForm').serialize()+"&action=SAVE&status=${status}";
	}else if(action == 'update'){
		dataString = $('#contestForm').serialize()+"&action=UPDATE&status=${status}";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#myModal-2').modal('hide');
				pagingInfo = jsonData.contestsPagingInfo;
				columnFilters = {};
				refreshContestsGridValues(jsonData.contestsList);
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/Contest?status="+selectedTab;
}


</script>
<ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
</ul>
<ul id="contextMenu1" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
</ul>

	<!-- Navigation info -->
	<ul id="nav-info" class="clearfix">
		<li><a href="${pageContext.request.contextPath}"><i class="fa fa-home"></i></a></li>
		<li class="active"><a href="${pageContext.request.contextPath}/Contest?status=${status}">Manage Contest</a></li>
	</ul>

	<div class="push">
		<ul class="nav nav-tabs">
			<li id="allContestTab" class="active"><a id="allContest1" href="#allContest" data-toggle="tab">Active Contests</a></li>
			<li id="expiredContestTab"><a id="expiredContest1" href="#expiredContest" data-toggle="tab">Expired Contests</a></li>
		</ul>
		<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	</div>
	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="allContest" class="tab-pane">
			<c:if test="${status =='ALL'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary"  onclick="openAddContestModal();">Create Contest</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Active Contests</label>
							<div class="pull-right">
								<!-- <a href="javascript:exportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contests_grid" style="width: 100%; height: 250px;"></div>
						<div id="contests_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<input type="hidden" name="contestIdStr" id="contestIdStr"/>
				<br />
				</c:if>
			</div>
			<div id="expiredContest" class="tab-pane">
			<c:if test="${status =='EXPIRED'}">	
				
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Expired Contests</label>
							<div class="pull-right">
								<!-- <a href="javascript:exportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contests_grid" style="width: 100%; height: 250px; "></div>
						<div id="contests_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<input type="hidden" name="contestIdStr" id="contestIdStr"/>
				<br />
			</c:if>
			</div>																				
		</div>
	</div>	
		
	<div style="position: relative" id="questionGridDiv" class="tab-pane active">
		<br/></br/>
		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Contests Questions</label>
				<div class="pull-right">
					<!-- <a href="javascript:questionExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
					<a href="javascript:questionResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
				</div>
			</div>
			<div id="question_grid" style="width: 100%; height: 430px;"></div>
			<div id="question_pager" style="width: 100%; height: 10px;"></div>
	
		</div>
	</div>
					
</div>


<!-- Add Contest -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest</h4>
			</div>
			<div class="modal-body">
					<div class="form-group tab-fields">
					<form name="contestForm" id="contestForm" method="post">
					<input type="hidden" id="contest_id" name="contestId" />
					<input type="hidden" id="prizeText" name="prizeText" value="N/A" >
					<input type="hidden" id="prizeValue" name="prizeValue" value="0">
					<input type="hidden" id="prizeQty" name="prizeQty" value="0">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Contest Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_name" name="contestName" >
						</div>	
						<div class="form-group col-sm-6 col-xs-6">
							<label>Start Date <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_fromDate" name="contestFromDate" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Start Time <span class="required">*</span>
							<select name="startDateHour" id="startDateHour" class="form-control" style="width:120px;" >
								<option value="0">00</option>
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
								<option value="4">04</option>
								<option value="5">05</option>
								<option value="6">06</option>
								<option value="7">07</option>
								<option value="8">08</option>
								<option value="9">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Minutes <span class="required">*</span>
							<select name="startDateMinute" id="startDateMinute" class="form-control" style="width:120px;" >
								<option value="0">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>
							</select>
							<input type="hidden" id="type" name="type" value="MOBILE" />
						</div>
						<!-- <div class="form-group col-sm-6 col-xs-6">
							<label>Winner Prize 
							</label> <input class="form-control" type="text" id="prizeText" name="prizeText" >
						</div> -->	
						<div class="form-group col-sm-3 col-xs-3">
							<label>No. Of Winners<span class="required">*</span>
							</label> <input class="form-control" type="text" id="maxWinners" name="maxWinners" >
						</div>
						<!-- <div class="form-group col-sm-3 col-xs-3">
							<label>Prize Qty.  per Winner<span class="required">*</span>
							</label> <input class="form-control" type="text" id="prizeQty" name="prizeQty" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Prize Value Per Winner  <span class="required">*</span>
							</label> <input class="form-control" type="text" id="prizeValue" name="prizeValue" >
						</div> -->
						<div class="form-group col-sm-3 col-xs-3" id="contestQSizeDiv">
							<label>Number of Questions<span class="required">*</span></label> 
							<input class="form-control" type="text" id="questionSize" name="questionSize" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Display Sponsors Code in APP?<span class="required">*</span>
							<select name="isPublicSponsor" id="isPublicSponsor" class="form-control" style="width:120px;" >
								<option value="false">NO</option>
								<option value="true">YES</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer" style="margin-top:22%;">				
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="contestSave('save')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="contestSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Ends of Add Shipping/Other Address popup-->

<!-- Add Questions -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="questionModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Question</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionForm" id="questionForm" method="post">
					<input type="hidden" id="q_contest_id" name="qContestId" />
					<input type="hidden" id="questionId" name="questionId" />
					<div id="startContestDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Question Text<span class="required">*</span>
							</label> <input class="form-control" type="text" id="questionText" name="questionText">
						</div>						
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option A <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionA" name="optionA">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option B <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionB" name="optionB">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option C <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionC" name="optionC">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Answer <span class="required">*</span>
							<select name="answer" id="answer" class="form-control">
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<!-- <option value="D">D</option> -->
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer" style="margin-top:30%;">
				<button class="btn btn-primary" id="qSaveBtn" type="button" onclick="questionSave('save')">Save</button>
				<button class="btn btn-primary" id="qUpdateBtn" type="button" onclick="questionSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add Question end here  -->



<!-- Add Sponsor -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sponsorModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Sponsor</h4>
			</div>
			<div class="modal-body full-width">
				<form name="sponsorForm" id="sponsorForm" method="post">
					<input type="hidden" id="s_contest_id" name="sContestId" />
					<input type="hidden" id="pdId" name="pdId" />
					<div id="startContestDiv" class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Sponsor Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="brandName" name="brandName">
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Sponsor ID<span class="required">*</span>
							</label> <input class="form-control" type="text" id="sponsorId" name="sponsorId">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Max User Limit<span class="required">*</span>
							</label> <input class="form-control" type="text" id="userLimit" name="userLimit">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Finalist Prize
							</label> <input class="form-control" type="text" id="finalistPrize" name="finalistPrize">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Winner Prize<span class="required">*</span>
							</label> <input class="form-control" type="text" id="winPrize" name="winPrize">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Sponsor First Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="spfName" name="spfName">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Sponsor Last Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="splName" name="splName">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Sponsor Email<span class="required">*</span>
							</label> <input class="form-control" type="text" id="spEmail" name="spEmail">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Sponsor Phone<span class="required">*</span>
							</label> <input class="form-control" type="text" id="spPhone" name="spPhone">
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label>Sponsor Address
							</label> <input class="form-control" type="text" id="spAddr" name="spAddr">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer" style="margin-top:50%;">
				<button class="btn btn-primary" id="sSaveBtn" type="button" onclick="sponsorSave('save')">Save</button>
				<button class="btn btn-primary" id="sUpdateBtn" type="button" onclick="sponsorSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add Sponsor end here  -->


<!--Change Question position Start here -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="changeQPositionModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Change Question Position</h4>
			</div>
			<div class="modal-body full-width">
					<div id="startContestDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label id="changePQText">
							</label> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							&nbsp;&nbsp;
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Select Position <span class="required">*</span>
							<select name="changePositionSelect" id="changePositionSelect" class="form-control">
							</select>
						</div>
					</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="chagneQPUpdateBtn" type="button" onclick="updateQuestionPosition('','MANUAL')">Update</button>
				<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Change Question position end here  -->


<!-- Add Question(s) from  Question Bank Modal -->
	<div id="add-question-bank" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Question(s) - Contest : <span id="contestName_Hdr_QuesBank" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Contest
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Contest</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Question Bank</li>
						</ol>
					</div>
				</div>
				<br />
				
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Questions</label>
							<div class="pull-right">
								<!-- <a href="javascript:contestQuesBankExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:contestQuesBankResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contestQuesBank_grid" style="width: 100%; height: 280px;"></div>
						<div id="contestQuesBank_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="contestId_QuesBank" name="contestId_QuesBank" />
				<button type="button" class="btn btn-primary" onclick="addQuestionsFromQuesBank()">Add Question(s)</button>				
				<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	<!-- End - Add Question(s) from  Question Bank Modal -->
	
	
	<!--Contest Participants Brands Model -->
	<div id="contest-parti-brands" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width" width="1000px">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Participant Sponsors</h4>
			</div>
			<div class="modal-body full-width">
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary"  onclick="addSponsor();">Add Sponsor</button>
				</div>
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Participant Sponsors</label>
							<div class="pull-right">
								<!-- <a href="javascript:contestQuesBankExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:contestBrandsResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contestBrands_grid" style="width: 100%; height: 280px;"></div>
						<div id="contestBrands_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" name="brandsCoId" id="brandsCoId">
				<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	<!-- End - Add Question(s) from  Question Bank Modal -->
	
	
	<!-- Winners Modal start  -->
	<div id="view-contest-winners" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Winners</h4>
			</div>
			<div class="modal-body full-width">
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Contest Winners</label>
							<div class="pull-right">
								<!-- <a href="javascript:winnerExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:contestWinnerResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contestGrandWinner_grid" style="width: 100%; height: 200px;"></div>
						<div id="contestGrandWinner_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="contestId_winner" name="contestId_winner" />
				<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	<!-- Winners Modal End  -->

<script type="text/javascript">
	
	//Contest Grid
	function getContestsGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/Contest.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+contestsSearchString+"&status=${status}",
			success : function(response){
				var jsonData = response;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.contestsPagingInfo;
				refreshContestsGridValues(jsonData.contestsList);
				clearAllSelections();				
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	/* function exportToExcel(type){
		var appendData = "headerFilter="+contestsSearchString+"&status="+type+"&type=MOBILE";
	    var url = apiServerUrl+"ContestExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function winnerExportToExcel(){
		var contId = $('#contestIdStr').val();
		var appendData = "contestId="+contId+"&headerFilter="+contestGrandWinnerSearchString;
	    var url = apiServerUrl+"${pageContext.request.contextPath}/GetWinnersExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function contestQuesBankExportToExcel(){
		var appendData = "headerFilter="+contestQuesBankSearchString+"&status=ACTIVE";
	    var url ="${pageContext.request.contextPath}/ContestQuesBankExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	} */
	
	
	function resetFilters(){
		contestsSearchString='';
		columnFilters = {};
		getContestsGridData(0);
		//refreshQuestionGridValues('');
	}

	/* var contestsCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); */
	
	var pagingInfo;
	var contestsDataView;
	var contestsGrid;
	var contestsData = [];
	var contestsGridPager;
	var contestsSearchString='';
	var columnFilters = {};
	var userContestsColumnsStr = '<%=session.getAttribute("contestsgrid")%>';

	var userContestsColumns = [];
	var loadContestsColumns = ["contestName", "startDate","maxWinners", "questionSize", "status","actionColumn"];
	var allContestsColumns = [ {
				id : "contestId",
				name : "Contest ID",
				field : "contestId",
				width : 80,
				sortable : true
			},{
				id : "contestName",
				name : "Contest Name",
				field : "contestName",
				width : 80,
				sortable : true
			}, {
				id : "startDate",
				name : "Start Date Time",
				field : "startDate",
				width : 80,
				sortable : true
			},/* {
				id : "prizeQty",
				name : "Prize Qty. Per Winner",
				field : "prizeQty",
				width : 80,
				sortable : true
			}, */{
				id : "maxWinners",
				name : "No. Of Winners",
				field : "maxWinners",
				width : 80,
				sortable : true
			},/* {
				id : "prizeValue",
				name : "Prize Value Per Winner",
				field : "prizeValue",
				width : 80,
				sortable : true
			},{
				id : "prizeText",
				name : "Winner Prize",
				field : "prizeText",
				width : 80,
				sortable : true
			}, */{
				id : "questionSize",
				name : "Number of Que.",
				field : "questionSize",
				width : 80,
				sortable : true
			}, 
			{
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			}, {
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			}, {
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			}, {
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			}, {
				id : "updatedBy",
				field : "updatedBy",
				name : "ModifiedBy ",
				width : 80,
				sortable : true
			},{
				id : "actionColumn",
				field : "actionColumn",
				name : "Actions ",
				width : 100,
				formatter:buttonFormatter
			}];

	if (userContestsColumnsStr != 'null' && userContestsColumnsStr != '') {
		columnOrder = userContestsColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestsColumns.length; j++) {
				if (columnWidth[0] == allContestsColumns[j].id) {
					userContestsColumns[i] = allContestsColumns[j];
					userContestsColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		//userContestsColumns = allContestsColumns;
		var columnOrder = loadContestsColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allContestsColumns.length;j++){
				if(columnWidth == allContestsColumns[j].id){
					userContestsColumns[i] = allContestsColumns[j];
					if(allContestsColumns[j].id == 'actionColumn'){
						userContestsColumns[i].width=500;
					}else if(allContestsColumns[j].id == 'contestName'){
						userContestsColumns[i].width=200;
					}else if(allContestsColumns[j].id == 'startDate'){
						userContestsColumns[i].width=150;
					}else {
						userContestsColumns[i].width=75;
					}
					
					break;
				}
			}			
		}
	}

	function getContestUserExport(contestId){
	    var url = apiServerUrl + "ExportContestUsers?contestId="+contestId;
	    $('#download-frame').attr('src', url);
	}
	
	
	
	function buttonFormatter(row, cell, value, columnDef, dataContext) {
		//var button = "<button class='delete' onclick="window.delete(0)"> id='"+ dataContext.contestId +"'></i>";
		//button += "<i class='fa fa-pencil' id='"+ dataContext.contestId +"'></i>";		
		//return button;
		if(dataContext.status == 'ACTIVE'){
			return '<div class="myIcon">'+
				'<span onClick="viewParticiapantBrands('+dataContext.contestId+');">Participant Sponsors</span>'+
				'<span onClick="activatePasscodes('+dataContext.contestId+');">Activate Sponsor IDs</span>'+
				'<span onClick="endContest('+dataContext.contestId+');">END</span>'+
				'<span onClick="resetContest('+dataContext.contestId+');">RESET</span>'+
				'<span onClick="resetQModal('+dataContext.contestId+');">ADD Q</span>'+
			  '<img class="delete" style="max-width: 21px;" src="/funzata/resources/img/delete.png" id="'+ dataContext.contestId +'"/>'+
			  '<img class ="edit"style="max-width: 18px;" src="/funzata/resources/img/edit.png" id="'+ dataContext.contestId +'"/>'+
			'</div>';
		}else if(dataContext.status == 'EXPIRED'){
			return '<div class="myIcon">'+
			'<span onClick="viewWinners('+dataContext.contestId+')">Winners</span>'+
				'<span onClick="viewParticiapantBrands('+dataContext.contestId+');">Participant Sponsors</span>'+
				'<span onClick="resetQModal('+dataContext.contestId+');">ADD Q</span>'+
				'<span onClick="moveToActive('+dataContext.contestId+');">Move to Active</span>'+
			  '<img class="delete" style="max-width: 21px;" src="/funzata/resources/img/delete.png" id="'+ dataContext.contestId +'"/>'+
			  '<img class ="edit"style="max-width: 18px;" src="/funzata/resources/img/edit.png" id="'+ dataContext.contestId +'"/>'+
			'</div>';
		}else if(dataContext.status == 'STARTED'){
			return '<div class="myIcon">'+
				'<span onClick="viewParticiapantBrands('+dataContext.contestId+');">Participant Sponsors</span>'+
				'<span onClick="endContest('+dataContext.contestId+');">END</span>'+
			  '<img class="delete" style="max-width: 21px;" src="/funzata/resources/img/delete.png" id="'+ dataContext.contestId +'"/>'+
			  '<img class ="edit"style="max-width: 18px;" src="/funzata/resources/img/edit.png" id="'+ dataContext.contestId +'"/>'+
			'</div>';
		}
		
	}
	$('body').on('click',".delete",function(){
	    var me = $(this), id = me.attr('id');
	    getDeleteContest(id);
	});
	$('body').on('click',".edit", function(){
	    var me = $(this), id = me.attr('id');
	    getEditContest(id);
	});
	
	var contestsOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var contestsGridSortcol = "contestId";
	var contestsGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestsGridComparer(a, b) {
		var x = a[contestsGridSortcol], y = b[contestsGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshContestsGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestsData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestsData[i] = {});
				d["id"] = i;
				d["contestId"] = data.id;
				d["contestName"] = data.contestName;
				d["startDate"] = data.startDateTimeStr;
				d["maxWinners"] = data.maxWinners;
				d["prizeText"] = data.prizeText;
				d["prizeQty"] = data.prizeQty;
				d["prizeValue"] = data.prizeValue;
				d["questionSize"] = data.questionSize;
				d["status"] = data.status;
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		contestsDataView = new Slick.Data.DataView();
		contestsGrid = new Slick.Grid("#contests_grid", contestsDataView,
				userContestsColumns, contestsOptions);
		contestsGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestsGrid.setSelectionModel(new Slick.RowSelectionModel());
		//contestsGrid.registerPlugin(contestsCheckboxSelector);
		
			contestsGridPager = new Slick.Controls.Pager(contestsDataView,
					contestsGrid, $("#contests_pager"),
					pagingInfo);
		var contestsGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestsColumns, contestsGrid, contestsOptions);
					
		contestsGrid.onSort.subscribe(function(e, args) {
			contestsGridSortdir = args.sortAsc ? 1 : -1;
			contestsGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestsDataView.fastSort(contestsGridSortcol, args.sortAsc);
			} else {
				contestsDataView.sort(contestsGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestsGrid
		contestsDataView.onRowCountChanged.subscribe(function(e, args) {
			contestsGrid.updateRowCount();
			contestsGrid.render();
		});
		contestsDataView.onRowsChanged.subscribe(function(e, args) {
			contestsGrid.invalidateRows(args.rows);
			contestsGrid.render();
		});
		$(contestsGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestsSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											contestsSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getContestsGridData(0);
								}
							}

						});
		contestsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editContest' && args.column.id != 'delContest'){
					if(args.column.id == 'startDate' || args.column.id == 'updatedDate' || args.column.id == 'createdDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		contestsGrid.init();
		
		var contestsRowIndex = -1;
		contestsGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
			if (tempContestsRowIndex != contestsRowIndex) {
				var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
				var selectCont = $('#q_contest_id').val();
				if(contestId != selectCont){
					$('#q_contest_id').val(contestId);
					$('#s_contest_id').val(contestId);
					$('#contestIdStr').val(contestId);
					getQuestionGridData(contestId,0);
				}
				
			}
		});
		// initialize the model after all the discountCodes have been hooked up
		contestsDataView.beginUpdate();
		contestsDataView.setItems(contestsData);
		//contestsDataView.setFilter(filter);
		contestsDataView.endUpdate();
		contestsDataView.syncGridSelection(contestsGrid, true);
		contestsGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserContestsPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = contestsGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		//saveUserPreference('contestsgrid', colStr);
	}
	
	function pagingControl(move, id) {
		if(id == 'contests_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getContestsGridData(pageNo);
		} else if(id == 'question_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(questionPagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(questionPagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(questionPagingInfo.pageNum) - 1;
			}
			var contestsId = $('#contestIdStr').val();
			getQuestionGridData(contestsId, pageNo);
		} else if(id == 'contestQuesBank_pager') {
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(contestQuesBankPagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(contestQuesBankPagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(contestQuesBankPagingInfo.pageNum) - 1;
			}
			getContestQuesBankGridData(pageNo);
		}
	}
	
		
	//Question Grid		
	
	function getQuestionGridData(contestId, pageNo){
		$('#contestQuestionTab').addClass('active');
		$('#questionGridDiv').addClass('active');
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestQuestions",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&pageNo="+pageNo+"&headerFilter="+questionSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				$('#q_contest_id').val(contestId);
				questionPagingInfo =JSON.parse(JSON.stringify(jsonData.questionPagingInfo));
				refreshContestQuestionGridValues(JSON.parse(JSON.stringify(jsonData.questionList)));
				if(jsonData.sts == 0){
					$('#questionGridDiv').find('.grid-canvas').append('<br/><br/><p align="center" style="font-size:20px">'+jsonData.msg+'</p>');
				}
				//$('#question_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserQuestionPreference()'>");
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	function updateQuestionPosition(){
		var contestId = $('#q_contest_id').val();
		if(contestId == '' || contestId == undefined){
			jAlert("Please select contest first to update question position.");
			return;
		}
		if(questionData == '' || questionData.length <= 0){
			jAlert("question records are not found, please refersh page and try again.");
			return;
		}
		var questionString = '';
		for(var i=0;i<questionData.length;i++){
			var position = questionData[i].serialNo;
			if(position=='' || position == undefined){
				jAlert("Position is not entered for some of the question, please add unique and sequencial position to all question.");
				return;
			}
			questionString += questionData[i].questionId+':'+position+',';
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateQuestionPosition",
			type : "post",
			dataType: "json",
			data : "contestId="+contestId+"&questionString="+questionString,
			success : function(response) {
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				/* if(action=='MANUAL'){
					$('#changeQPositionModal').modal('hide');
				} */
				questionPagingInfo = jsonData.questionPagingInfo;
				refreshContestQuestionGridValues(jsonData.questionList);
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
			});
	}
	
	
	
	function questionResetFilters(){
		questionSearchString='';
		questionColumnFilters = {};
		var contstId = $('#q_contest_id').val();
		getQuestionGridData(contstId, 0);
	}
	
	var questionPagingInfo;
	var questionDataView;
	var questionGrid;
	var questionData = [];
	var questionGridPager;
	var questionSearchString='';
	var questionColumnFilters = {};
	var userQuestionColumnsStr = '<%=session.getAttribute("questiongrid")%>';
	var loadQuestionColumns = ["question", "optionA","optionB", "optionC", "answer","serialNo","actionQColumn"];
	var userQuestionColumns = [];
	var allQuestionColumns = [
			{
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "optionA",
				field : "optionA",
				name : "option A",
				width : 80,
				sortable : true
			},{
				id : "optionB",
				field : "optionB",
				name : "option B",
				width : 80,
				sortable : true
			},{
				id : "optionC",
				field : "optionC",
				name : "option C",
				width : 80,
				sortable : true
			}, {
				id : "answer",
				field : "answer",
				name : "Answer",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "serialNo",
				field : "serialNo",
				name : "Serial No",
				width : 80,
				editor:Slick.Editors.Integer,
				sortable : true
			},{
				id : "actionQColumn",
				field : "actionQColumn",
				name : "Actions",
				width : 100,
				formatter:questionFormatter
			}];

	if (userQuestionColumnsStr != 'null' && userQuestionColumnsStr != '') {
		columnOrder = userQuestionColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allQuestionColumns.length; j++) {
				if (columnWidth[0] == allQuestionColumns[j].id) {
					userQuestionColumns[i] = allQuestionColumns[j];
					userQuestionColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		//userQuestionColumns = allQuestionColumns;
		var columnOrder = loadQuestionColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allQuestionColumns.length;j++){
				if(columnWidth == allQuestionColumns[j].id){
					userQuestionColumns[i] = allQuestionColumns[j];
					if(allQuestionColumns[j].id == 'actionColumn'){
						userQuestionColumns[i].width=80;
					}else if(allQuestionColumns[j].id == 'question'){
						userQuestionColumns[i].width=300;
					}else {
						userQuestionColumns[i].width=80;
					}
					break;
				}
			}			
		}
	}
	
	
	function questionFormatter(row, cell, value, columnDef, dataContext) {
		return '<div class="myIcon">'+
		  '<img class="deleteQue" style="max-width: 21px;" src="/funzata/resources/img/delete.png" id="'+ dataContext.questionId +'"/>'+
		  '<img class ="editQue"style="max-width: 18px;" src="/funzata/resources/img/edit.png" id="'+ dataContext.questionId +'"/>'+
		'</div>';
	}
	
	$('body').on('click',".deleteQue",function(){
	    var me = $(this), id = me.attr('id');
	    deleteQuestion(id);
	});
	$('body').on('click',".editQue", function(){
	    var me = $(this), id = me.attr('id');
	    editQuestion(id);
	});
	
	
		
	var questionOptions = {
		editable : true,
		enableAddRow : false,
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		asyncEditorLoading : true,
		explicitInitialization : true
	};
	var questionGridSortcol = "questionId";
	var questionGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function questionGridComparer(a, b) {
		var x = a[questionGridSortcol], y = b[questionGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	

	function refreshContestQuestionGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		questionData = [];
		if (jsonData != null) {
			questionArray = jsonData;
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (questionData[i] = {});
				d["id"] = i;
				d["questionId"] = data.id;
				d["contestId"] = data.contestId;
				d["question"] = data.question;
				d["optionA"] = data.optionA;
				d["optionB"] = data.optionB;
				d["optionC"] = data.optionC;
				d["serialNo"] = data.serialNo;
				d["answer"] = data.answer;
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		questionDataView = new Slick.Data.DataView();
		questionGrid = new Slick.Grid("#question_grid", questionDataView,
				userQuestionColumns, questionOptions);
		questionGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		questionGrid.setSelectionModel(new Slick.RowSelectionModel());
		//questionGrid.registerPlugin(contestsCheckboxSelector);
		
		questionGridPager = new Slick.Controls.Pager(questionDataView,
					questionGrid, $("#question_pager"),
					questionPagingInfo);
		var questionGridColumnpicker = new Slick.Controls.ColumnPicker(
				allQuestionColumns, questionGrid, questionOptions);
		
		
		questionGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = questionGrid.getCellFromEvent(e);
		      questionGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy question'){
				var tempQuestionBankRowIndex = questionGrid.getSelectedRows([0])[0];
				if (tempQuestionBankRowIndex == null) {
					jAlert("Plese select Question to Copy", "info");
					return false;
				}else {
					var questionText = questionGrid.getDataItem(tempQuestionBankRowIndex).question;
					var a = questionGrid.getDataItem(tempQuestionBankRowIndex).optionA;
					var b = questionGrid.getDataItem(tempQuestionBankRowIndex).optionB;
					var c = questionGrid.getDataItem(tempQuestionBankRowIndex).optionC;
					var answer = questionGrid.getDataItem(tempQuestionBankRowIndex).answer;
					var copyText = questionText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}
		});
		
	
		questionGrid.onSort.subscribe(function(e, args) {
			questionGridSortdir = args.sortAsc ? 1 : -1;
			questionGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				questionDataView.fastSort(questionGridSortcol, args.sortAsc);
			} else {
				questionDataView.sort(questionGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the questionGrid
		questionDataView.onRowCountChanged.subscribe(function(e, args) {
			questionGrid.updateRowCount();
			questionGrid.render();
		});
		questionDataView.onRowsChanged.subscribe(function(e, args) {
			questionGrid.invalidateRows(args.rows);
			questionGrid.render();
		});
		$(questionGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							questionSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								questionColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in questionColumnFilters) {
										if (columnId !== undefined
												&& questionColumnFilters[columnId] !== "") {
											questionSearchString += columnId
													+ ":"
													+ questionColumnFilters[columnId]
													+ ",";
										}
									}
									var contstsId = $('#q_contest_id').val();
									getQuestionGridData(contstsId, 0);
								}
							}

						});
		questionGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(questionColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(questionColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		questionGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		questionDataView.beginUpdate();
		questionDataView.setItems(questionData);
		//questionDataView.setFilter(filter);
		questionDataView.endUpdate();
		questionDataView.syncGridSelection(questionGrid, true);
		questionGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserQuestionPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = questionGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		//saveUserPreference('questiongrid', colStr);
	}
			
	//Edit Contest
	function editContest(){
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select contest to Edit", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			getEditContest(contestId);
		}
	}
	
	
	
	function getEditContest(contestId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateContest",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					$('#myModal-2').modal('show');
					setEditContest(jsonData.contestsList);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function setEditContest(contestsEd){
		$('#saveBtn').hide();			
		$('#updateBtn').show();
		if(contestsEd != null && contestsEd.length > 0){
			for (var i = 0; i < contestsEd.length; i++){					
				var data = contestsEd[i];
				$('#contest_id').val(data.id);
				$('#contest_name').val(data.contestName);
				$('#prizeText').val(data.prizeText);
				$('#contest_fromDate').val(data.startDateStr);
				$('#startDateHour').val(data.hours);
				$('#startDateMinute').val(data.minutes);
				$('#maxWinners').val(data.maxWinners);	
				$('#prizeValue').val(data.prizeValue);
				$('#prizeQty').val(data.prizeQty);
				$('#questionSize').val(data.questionSize);
				$('#isPublicSponsor').val('false');
				if(data.isSponPublic == true || data.isSponPublic == 'true'){
					$('#isPublicSponsor').val('true');
				}
				
				
			}
		}
	}
	
	//Edit Question
	function editQuestion(qId){
		if (qId == null || qId == undefined) {
			jAlert("Plese select Question to Edit", "info");
			return false;
		}else {
			getEditQuestion(qId);
		}
	}
		
	function getEditQuestion(questionId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateQuestion",
			type : "post",
			dataType: "json",
			data: "questionId="+questionId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					//$('#contestCategoryDiv').empty();					
					$('#questionModal').modal('show');
					setEditQuestion(jsonData.questionList);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditQuestion(question){
		$('#qSaveBtn').hide();			
		$('#qUpdateBtn').show();
		if(question != null && question.length > 0){
			for (var i = 0; i < question.length; i++){					
				var data = question[i];
				$('#questionId').val(data.id);
				$('#q_contest_id').val(data.contestId);
				$('#questionText').val(data.question);
				$('#optionA').val(data.optionA);
				$('#optionB').val(data.optionB);
				$('#optionC').val(data.optionC);
				$('#answer').val(data.answer);
			}
		}
	}
	
	//Delete Contest
	function deleteContest(){		
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select contest to Delete", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			getDeleteContest(contestId);
		}
	}
	
	function getDeleteContest(contestId){
		if (contestId == '') {
			jAlert("Please select a Contest to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete a Contest ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action=DELETE&status=${status}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.sts == 1){
								pagingInfo = jsonData.contestsPagingInfo;
								columnFilters = {};
								refreshContestsGridValues(jsonData.contestsList);
								refreshContestQuestionGridValues([]);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function deleteQuestion(qId){		
		var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
		if (qId == null || qId == undefined) {
			jAlert("Plese select Question to Delete", "info");
			return false;
		}else {
			var contestId = questionGrid.getDataItem(tempQuestionRowIndex).contestId;
			getDeleteQuestion(qId,contestId);
		}
	}
	
	function getDeleteQuestion(questionId,contestId){
		if (questionId == '') {
			jAlert("Please select a Question to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete selected Question ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateQuestion",
						type : "post",
						dataType: "json",
						data : "questionId="+questionId+"&qContestId="+contestId+"&action=DELETE",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.sts == 1){
								questionPagingInfo = jsonData.questionPagingInfo;
								questionColumnFilters = {};
								refreshContestQuestionGridValues(jsonData.questionList);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function resetContest(contestId){
		if (contestId == undefined || contestId < 0) {
			jAlert("Plese select Contest.", "info");
			return false;
		}
		var confirmText="Are you sure to reset Contest data ?";
		jConfirm(confirmText,"Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ResetContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action=RESET",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							getContestsGridData(0);
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	function endContest(contestId){
		if (contestId == undefined || contestId < 0) {
			jAlert("Plese select Contest.", "info");
			return false;
		}
		var confirmText="Are you sure to end contest ?";
		jConfirm(confirmText,"Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ResetContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action=END",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							getContestsGridData(0);
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	function moveToActive(contestId){
		if (contestId == undefined || contestId < 0) {
			jAlert("Plese select Contest.", "info");
			return false;
		}
		var confirmText="Are you sure tomove contest to active status ?";
		jConfirm(confirmText,"Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ResetContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action=ACTIVE",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							getContestsGridData(0);
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	function contestWinnerResetFilters(){
		contestGrandWinnerSearchString='';
		contestGrandWinnerColumnFilters = {};
		viewWinners();
	}
	
	
	function viewWinners(contestId){
		if(contestId == null || contestId == undefined){
			var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
			contestId = 0;
			if(tempContestsRowIndex >= 0) {
				contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			}
		}
		
		if(contestId <=0 ){
			jAlert("Please select contest to view winners.");
			return;
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestWinners.json",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&headerFilter="+contestGrandWinnerSearchString,
			success : function(response){
				var jsonData = response;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				contestGrandWinnerPagingInfo = jsonData.winnerPagingInfo;
				refreshContestGrandWinnerGridValues(jsonData.winnerList);
				$('#view-contest-winners').modal('show');			
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	//Contest winners
	var contestGrandWinnerPagingInfo;
	var contestGrandWinnerDataView;
	var contestGrandWinnerGrid;
	var contestGrandWinnerData = [];
	var contestGrandWinnerGridPager;
	var contestGrandWinnerSearchString='';
	var contestGrandWinnerColumnFilters = {};
	var userContestGrandWinnerColumnsStr = '<%=session.getAttribute("contestwinnergrid")%>';

	var userContestGrandWinnerColumns = [];
	var allContestGrandWinnerColumns = [
			{
				id : "userId",
				field : "userId",
				name : "User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Phone",
				width : 80,
				sortable : true
			}, {
				id : "brandName",
				field : "brandName",
				name : "Sponsor Name",
				width : 80,
				sortable : true
			}, {
				id : "passCode",
				field : "passCode",
				name : "Sponsor ID",
				width : 80,
				sortable : true
			}, {
				id : "prizeText",
				field : "prizeText",
				name : "Winner Prize",
				width : 80,
				sortable : true
			},{
				id : "isWinnerEmailSent",
				field : "isWinnerEmailSent",
				name : "Winning Email Sent?",
				width : 80,
				sortable : true
			}];

	if (userContestGrandWinnerColumnsStr != 'null' && userContestGrandWinnerColumnsStr != '') {
		columnOrder = userContestGrandWinnerColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestGrandWinnerColumns.length; j++) {
				if (columnWidth[0] == allContestGrandWinnerColumns[j].id) {
					userContestGrandWinnerColumns[i] = allContestGrandWinnerColumns[j];
					userContestGrandWinnerColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestGrandWinnerColumns = allContestGrandWinnerColumns;
	}
	
	var contestGrandWinnerOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var contestGrandWinnerGridSortcol = "id";
	var contestGrandWinnerGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestGrandWinnerGridComparer(a, b) {
		var x = a[contestGrandWinnerGridSortcol], y = b[contestGrandWinnerGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshContestGrandWinnerGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestGrandWinnerData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestGrandWinnerData[i] = {});
				d["id"] = i;
				d["winnerId"] = data.id;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["brandName"] = data.brandName;
				d["passCode"] = data.barcode;
				d["prizeText"] = data.prizeText;
				d["isWinnerEmailSent"] = "NO";
				if(data.isWinnerEmailSent == true || data.isWinnerEmailSent == 'true'){
					d["isWinnerEmailSent"] = "YES";
				}
			}
		}

		contestGrandWinnerDataView = new Slick.Data.DataView();
		contestGrandWinnerGrid = new Slick.Grid("#contestGrandWinner_grid", contestGrandWinnerDataView,
				userContestGrandWinnerColumns, contestGrandWinnerOptions);
		contestGrandWinnerGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestGrandWinnerGrid.setSelectionModel(new Slick.RowSelectionModel());
		
		contestGrandWinnerGridPager = new Slick.Controls.Pager(contestGrandWinnerDataView,
					contestGrandWinnerGrid, $("#contestGrandWinner_pager"),
					contestGrandWinnerPagingInfo);
		var contestGrandWinnerGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestGrandWinnerColumns, contestGrandWinnerGrid, contestGrandWinnerOptions);
			
		contestGrandWinnerGrid.onSort.subscribe(function(e, args) {
			contestGrandWinnerGridSortdir = args.sortAsc ? 1 : -1;
			contestGrandWinnerGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestGrandWinnerDataView.fastSort(contestGrandWinnerGridSortcol, args.sortAsc);
			} else {
				contestGrandWinnerDataView.sort(contestGrandWinnerGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestGrandWinnerGrid
		contestGrandWinnerDataView.onRowCountChanged.subscribe(function(e, args) {
			contestGrandWinnerGrid.updateRowCount();
			contestGrandWinnerGrid.render();
		});
		contestGrandWinnerDataView.onRowsChanged.subscribe(function(e, args) {
			contestGrandWinnerGrid.invalidateRows(args.rows);
			contestGrandWinnerGrid.render();
		});
		$(contestGrandWinnerGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestGrandWinnerSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								contestGrandWinnerColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in contestGrandWinnerColumnFilters) {
										if (columnId !== undefined
												&& contestGrandWinnerColumnFilters[columnId] !== "") {
											contestGrandWinnerSearchString += columnId
													+ ":"
													+ contestGrandWinnerColumnFilters[columnId]
													+ ",";
										}
									}
									viewWinners();
								}
							}

						});
		contestGrandWinnerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {				
				$("<input type='text'>").data("columnId", args.column.id)
						.val(contestGrandWinnerColumnFilters[args.column.id]).appendTo(
								args.node);				
			}
		});
		contestGrandWinnerGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		contestGrandWinnerDataView.beginUpdate();
		contestGrandWinnerDataView.setItems(contestGrandWinnerData);
		//contestGrandWinnerDataView.setFilter(filter);
		contestGrandWinnerDataView.endUpdate();
		contestGrandWinnerDataView.syncGridSelection(contestGrandWinnerGrid, true);
		//contestGrandWinnerGrid.resizeCanvas();
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
	}
	
	
	function saveUserContestQuesBankPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = contestQuesBankGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		//saveUserPreference('contestquesbankgrid', colStr);
	}
	
	function getSelectedContestQuesBankGridId() {
		var tempContestQuesBankRowIndex = contestQuesBankGrid.getSelectedRows();
		
		var quesBankIdStr='';
		$.each(tempContestQuesBankRowIndex, function (index, value) {
			quesBankIdStr += ','+contestQuesBankGrid.getDataItem(value).questionBankId;
		});
		
		if(quesBankIdStr != null && quesBankIdStr!='') {
			quesBankIdStr = quesBankIdStr.substring(1, quesBankIdStr.length);
			 return quesBankIdStr;
		}
	}
	
	function addQuestionsFromQuesBank(){
		var contestId = $('#contestId_QuesBank').val();
		var ids = getSelectedContestQuesBankGridId();
		if(ids==null || ids==''){
			jAlert("Please select atleast one Question(s) to add Contest Question");
			return;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateQuestion",
				type : "post",
				dataType : "json",
				data : "qContestId="+contestId+"&questionBankIds="+ids+"&action=ADDNEW",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#add-question-bank').modal('hide');
						questionPagingInfo = jsonData.questionPagingInfo;
						questionColumnFilters = {};
						refreshContestQuestionGridValues(jsonData.questionList);
						clearAllSelections();
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	
		//Contest Brands
		function viewParticiapantBrands(contestId){
			var tempContestGridRowIndex = contestsGrid.getSelectedRows([0])[0];
			if (contestId == null || contestId == undefined) {
				jAlert("Plese select contest to view participant brands", "info");
				return false;
			}else {
				$('#brandsCoId').val(contestId);
				$('#contest-parti-brands').modal('show');
				contestBrandsColumnFilters = {};
				getContestBrands(contestId);
			}		
		}
		
		function getContestBrands(contestId){
			$.ajax({
				url : "${pageContext.request.contextPath}/ContestParticipantBrands.json",
				type : "post",
				dataType: "json",
				data: "contestId="+contestId+"&headerFilter="+contestBrandsSearchString,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					contestBrandsPagingInfo = jsonData.packagePagingInfo;
					refreshContestBrandsGridValues(jsonData.packageList);clearAllSelections();
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						$('#contestBrands_grid').find('.grid-canvas').append('<br/><br/><p align="center" style="font-size:20px">'+jsonData.msg+'</p>');
					}
					
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		
		function activatePasscodes(contestId){
			$.ajax({
				url : "${pageContext.request.contextPath}/ActivatePasscodes.json",
				type : "post",
				dataType: "json",
				data: "contestId="+contestId,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}else{
						jAlert("Something went weong while activating Sponsor IDs.");
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		
		
		
		
		
		function contestBrandsResetFilters(){
			contestBrandsSearchString='';
			contestBrandsColumnFilters = {};
			var contestId = $('#brandsCoId').val();
			getContestBrands(contestId);
		}
		
		/* var contestBrandsCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		}); */
		
		var contestBrandsPagingInfo;
		var contestBrandsDataView;
		var contestBrandsGrid;
		var contestBrandsData = [];
		var contestBrandsGridPager;
		var contestBrandsSearchString='';
		var contestBrandsColumnFilters = {};
		var userContestBrandsColumnsStr = '<%=session.getAttribute("contestbrands")%>';
		
		var userContestBrandsColumns = [];
		var allContestBrandsColumns = [
			//contestBrandsCheckboxSelector.getColumnDefinition(),
				{
					id : "brandName",
					field : "brandName",
					name : "Sponsor Name",
					width : 80,
					sortable : true
				},{
					id : "email",
					field : "email",
					name : "Email",
					width : 80,
					sortable : true
				},{
					id : "phone",
					field : "phone",
					name : "Phone",
					width : 80,
					sortable : true
				},{
					id : "firstName",
					field : "firstName",
					name : "First Name",
					width : 80,
					sortable : true
				},{
					id : "lastName",
					field : "lastName",
					name : "Last Name",
					width : 80,
					sortable : true
				},{
					id : "passCode",
					field : "passCode",
					name : "Sponsor ID",
					width : 80,
					sortable : true
				},{
					id : "userLimitCnt",
					field : "userLimitCnt",
					name : "Max. User Limit",
					width : 50,
					sortable : true
				},{
					id : "actUserCnt",
					field : "actUserCnt",
					name : "Active Users",
					width : 50,
					sortable : true
				},{
					id : "winnerPrize",
					field : "winnerPrize",
					name : "Winner Prize",
					width : 80,
					sortable : true
				}, {
					id : "finelistPrize",
					field : "finelistPrize",
					name : "Finalist Prize",
					width : 80,
					sortable : true
				},/* {
					id : "isWinnerEmailSent",
					field : "isWinnerEmailSent",
					name : "Winner Email Sent?",
					width : 50,
					sortable : true
				},{
					id : "crDateTimeStr",
					field : "crDateTimeStr",
					name : "Created Date",
					width : 80,
					sortable : true
				},*/{
					id : "updDateTimeStr",
					field : "updDateTimeStr",
					name : "Updated Date",
					width : 80,
					sortable : true
				},{
					id : "updby",
					field : "updby",
					name : "Updated By",
					width : 50,
					sortable : true
				},{
					id : "actionColumn",
					field : "actionColumn",
					name : "Actions",
					width : 300,
					formatter:brandFormatter
				}];
		
		if (userContestBrandsColumnsStr != 'null' && userContestBrandsColumnsStr != '') {
			columnOrder = userContestBrandsColumnsStr.split(',');
			var columnWidth = [];
			for ( var i = 0; i < columnOrder.length; i++) {
				columnWidth = columnOrder[i].split(":");
				for ( var j = 0; j < allContestBrandsColumns.length; j++) {
					if (columnWidth[0] == allContestBrandsColumns[j].id) {
						userContestBrandsColumns[i] = allContestBrandsColumns[j];
						userContestBrandsColumns[i].width = (columnWidth[1] - 5);
						break;
					}
				}
		
			}
		} else {
			userContestBrandsColumns = allContestBrandsColumns;
		}
		
		
		function brandFormatter(row, cell, value, columnDef, dataContext) {
			return '<div class="myIcon">'+
			'<span onClick="downloadWinnerExcel('+dataContext.pdID+','+dataContext.contestId+');">Winners Excel</span>'+
			'<span onClick="downloadFinalistExcel('+dataContext.pdID+','+dataContext.contestId+');">Finalist Excel</span>'+
			'<span onClick="downloadPartExcel('+dataContext.pdID+','+dataContext.contestId+');">Participant Excel</span>'+
			'<img class="deleteSpo" style="max-width: 21px;" src="/funzata/resources/img/delete.png" id="'+ dataContext.pdID +'"/>'+
			'<img class ="editSponsor"style="max-width: 18px;" src="/funzata/resources/img/edit.png" id="'+ dataContext.pdID +'"/>'+
			'</div>';
		}
		
		$('body').on('click',".editSponsor", function(){
		    var me = $(this), id = me.attr('id');
		    editSponsor(id);
		});
		$('body').on('click',".deleteSpo", function(){
		    var me = $(this), id = me.attr('id');
		    deleteSponsor(id);
		});
		
		function editSponsor(pdID){
			if (pdID == null || pdID == undefined || pdID == '') {
				return false;
			}else {
				getEditSponsor(pdID);
			}
		}
		
		function deleteSponsor(pdID){
			if (pdID == null || pdID == undefined || pdID == '') {
				return false;
			}else {
				getDeleteSponsor(pdID);
			}
		}
		
		function downloadWinnerExcel(brandId,coId){
			//var contId = $('#contestIdStr').val();
			var appendData = "contestId="+coId+"&brandId="+brandId;
		    var url = "${pageContext.request.contextPath}/GetWinnersExcel?"+appendData;
		    $('#download-frame').attr('src', url);
		}
		
		function downloadFinalistExcel(brandId,coId){
			//var contId = $('#contestIdStr').val();
			var appendData = "contestId="+coId+"&brandId="+brandId;
		    var url = "${pageContext.request.contextPath}/GetfinalistExcel?"+appendData;
		    $('#download-frame').attr('src', url);
		}
		
		function downloadPartExcel(brandId,coId){
			var appendData = "contestId="+coId+"&brandId="+brandId;
		    var url ="${pageContext.request.contextPath}/GetContestParticipantExcel?"+appendData;
		    $('#download-frame').attr('src', url);
		}
		
		
		var contestBrandsOptions = {
			editable: true,
			enableCellNavigation : true,
			asyncEditorLoading: false,
			forceFitColumns : true,
			multiSelect: false,
			topPanelHeight : 25,
			showHeaderRow: true,
			headerRowHeight: 30,
			explicitInitialization: true
		};
		var contestBrandsGridSortcol = "brandId";
		var contestBrandsGridSortdir = 1;
		var percentCompleteThreshold = 0;
		
		function contestBrandsGridComparer(a, b) {
			var x = a[contestBrandsGridSortcol], y = b[contestBrandsGridSortcol];
			if (!isNaN(x)) {
				return (parseFloat(x) == parseFloat(y) ? 0
						: (parseFloat(x) > parseFloat(y) ? 1 : -1));
			}
			if (x == '' || x == null) {
				return 1;
			} else if (y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String)
					&& (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));
			}
		}
		
		function refreshContestBrandsGridValues(jsonData) {
			$("div#divLoading").addClass('show');
			contestBrandsData = [];
			if (jsonData != null && jsonData.length > 0) {
				for ( var i = 0; i < jsonData.length; i++) {
					var data = jsonData[i];
					var d = (contestBrandsData[i] = {});
					d["id"] = i;
					d["pdID"] = data.pdID;
					d["brandName"] = data.brandName;
					d["email"] = data.email;
					d["contestId"] = data.contestId;
					d["phone"] = data.phone;
					d["firstName"] = data.firstName;
					d["lastName"] = data.lastName;
					d["passCode"] = data.passCode;
					d["userLimitCnt"] = data.userLimitCnt;
					d["actUserCnt"] = data.actUserCnt;
					d["winnerPrize"] = data.winnerPrize;
					d["status"] = data.status;
					d["finelistPrize"] = data.finelistPrize;
					d["updBy"] = data.updBy;
					d["updDateTimeStr"] = data.updDateTimeStr;
					d["isWinnerEmailSent"] = "NO";
					if(data.isWinnerEmailSent == true || data.isWinnerEmailSent == 'true'){
						d["isWinnerEmailSent"] = "YES";
					}
				}
			}
		
			contestBrandsDataView = new Slick.Data.DataView();
			contestBrandsGrid = new Slick.Grid("#contestBrands_grid", contestBrandsDataView,
					userContestBrandsColumns, contestBrandsOptions);
			contestBrandsGrid.registerPlugin(new Slick.AutoTooltips({
				enableForHeaderCells : true
			}));
			contestBrandsGrid.setSelectionModel(new Slick.RowSelectionModel());
			//contestBrandsGrid.registerPlugin(contestBrandsCheckboxSelector);
			
			contestBrandsGridPager = new Slick.Controls.Pager(contestBrandsDataView,
						contestBrandsGrid, $("#contestBrands_pager"),
						contestBrandsPagingInfo);
			var contestBrandsGridColumnpicker = new Slick.Controls.ColumnPicker(
					allContestBrandsColumns, contestBrandsGrid, contestBrandsOptions);
			
			/* contestBrandsGrid.onContextMenu.subscribe(function (e) {
				  e.preventDefault();
				  var cell = contestBrandsGrid.getCellFromEvent(e);
				  contestBrandsGrid.setSelectedRows([cell.row]);
					var height = screen.height - e.pageY;
					var width = screen.width - e.pageX;
					if(height < $("#contextMenu1").height()){
						height = e.pageY - $("#contextMenu1").height();
					}else{
						height = e.pageY
					}
					if(width < $("#contextMenu1").width()){
						width =  e.pageX- $("#contextMenu1").width();
					}else{
						width =  e.pageX;
					}
				 $("#contextMenu1")
					  .data("row", cell.row)
					  .css("top", height)
					  .css("left", width)
					  .show();
				  $("body").one("click", function () {
					$("#contextMenu1").hide();
				  });
				}); */
			
			/* $("#contextMenu1").click(function (e) {
				if (!$(e.target).is("li")) {
				  return;
				}
			}); */
				
			contestBrandsGrid.onSort.subscribe(function(e, args) {
				contestBrandsGridSortdir = args.sortAsc ? 1 : -1;
				contestBrandsGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					contestBrandsDataView.fastSort(contestBrandsGridSortcol, args.sortAsc);
				} else {
					contestBrandsDataView.sort(contestBrandsGridComparer, args.sortAsc);
				}
			});
			
			// wire up model discountCodes to drive the contestBrandsGrid
			contestBrandsDataView.onRowCountChanged.subscribe(function(e, args) {
				contestBrandsGrid.updateRowCount();
				contestBrandsGrid.render();
			});
			contestBrandsDataView.onRowsChanged.subscribe(function(e, args) {
				contestBrandsGrid.invalidateRows(args.rows);
				contestBrandsGrid.render();
			});
			$(contestBrandsGrid.getHeaderRow())
					.delegate(
							":input",
							"keyup",
							function(e) {
								var keyCode = (e.keyCode ? e.keyCode : e.which);
								contestBrandsSearchString = '';
								var columnId = $(this).data("columnId");
								if (columnId != null) {
									contestBrandsColumnFilters[columnId] = $.trim($(this)
											.val());
									if (keyCode == 13) {
										for ( var columnId in contestBrandsColumnFilters) {
											if (columnId !== undefined
													&& contestBrandsColumnFilters[columnId] !== "") {
												contestBrandsSearchString += columnId
														+ ":"
														+ contestBrandsColumnFilters[columnId]
														+ ",";
											}
										}
										var contestId = $('#brandsCoId').val();
										getContestBrands(contestId);
									}
								}
		
							});
			contestBrandsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if (args.column.id.indexOf('checkbox') == -1) {				
					$("<input type='text'>").data("columnId", args.column.id)
							.val(contestBrandsColumnFilters[args.column.id]).appendTo(
									args.node);				
				}
			});
			contestBrandsGrid.init();
			// initialize the model after all the discountCodes have been hooked up
			contestBrandsDataView.beginUpdate();
			contestBrandsDataView.setItems(contestBrandsData);
			//contestBrandsDataView.setFilter(filter);
			contestBrandsDataView.endUpdate();
			contestBrandsDataView.syncGridSelection(contestBrandsGrid, true);
			//contestBrandsGrid.resizeCanvas();
			$("#gridContainer").resizable();
			$("div#divLoading").removeClass('show');
		}
	
	
		
		
		function getDeleteSponsor(pdID){
			var coId = $('#s_contest_id').val();
			if(coId == null || coId == undefined || coId == ''){
				return;
			}
			
			jConfirm("Are you sure to delete Sponsor?","Confirm",function(r){
				if (r) {
					$.ajax({
						url : "${pageContext.request.contextPath}/UpdateSponsor",
						type : "post",
						dataType: "json",
						data: "pdId="+pdID+"&sContestId="+coId+"&action=DELETE",
						success : function(response){				
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.sts == 1){
								contestBrandsPagingInfo = jsonData.packagePagingInfo;
								refreshContestBrandsGridValues(jsonData.packageList);
								clearAllSelections();
							}
							jAlert(jsonData.msg);
							return;
						},
						error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				} else {
					return false;
				}
			});
		}
		
		function getEditSponsor(pdID){
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateSponsor",
				type : "post",
				dataType: "json",
				data: "pdId="+pdID+"&action=EDIT",
				success : function(response){				
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.sts == 1){
						//$('#contestCategoryDiv').empty();					
						$('#sponsorModal').modal('show');
						setEditSponsor(jsonData.sponsor);
					}else{
						jAlert(jsonData.msg);
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		
		function setEditSponsor(sponsor){
			$('#sSaveBtn').hide();			
			$('#sUpdateBtn').show();
			if(sponsor!= null){
				$('#pdId').val(sponsor.pdID);
				$('#brandName').val(sponsor.brandName);
				$('#sponsorId').val(sponsor.passCode);
				$('#userLimit').val(sponsor.userLimitCnt);
				$('#finalistPrize').val(sponsor.finelistPrize);
				$('#winPrize').val(sponsor.winnerPrize);
				$('#spfName').val(sponsor.firstName);
				$('#splName').val(sponsor.lastName);
				$('#spEmail').val(sponsor.email);
				$('#spPhone').val(sponsor.phone);
				$('#spAddr').val(sponsor.addr);
				
			}
		}
	
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${contestsPagingInfo}));
		refreshContestsGridValues(JSON.parse(JSON.stringify(${contestsList})));
	};
		
</script>