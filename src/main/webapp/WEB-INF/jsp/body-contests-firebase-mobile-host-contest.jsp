<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-firestore.js"></script>

<script>
//Production
var firebaseConfig = {
    apiKey: "AIzaSyDhY64DxeKf8GO7uEhrorvS2bc_eI-Q4Hk",
    authDomain: "secondarysandbox-72ec0.firebaseapp.com",
    databaseURL: "https://secondarysandbox-72ec0.firebaseio.com",
    projectId: "secondarysandbox-72ec0",
    storageBucket: "secondarysandbox-72ec0.appspot.com",
    messagingSenderId: "826942470671",
    appId: "1:826942470671:web:6f3af3267ebefcd86ec17a",
    measurementId: "G-E7VG37VTE1"
  };
	  
	  
//sandbox
/* var firebaseConfig = {
    apiKey: "AIzaSyAE9cWnwc9_JxShdbgMfvgMjnXoUxPEGBk",
    authDomain: "lastrowcats-bd01e.firebaseapp.com",
    databaseURL: "https://lastrowcats-bd01e.firebaseio.com",
    projectId: "lastrowcats-bd01e",
    storageBucket: "lastrowcats-bd01e.appspot.com",
    messagingSenderId: "238014501294",
    appId: "1:238014501294:web:9bd34fa14fc0628efcb83f"
  };  */

//Initialize Firebase
firebase.initializeApp(firebaseConfig);
var db = firebase.firestore();
var hostColl = db.collection("hostNode");
var tracker = hostColl.doc("tracker");
var que = hostColl.doc("que");


tracker.onSnapshot(function(doc){
	var data = doc.data();
	$('#nextQuestion1').text('');
	setFirestoreDate(data.state);
});

var ANS = '';
var QSIZE = parseInt('${contest.questionSize}');
var queNo = '';


que.get().then(function(doc){
	var data = doc.data();
	$('#contestMsg').hide();
	$('#optionDiv').hide();
	
	queNo = data.qNo;
	ANS = data.ans;
});



function setFirestoreDate(state){
	$('#runningQuestionDiv').show();
	if(state == 'QUESTION'){
		que.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').hide();
			queNo = data.qNo;
			ANS = data.ans;
			$('#nextQuestion1').text('');
			if(queNo == 1 || queNo == '1'){
				$('#nextQuestion1').text('CONTEST STARTED READY FOR Q1');
			}else if(queNo == null || queNo == '' || queNo == 'null'){
				$('#nextQuestion1').text('CONTEST ENDED');
			}else{
				$('#nextQuestion1').text('ANSWER DISPLAYED');
				nextQuestionCounter("Q"+queNo+" IN",queNo);
			}
		});
	}else if(state == 'QUEFIRED'){
		$('#contestMsg').hide();
		$('#optionDiv').hide();
		$('#nextQuestion1').text('Q'+queNo+" DISPLAYED");
	}else if(state == 'COUNT'){
		$('#contestMsg').hide();
		$('#optionDiv').show();
		$('#optionDiv').css('background','#00a0df');
		
		if(ANS == 'A'){
			$('#correctOption').text('A');
			//$('#correctOptionCount').text(data.countA);
		}else if(ANS == 'B'){
			$('#correctOption').text('B');
			//$('#correctOptionCount').text(data.countB);
		}else if(ANS == 'C'){
			$('#correctOption').text('C');
			//$('#correctOptionCount').text(data.countC);
		}
		if(queNo == null || queNo == '' || queNo == 'null'){
			$('#nextQuestion1').text('CONTEST ENDED');
		}else{
			$('#nextQuestion1').text('READY FOR A'+queNo);
		}
	}else if(state == 'SUMMARY'){
		$('#contestMsg').hide();
		$('#optionDiv').hide();
		$('#nextQuestion1').text('DISPLAYED SUMMARY WINNERS');
	}else if(state == 'LOTTERY'){
		$('#contestMsg').hide();
		$('#optionDiv').hide();
		$('#nextQuestion1').text('RUNNING LOTTERY');
	}else if(state == 'WINNER'){
		$('#contestMsg').hide();
		$('#optionDiv').hide();
		$('#nextQuestion1').text('READY FOR GRAND WINNERS');
	}else if(state == 'WINFIRED'){
		$('#contestMsg').hide();
		$('#optionDiv').hide();
		$('#nextQuestion1').text('DISPLAYED GRAND WINNER');
	}else if(state == 'END'){
		$('#contestMsg').hide();
		$('#winnerRewardLbl').text('');
		$('#optionDiv').hide();
		$('#totalUserCountlbl').text('0');
		$('#nextQuestion').text('');
		$('#nextQuestion1').text('CONTEST ENDED');
	}else{
		$('#optionDiv').hide();
	}
	
}



	  
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}		
	});
	
});


function nextQuestionCounter(buttonText,queNo){
	var c = 12;
	$('#nextQuestion').text(buttonText+' - '+c);
	setInterval(function(){
	  c--;
	  if(c>=0){
		  $('#nextQuestion').text(buttonText+' - '+c);
	  }
      if(c==0){
    	  $('#nextQuestion1').text('READY FOR Q'+queNo);
		  $('#nextQuestion').text('');
		  
      }
	},1000);
}


function renderWinners(winner){
	var winnerDiv = '<table cellspacing=6 cellpadding=2>';
	$('#winnerDiv').empty();
	if(winner.length> 0){
		$('#winnerRewardLbl').text(winner[0].tix+' Tickets');
	}
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<tr><td style="margin:15px;text-align:center;"><label style="font-size: 15px;">'+winner[i].uId+'</td></tr>';
	}
	winnerDiv +='</table>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv); 
}

function fillLiveUserList(data){
	if(data.userList!=null && data.userList != '' && data.userList.length > 0){
		var userString = '';
		$('#liveUsersDiv').empty();
		for(var i=0;i<data.userList.length;i++){
			userString += '<div class="form-group col-sm-2 col-xs-2">'+
			'<label id="totalUserCountlbl" style="font-size: 15px;">'+data.userList[i].uId+'</label></div>';
		}
		$('#liveUsersDiv').append(userString);
	}
}

function renderSummaryWinners(winner){
	var winnerDiv = '<table cellspacing=6 cellpadding=2>';
	$('#winnerDiv').empty();
	if(winner.length> 0){
		$('#winnerRewardLbl').text('$'+winner[0].prize);
	}
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<tr><td style="margin:15px;text-align:center;"><label style="font-size: 15px;">'+winner[i].uId+'</td></tr>';
	}
	winnerDiv +='</table>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv); 
	
	/* winnerDiv = '<marquee scrollamount="15"><table><tr="row1"></tr><tr="row2"></tr><tr="row3"></tr></table></marquee>';
	$('#winnerDiv').append(winnerDiv);
	if(winner.length> 0){
		$('#winnerRewardLbl').text('$'+winner[0].prize);
	}
	for(var i=0;i<winner.length;i=i+3){
		var row1  = document.getElementById('row1');
		var cell = row1.insertCell(-1);
		cell.innerHTML = winner[i].uId;
		if((i+1) < winner.length){
			var row2  = document.getElementById('row2');
			var cell = row2.insertCell(-1);
			cell.innerHTML = winner[i+1].uId;
		}
		if((i+2) < winner.length){
			var row3  = document.getElementById('row3');
			var cell = row3.insertCell(-1);
			cell.innerHTML = winner[i+2].uId;
		}
	} */
	
	
}

function renderMegaJackpotWinners(winner){
	var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
	$('#winnerDiv').empty();
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
		'<label style="font-size: 30px;">'+winner[i].rPointsSt+'</label><br/><label style="font-size: 30px;">'+winner[i].jCreditSt+'</label>'+
		'<br/><label style="font-size: 12px;">'+winner[i].uId+'</label></div>';
			
	}
	winnerDiv +='</div></marquee>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv);
	
}

function renderMiniJackpotWinners(winner,type,prize){
	if(winner.length <=3){
		var winnerDiv = '<div align="center" id="grandWinnerDiv"><table align="center"> <tr>';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(type+' - '+prize);
		}
		for(var i=0;i<winner.length;i++){
			winnerDiv +=  '<td align="center">'+
				'<label style="font-size: 12px;margin:15px;text-align:center;width:350px;">'+winner[i].uId+'</label></td>';
		}
		winnerDiv +='</tr></table></div>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}else{
		var winnerDiv = '<marquee scrollamount="15"><div id="winnerDivStyle">';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(type+' - '+prize);
		}
		for(var i=0;i<winner.length;i++){
				winnerDiv += '<div style="margin:15px;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
			'<label style="font-size: 15px;">'+winner[i].uId+'</label>'+
			'</div>';
				
		}
		winnerDiv +='</div></marquee>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}
	
}


</script>

<div id="contestDiv">	
	<div class="panel-body1 full-width" >
			<div class="form-group tab-fields">
				<!-- <div class="form-group col-sm-3 col-xs-3">
					<label style="font-size: 15px;">Users :&nbsp;&nbsp;&nbsp;</label>
					<label id="totalUserCountlbl" style="font-size: 15px;margin-left:-12px;">0</label>
				</div> -->
				 <div class="form-group col-sm-12 col-xs-12" id="lifelineCountDiv" style="margin-left:150px;">
					<label id="nextQuestion1" style="font-size: 15px;;color:red;">Contest Not Started</label><br/>
					<label id="nextQuestion" style="font-size: 15px;;color:red;"></label>
					<!-- <button class="btn btn-primary" id="nextQuestion" type="button" disabled>Contest Not Started</button> -->
				</div>
				<div id="optionDiv" class="form-group col-sm-2 col-xs-2" style="margin-left:125px;">
					<label id="correctOption" style="font-size: 15px;"></label>
				</div>
			</div>
			<div class="form-group tab-fields">
				
			</div>
			<div align="left" id="winnerRewardDollarDiv">
				<label id= "winnerRewardLbl" style="font-size: 15px;"></label>
				<br/><br/>
			</div>
			<div id="winnerDiv" style="display:none;float:left">
				
			</div>
		</form>
		<div id="contestMsg" class="form-group tab-fields" style="text-align: center;">
				<div class="form-group col-sm-12 col-xs-12">
				<c:if test="${not empty msg}">
					<label style="font-size: 25px;;color:red;">${msg}</label>	
				</c:if>
				</div>
		</div>
	</div>
</div>