<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<script>
var QSIZE = '${contest.questionSize}';
var questionArray = [];
var j = 0;
var varSelectedList = '';
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}		
	});
	
});


function contDownTimer(buttonText,qNo){
	var c = 12;
	$('#nextQuestion').text(c);
	$('#nextQuestion').attr("disabled",true);
	setInterval(function(){
	  c--;
	  if(c>=0){
		  $('#nextQuestion').text(c);
	  }
      if(c==0){
    	  $('#nextQuestion').attr("disabled",false);
    	  $('#nextQuestion').text(buttonText);
      }
	},1000);
}

function contDownTimer1(buttonText,qNo){
	var c = 12;
	$('#nextQuestion').text(c);
	$('#nextQuestion').attr("disabled",true);
	setInterval(function(){
	  c--;
	  if(c>=0){
		  $('#nextQuestion').text(c);
	  }
      if(c==0){
    	  $('#nextQuestion').attr("disabled",false);
    	  $('#nextQuestion').text(buttonText);
      }
	},1000);
}

var isTotalCount= true;
function nextQuestion(){
	var buttonType = $('#nextQuestion').text();
	var type = '';
	
	var userCountUpdater;
	if(buttonType!='' && buttonType != undefined){
		if(buttonType.indexOf('Resume') >= 0){
			type='RESUME';
		}else if(buttonType.indexOf('Question') >= 0 || buttonType.indexOf('Summary') >= 0
				|| buttonType.indexOf('End') >= 0){
			type='QUESTION';
			if(buttonType.indexOf('Summary') >= 0){
				type = 'SUMMARY';
			}
		}else if(buttonType.indexOf('Answer') >= 0){
			type='ANSWER';
		}else if(buttonType.indexOf('Start') >= 0){
			type='START';
		}else if(buttonType.indexOf('Lottery') >= 0){
			type = 'LOTTERY';
		}else if(buttonType.indexOf('Declare Winners') >= 0){
			type = 'WINNERS';
		}else if(buttonType.indexOf('Display Winners') >= 0){
			type = 'WINNER';
		}else if(buttonType.indexOf('Count') >= 0){
			type = 'COUNT';
		}
	}
	$.ajax({
		url : "${pageContext.request.contextPath}/NextContestQuestionFireBase",
		type : "post",
		dataType: "json",
		data: "runningContestId="+$('#runningContestId').val()+"&runningQuestionNo="+$('#runningQuestionNo').val()+"&type="+type,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				var data = jsonData.question;
				$('#runningContestId').val(data.contestId);
				$('#runningQuestionNo').val(data.serialNo);
				$('#runningQuestionText').text(data.serialNo+".  "+data.question);
				$('#runningQuestionOptionA').text(data.optionA);
				$('#runningQuestionOptionB').text(data.optionB);
				$('#runningQuestionOptionC').text(data.optionC);
				/* $('#questionOptionACount').text('');
				$('#questionOptionBCount').text('');
				$('#questionOptionCCount').text(''); */
				$('#optionADiv').css('background','#ccc');
				$('#optionBDiv').css('background','#ccc');
				$('#optionCDiv').css('background','#ccc');
				/* if(isTotalCount == true){
					userCountUpdater = setInterval(function(){updateTotalUserCount()},5000);
				} 
				isTotalCount = false;*/
				$('#contestInfoDiv1').hide();
				$('#contestInfoDiv2').hide();
				$('#questionSizeDiv').hide();
				if(type=='START'){
					var qNo = parseInt(data.serialNo);
					$('#runningQuestionText').text(jsonData.contestMsg);
					$('#nextQuestion').text('Show Question-'+(qNo+1));
					$('#runningQuestionNo').val(0);
				}else if(type=='QUESTION'  || (type=='RESUME' && jsonData.contest.lastAction == 'QUESTION')){
					$('#optionDiv').show();
					$('#winnerRewardLbl').text('');
					$('#winnerDiv').empty();
					$('#runningQuestionDiv').addClass('col-xs-6');
					$('#runningQuestionDiv').addClass('col-sm-6');
					$('#runningQuestionDiv').removeClass('col-xs-12');
					$('#runningQuestionDiv').removeClass('col-sm-12');
					$('#optionADiv').show();
					$('#optionBDiv').show();
					$('#optionCDiv').show();
					contDownTimer('Show Count-'+data.serialNo,data.serialNo);
					$('#runningQuestionAnswer').text('');
					$('#nextADiv').hide();
					$('#nextBDiv').hide();
					$('#nextCDiv').hide();
					$('#nextQuestionText').text('');
					$('#liveUsersDiv').empty();
				}else if(type=='COUNT' || (type=='RESUME' && jsonData.contest.lastAction == 'COUNT')){
					$('#nextQuestion').text('Show Answer-'+data.serialNo);
					$('#optionDiv').show();
					if(data.answer == 'A'){
						$('#optionADiv').css('background','#00a0df');
					}else if(data.answer == 'B'){
						$('#optionBDiv').css('background','#00a0df');
					}else if(data.answer == 'C'){
						$('#optionCDiv').css('background','#00a0df');
					}
					
					$('#nextADiv').hide();
					$('#nextBDiv').hide();
					$('#nextCDiv').hide();
					$('#nextQuestionText').text('');
					/* $('#questionRewardDollar').text("Reward $"+jsonData.userCount.quRwds);
					$('#questionOptionACount').text(jsonData.userCount.aCount);
					$('#questionOptionBCount').text(jsonData.userCount.bCount);
					$('#questionOptionCCount').text(jsonData.userCount.cCount);
					fillLiveUserList(jsonData); */
				}else if(type=='ANSWER' || (type=='RESUME' && jsonData.contest.lastAction == 'ANSWER')){
					$('#optionDiv').show();
					$('#nextADiv').show();
					$('#nextBDiv').show();
					$('#nextCDiv').show();
					var qNo = parseInt(data.serialNo);
					
					$('#nextQuestion').text('Show Question-'+(qNo+1));
					if(data.answer == 'A'){
						$('#optionADiv').css('background','#00a0df');
					}else if(data.answer == 'B'){
						$('#optionBDiv').css('background','#00a0df');
					}else if(data.answer == 'C'){
						$('#optionCDiv').css('background','#00a0df');
					}
					/* $('#questionRewardDollar').text("Reward $"+jsonData.userCount.quRwds);
					$('#questionOptionACount').text(jsonData.userCount.aCount);
					$('#questionOptionBCount').text(jsonData.userCount.bCount);
					$('#questionOptionCCount').text(jsonData.userCount.cCount); */
					if(qNo == QSIZE){
						$('#nextQuestion').text('Show Summary');
						$('#nextADiv').hide();
						$('#nextBDiv').hide();
						$('#nextCDiv').hide();
						$('#nextQuestionText').text('');
					}else{
						contDownTimer1('Show Question-'+(qNo+1),qNo);
						$('#nextADiv').css('background','#ccc');
						$('#nextBDiv').css('background','#ccc');
						$('#nextCDiv').css('background','#ccc');
						$('#nextQuestionText').text(jsonData.nextQuestion.serialNo+".  "+jsonData.nextQuestion.question);
						$('#nextQuestionOptionA').text(jsonData.nextQuestion.optionA);
						$('#nextQuestionOptionB').text(jsonData.nextQuestion.optionB);
						$('#nextQuestionOptionC').text(jsonData.nextQuestion.optionC);
						if(jsonData.nextQuestion.answer == 'A'){
							$('#nextADiv').css('background','#00a0df');
						}else if(jsonData.nextQuestion.answer == 'B'){
							$('#nextBDiv').css('background','#00a0df');
						}else if(jsonData.nextQuestion.answer == 'C'){
							$('#nextCDiv').css('background','#00a0df');
						}
					}
				}else if(type=='SUMMARY' || (type=='RESUME' && jsonData.contest.lastAction == 'SUMMARY')){
					$('#runningQuestionDiv').removeClass('col-xs-6');
					$('#runningQuestionDiv').removeClass('col-sm-6');
					$('#runningQuestionDiv').addClass('col-xs-12');
					$('#runningQuestionDiv').addClass('col-sm-12');
					$('#runningQuestionText').text('Summary Displayed, Press the button below to run lottery.');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					contDownTimer('Run Lottery',data.serialNo);
					//$('#nextQuestion').text('Enter to Lottery');
					$('#liveUsersDiv').empty();
				}else if(type=='LOTTERY' || (type=='RESUME' && jsonData.contest.lastAction == 'LOTTERY')){
					$('#runningQuestionDiv').removeClass('col-xs-6');
					$('#runningQuestionDiv').removeClass('col-sm-6');
					$('#runningQuestionDiv').addClass('col-xs-12');
					$('#runningQuestionDiv').addClass('col-sm-12');
					$('#runningQuestionText').text('Lottery is Running, Press the button below to randomnly choose final Winners.');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					contDownTimer('Declare Winners',data.serialNo);
					//$('#nextQuestion').text('Declare Winners');
					$('#liveUsersDiv').empty();
				}else if(type=='WINNER' || (type=='RESUME' && jsonData.contest.lastAction == 'WINNER')){
					$('#runningQuestionText').text('Press End button Contest to End contest.');
					$('#nextQuestion').text('End Contest');
					$('#liveUsersDiv').empty();
				}else if(type=='WINNERS' || (type=='RESUME' && jsonData.contest.lastAction == 'WINNERS')){
					$('#runningQuestionText').text('Contest winner are displayed.');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					$('#nextQuestion').text('Display Winners');
				}
			}else{
				jAlert(jsonData.msg);
				return;
			}
			if((jsonData.msg != '' && jsonData.msg != null) || (type=='RESUME' && jsonData.contest.lastAction == 'END')){
				 if(buttonType.indexOf('End') >= 0){
					    $('#nextQuestion').hide();
						$('#runningQuestionText').text('');
						$('#optionADiv').hide();
						$('#optionBDiv').hide();
						$('#optionCDiv').hide();
						jAlert(jsonData.msg);
						window.location="${pageContext.request.contextPath}/Contest?status=EXPIRED";
				 }else{
					 jAlert(jsonData.msg);
				 }
			}
			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function renderWinners(winner,count){
	if(winner.length <=3){
		var winnerDiv = '<div align="center" id="grandWinnerDiv"><table align="center"> <tr>';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(winner[0].rTix+' Tickets');
		}
		for(var i=0;i<winner.length;i++){
			winnerDiv +=  '<td align="center">'+
				'<label style="font-size: 60px;margin:15px;border:1px solid;text-align:center;width:350px;">'+winner[i].uId+'</label></td>';
		}
		winnerDiv +='</tr></table></div>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}else{
		var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(winner[0].rTix+' Tickets');
		}
		for(var i=0;i<winner.length;i++){
				winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
			'<label style="font-size: 55px;">'+winner[i].uId+'</label>'+
			'</div>';
				
		}
		winnerDiv +='</div></marquee>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}
}


function renderSummaryWinners(winner){
	var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
	$('#winnerDiv').empty();
	if(winner.length> 0){
		$('#winnerRewardLbl').text('$'+winner[0].rPointsSt);
	}
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
		'<label style="font-size: 55px;">'+winner[i].uId+'</label>'+
		'</div>';
			
	}
	winnerDiv +='</div></marquee>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv);
}
function resetContest(action){
	var confirmText="";
	if(action == 'RESET'){
		confirmText = "Are you sure to reset Contest data ?";
	}else if(action == 'END'){
		confirmText = "Are you sure to end contest ?";
	}else if(action == 'ACTIVE'){
		confirmText = "Are you sure to move contest to active?";
	}
	
	jConfirm(confirmText,"Confirm",function(r){
		if (r) {
			$.ajax({
					url : "${pageContext.request.contextPath}/ResetContest",
					type : "post",
					dataType: "json",
					data : "contestId="+$('#runningContestId').val()+"&action="+action,
					success : function(response) {
						var jsonData = JSON.parse(JSON.stringify(response));
						jAlert(jsonData.msg);
						if(action == 'END' && jsonData.sts==1){
							window.location="${pageContext.request.contextPath}/Contest?status=EXPIRED";
						}
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
					});
		} else {
			return false;
		}
	});
	return false;
}

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Today's Contest</li>
			<li style="float:right;margin-top: 50px;" ><button type="button" class="btn btn-primary" onclick="resetContest('END');">End Contest</button></li>
		</ol>
		
	</div>
</div>
<div id="contestDiv">	
	<div class="panel-body1 full-width">
		
		<form name="runningContestForm" id="runningContestForm" method="post">
			<input type="hidden" id="runningContestId" name="runningContestId" value="${contest.id}" />
			<input type="hidden" id="runningQuestionNo" name="runningQuestionNo"  value="${lastQuestionNo}"/>
			<div class="form-group tab-fields">
				<div class="form-group col-sm-4 col-xs-4" id="contestInfoDiv1">
					<label style="font-size: 25px;">${contest.contestName}</label><br/>
					<label style="font-size: 20px;">${contest.startDateTimeStr}</label><br/>
				</div>
				<div class="form-group col-sm-4 col-xs-4" id="contestInfoDiv2">
					<label style="font-size: 20px;">Winner Prize : ${contest.prizeText}</label><br/>
				</div>
				<div class="form-group col-sm-4 col-xs-4" id="questionSizeDiv">
					<label style="font-size: 20px;">Question Size : ${contest.questionSize}</label>
				</div>
				<!-- <div class="form-group col-sm-4 col-xs-4">
					<label style="font-size: 20px;">Total User Count :&nbsp;&nbsp;&nbsp;</label>
					<label id="totalUserCountlbl" style="font-size: 20px;">0</label>
				</div> 
				<div class="form-group col-sm-4 col-xs-4" id="lifelineCountDiv">
					<label style="font-size: 20px;">Last Q Used Lifeline Count :&nbsp;&nbsp;&nbsp;</label>
					<label id="lifelineCountLbl" style="font-size: 20px;"></label>
				</div>
				<div id="liveUsersDiv"></div>
				
				</div>-->
				<%-- <div class="form-group col-sm-8 col-xs-8">
					<c:if test="${empty msg}">
						<label style="font-size: 15px;">Note : Once contest is started please do not refresh or leave page or contest data will be lost.</label>
					</c:if>
				</div> --%>
			</div>
			<div class="form-group tab-fields">
			<hr style="width: 100%; color: black; height: 1px; background-color:black;" />
				<div id="runningQuestionDiv" class="form-group col-sm-6 col-xs-6">
					<label id="runningQuestionText" style="font-size: 35px;"></label>
					
				</div>
				<div class="form-group col-sm-6 col-xs-6">
					<label id="nextQuestionText" style="font-size: 35px;"></label>
				</div>
			</div>
			<br/><br/>
			<div align="center" id="winnerRewardDollarDiv">
				<label id= "winnerRewardLbl" style="font-size: 80px;"></label>
			</div>
			<div id="winnerDiv" class="form-group tab-fields" style="display:none">
				
			</div>
			<div class="form-group tab-fields" style="display:none;" id="optionDiv">
				<div id="optionADiv" class="form-group col-sm-6 col-xs-6" style="margin-left: 20px;">
					<label style="font-size: 20px;">A.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionA" style="font-size: 30px;"></label>
					<!-- <label id="questionOptionACount" style="font-size: 30px;float:right;"></label> -->
				</div>
				<div id="nextADiv" class="form-group col-sm-5 col-xs-5" style="display:none;margin-left: 20px;">
					<label style="font-size: 20px;">A.&nbsp;&nbsp;&nbsp;</label>
					<label id="nextQuestionOptionA" style="font-size: 30px;"></label>
				</div>
				<!-- <div class="form-group col-sm-4 col-xs-4" style="margin-left: 60px;">
					<label id="questionRewardDollar" style="font-size: 30px;"></label>
				</div> -->
				<div id="optionBDiv" class="form-group col-sm-6 col-xs-6" style="margin-left: 20px;">
					<label style="font-size: 20px;">B.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionB" style="font-size: 30px;"></label>
					<!-- <label id="questionOptionBCount" style="font-size: 30px;float:right;"></label> -->
				</div>
				<div id="nextBDiv" class="form-group col-sm-5 col-xs-5" style="display:none;margin-left: 20px;">
					<label style="font-size: 20px;">B.&nbsp;&nbsp;&nbsp;</label>
					<label id="nextQuestionOptionB" style="font-size: 30px;"></label>
				</div>
				<div id="optionCDiv" class="form-group col-sm-6 col-xs-6" style="margin-left: 20px;">
					<label style="font-size: 20px;">C.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionC" style="font-size: 30px;"></label>
					<!-- <label id="questionOptionCCount" style="font-size: 30px;float:right;"></label> -->
				</div>
				<div id="nextCDiv" class="form-group col-sm-65 col-xs-5" style="display:none;margin-left: 20px;">
					<label style="font-size: 20px;">C.&nbsp;&nbsp;&nbsp;</label>
					<label id="nextQuestionOptionC" style="font-size: 30px;"></label>
				</div>
				<!-- <div class="form-group col-sm-6 col-xs-6">
					<label>D.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionD"></label>
				</div> -->
			</div>
		</form>
		<div class="form-group tab-fields" style="text-align: center;">
				<div class="form-group col-sm-12 col-xs-12">
				<c:if test="${empty msg}">
					<button class="btn btn-primary" id="nextQuestion" type="button" onclick="nextQuestion()">${buttonText}</button>
				</c:if>
				<c:if test="${not empty msg}">
					<label style="font-size: 18px;;color:red;">${msg}</label>	
				</c:if>
				</div>
		</div>
	</div>
</div>