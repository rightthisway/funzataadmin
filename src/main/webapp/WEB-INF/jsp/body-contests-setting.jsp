<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<script>
$(document).ready(function() {
	
});

function sendNotification(){
	
	var title = $('#noTitle').val();
	var noText = $('#noText').val();
	
	if(title = null || title == ''){
		jAlert("Notification title is mendatory.");	
		return;
	}
	if(noText = null || noText == ''){
		jAlert("Notification text is mendatory.");	
		return;
	}
	
	
	$.ajax({
		url : "${pageContext.request.contextPath}/SendNotificationNow",
		type : "post",
		dataType: "json",
		data: $('#notificationForm').serialize(),
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#notificationManualModal').modal('hide');
			}
			jAlert(jsonData.msg);
			return;
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function openManualNitification(){
	$('#notificationManualModal').modal('show');
}

</script>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Contests Settings</li>
		</ol>
	</div>
</div>
<div class="row" style="font-size:14px !important;">
	<div class="col-lg-4">
         <section class="panel">
             <header class="panel-heading">
                <strong>Contest Notifications</strong>
             </header>
             <div class="list-group">
            	  <!--   <a class="list-group-item " href="javascript:getContestConfigSettings('NOTIFICATION20')">
                    Change 3 Minutes Notification Message
                 </a> -->
                <!--  <a class="list-group-item " href="javascript:getContestConfigSettings('NOTIFICATION7')">
                     Change 1 Hour Notification Message
                 </a> -->
                  <a class="list-group-item " href="javascript:openManualNitification();">
                     Send Notification
                 </a>
                  <!-- <a class="list-group-item " href="javascript:updateOneSignalNotification();">
                     Update One Signal Notification Device Ids
                 </a>
                 <a class="list-group-item " href="javascript:getContestConfigSettings('SHOWNEXTCONTEST')">
                     Display TBD OR Display Next Contest
                 </a> -->
                 <!--  <a class="list-group-item " href="javascript:updateChattingSettings('true')">
                     Set Chatting On
                 </a> -->
             </div>
         </section>
     </div>
     <!-- <div class="col-lg-4">
         <section class="panel">
             <header class="panel-heading">
                <strong>Contest Settings</strong> 
             </header>
             <div class="list-group">
            	   <a class="list-group-item " href="javascript:getContestConfigSettings('LIVEURL')">
                    Change Live Stream URL
                 </a>
                 <a class="list-group-item " href="javascript:getContestConfigSettings('LIVESTHRESHOLD')">
                    Change Earn Life Threshold
                 </a>
                  <a class="list-group-item " href="javascript:getReferralRewardSettings();">
                    Referral Reward Setting
                 </a>
             </div>
         </section>
     </div> -->
</div>

<!-- Notification Manual - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="notificationManualModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Notification</h4>
			</div>
			<div class="modal-body full-width">
			<form name="notificationForm" id="notificationForm" method="post">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-6 col-xs-6">
						<label>Notification Title<span class="required">*</span>
						</label> <input class="form-control" type="text" id="noTitle" name="noTitle">
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label>Notification Text<span class="required">*</span></label> 
						<input class="form-control" type="text" id="noText" name="noText">
					</div>
				</div>
				</form>
			</div>
			<div class="modal-footer full-width" style="margin-top:23%;">
				<button class="btn btn-primary" type="button" onclick="sendNotification()">Send Now</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  Notification Manual - Ends  -->