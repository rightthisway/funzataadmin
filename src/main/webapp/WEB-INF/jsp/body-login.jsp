<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Funzata-Login</title>
<meta name="description" content="Funzata Admin">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">

<meta name="viewport" content="width=device-width,initial-scale=1">

<!-- Icons -->
<link rel="shortcut icon" href="/funzata/resources/img/icon57.png">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="57x57">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="72x72">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="76x76">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="114x114">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="120x120">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="144x144">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="152x152">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<link rel="stylesheet" href="/funzata/resources/css/bootstrap.css">

<!-- Related styles of various javascript plugins -->
<link rel="stylesheet" href="/funzata/resources//funzata/resources/css/plugins.css">

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
<link rel="stylesheet" href="/funzata/resources/css/main.css">
<!-- END Stylesheets -->

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/funzata/resources/js/vendor/bootstrap.min.js"></script>
<script src="/funzata/resources/js/vendor/modernizr-respond.min.js"></script>
<script src="/funzata/resources/js/plugins.js"></script>
<script src="/funzata/resources/js/main.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	});

	function loginValidations() {
		if ($("#username").val() == '') {
			jAlert("Username can't be blank");
			return false;
		} else if ($("#password").val() == '') {
			jAlert("Password can't be blank");
			return false;
		}
	}
</script>
<title>Funzata - Login</title>
</head>

<body class="login">
	<div id="login-container">
		<div id="login-logo">
			<a href=""> <img src="/funzata/resources/img/template/logo1.png" alt="logo" height="125px" width="352px">
			</a>
		</div>
		<!-- Login Form -->
		<form id="login-form" action="Login" method="post"
			onsubmit="return loginValidations()" class="form-horizontal" style="display:block">
			<div class="form-group">
				<c:if test="${error == true }">
					<label
						style="color: red; display: block; text-align: center; font-size: 12px;">Username
						or Password is incorrect</label>
				</c:if>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<div class="input-group">
						<input type="text" id="username" name="username"
							placeholder="username" class="form-control"> <span
							class="input-group-addon"><i
							class="fa fa-envelope-o fa-fw"></i></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<div class="input-group">
						<input type="password" id="password" name="password"
							placeholder="Password" class="form-control"> <span
							class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
					</div>
				</div>
			</div>
			<div class="clearfix">
				<div class="btn-group btn-group-sm pull-right">
					<button type="submit" class="btn btn-success">
						<i class="fa fa-arrow-right"></i> Login
					</button>
				</div>
				<label id="topt-fixed-header-top"
					class="switch switch-success pull-left" data-toggle="tooltip"
					title="Remember me"><input type="checkbox"><span></span></label>
			</div>
		</form>
		<!-- END Login Form -->
	</div>
	<!-- END Login Container -->

</body>
</html>
