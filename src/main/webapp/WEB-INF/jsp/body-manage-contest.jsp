<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<div id="page-content">
	<!-- Navigation info -->
	<ul id="nav-info" class="clearfix">
		<li><a href="${pageContext.request.contextPath}/Dashboard"><i class="fa fa-home"></i></a></li>
		<li><a href="javascript:void(0)">Tables</a></li>
		<li class="active"><a href="">DataTables</a></li>
	</ul>
	<!-- END Navigation info -->
	
	<!-- Datatables -->
    <h3 class="page-header page-header-top">Manage Contest<small>(Mange all active/Expired contests)</small></h3>
	
	<table id="example-datatables" class="table table-striped table-bordered table-hover">
         <thead>
             <tr>
                 <th class="cell-small"></th>
                 <th><i class="fa fa-user"></i> Contest Name</th>
                 <th><i class="fa fa-user"></i> Contest Date</th>
                 <th><i class="fa fa-user"></i> No. Of Questions</th>
                 <th><i class="fa fa-user"></i> No. Of Winners</th>
                 <th><i class="fa fa-user"></i> Updated By</th>
                 <th><i class="fa fa-user"></i> Updated Date</th>
                 <th><i class="fa fa-bolt"></i> Status</th>
             </tr>
         </thead>
         <tbody>
         <c:forEach var="data" items="${contestsList}">
           	 <tr>
                 <td class="text-center">
                     <div class="btn-group">
                         <a href="javascript:void(0)" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                         <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                     </div>
                 </td>
                 <td><c:out value="${data.contestName}"/></a></td>
                 <td><c:out value="${data.startDateTimeStr}"/></a></td>
                 <td class="hidden-xs hidden-sm hidden-md"><c:out value="${data.questionSize}"/></td>
                 <td class="hidden-xs hidden-sm hidden-md"><c:out value="${data.maxWinners}"/></td>
                 <td class="hidden-xs hidden-sm hidden-md"><c:out value="${data.updatedBy}"/></td>
                 <td class="hidden-xs hidden-sm hidden-md"><c:out value="${data.updatedDateTimeStr}"/></td>
                 <c:if test="${data.status eq 'ACTIVE'}"><td><span class="label label-active"><c:out value="${data.status}"/></span></td></c:if>
                 <c:if test="${data.status eq 'STARTED'}"><td><span class="label label-success"><c:out value="${data.status}"/></span></td></c:if>
             </tr>
         </c:forEach>
         </tbody>
     </table>
     <!-- END Datatables -->
	
</div>