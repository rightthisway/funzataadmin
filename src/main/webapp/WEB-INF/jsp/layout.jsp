<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@ page import="com.rtfbar.admin.data.TrackerUser"%>
<%@ page import="com.rtfbar.admin.data.Role"%>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1"> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, Dashboard, Events, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.ico">

<title><tiles:insertAttribute name="title"></tiles:insertAttribute></title>
<tiles:importAttribute name="menu" scope="request" />
<tiles:importAttribute name="subMenu" scope="request" />


<link rel="shortcut icon" href="/funzata/resources/img/icon57.png">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="57x57">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="72x72">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="76x76">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="114x114">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="120x120">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="144x144">
<link rel="apple-touch-icon" href="/funzata/resources/img/icon57.png" sizes="152x152">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<link rel="stylesheet" href="/funzata/resources/css/bootstrap.css">

<!-- Related styles of various javascript plugins -->
<link rel="stylesheet" href="/funzata/resources/css/plugins.css">

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
<link rel="stylesheet" href="/funzata/resources/css/main.css">

<!-- Load a specific file here from css/themes/ folder to alter the default theme of the template -->

<!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
<link rel="stylesheet" href="/funzata/resources/css/themes.css">
<link rel="stylesheet" href="/funzata/resources/css/jquery.alerts.css">
<!-- END Stylesheets -->

<!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
<script src="/funzata/resources/js/vendor/modernizr-respond.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/funzata/resources/js/vendor/bootstrap.min.js"></script>
<script src="/funzata/resources/js/plugins.js"></script>
<script src="/funzata/resources/js/main.js"></script>
<script src="/funzata/resources/js/jquery.alerts.js"></script>
<script>
	$(document).ready(function() {
		//carousel  
		setTimeout(function() {
			$("div#divLoading").removeClass('show');
		}, 3000);

		$(document).ajaxStart(function() {
			$("div#divLoading").addClass('show');
		});

		$(document).ajaxStop(function() {
			$("div#divLoading").removeClass('show');
		});

		<c:if test="${menu == 'manageContest'}">
			$('#manageContestA').addClass('active');
		</c:if>
		<c:if test="${menu == 'todaysContest'}">
			$('#todaysContestA').addClass('active');
		</c:if>
		<c:if test="${menu == 'contestSetting'}">
			$('#contestSettingA').addClass('active');
		</c:if>
		<c:if test="${menu == 'hostPage'}">
			$('#hostPageA').addClass('active');
		</c:if>
	});
	
	//Function to highlight the selected menu
	//when page reloads
	function saveUserPreference(gridName, colStr) {
		$.ajax({
			url : "${pageContext.request.contextPath}/SaveUserPreference",
			type : "post",
			data : "columns=" + colStr + "&gridName=" + gridName,
			dataType : "json",
			success : function(res) {
				if (res.status == 1) {
					jAlert("Grid preference updated successfully.");
				} else {
					jAlert("There is something wrong. Please try again");
				}
			},
			error : function(error) {
				jAlert("There is something wrong. Please try again" + error,
						"Error");
				return false;
			}
		});
	}
</script>


</head>
<body>
	<!-- container section start -->
	<div id="page-container">
		<!--header start-->
		<header class="navbar navbar-inverse">
			<ul class="navbar-nav-custom pull-right hidden-md hidden-lg">
				<li class="divider-vertical"></li>
				<li><a href="javascript:void(0)" data-toggle="collapse"
					data-target=".navbar-main-collapse"> <i class="fa fa-bars"></i>
				</a></li>
			</ul>
			<a href="${pageContext.request.contextPath}/Dashboard"
				class="navbar-brand"><img src="/funzata/resources/img/template/logo.png" alt="logo" height="23px" width="120px"></a>
			<div id="loading" class="pull-left">
				<i class="fa fa-certificate fa-spin"></i>
			</div>
			<ul id="widgets" class="navbar-nav-custom pull-right">
				<!-- Just a divider -->
				<li class="divider-vertical"></li>
				<!-- END Messages Widget -->

				<li class="divider-vertical"></li>

				<!-- User Menu -->
				<li class="dropdown pull-right dropdown-user"><a
					href="javascript:void(0)" class="dropdown-toggle"
					data-toggle="dropdown"><img src="/funzata/resources/img/template/avatar.png"
						alt="avatar"> <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<!-- Just a button demostrating how loading of widgets could happen, check main.js- - uiDemo() -->
						<!-- <li><a href="javascript:void(0)" class="loading-on"><i
								class="fa fa-refresh"></i> Refresh</a></li>
						<li class="divider"></li>
						<li>
							Modal div is at the bottom of the page before including javascript code
							<a href="#modal-user-settings" role="button" data-toggle="modal"><i
								class="fa fa-user"></i> User Profile</a>
						</li>
						<li><a href="javascript:void(0)"><i class="fa fa-wrench"></i>
								App Settings</a></li>
						<li class="divider"></li> -->
						<li><a href="${pageContext.request.contextPath}/Logout"><i
								class="fa fa-lock"></i> Log out</a></li>
					</ul></li>
				<!-- END User Menu -->
			</ul>
			<!-- END Header Widgets -->
		</header>
		<!--header end-->

		<div id="inner-container">
			<!-- Sidebar -->
			<aside id="page-sidebar"
				class="collapse navbar-collapse navbar-main-collapse">
				<nav id="primary-nav">
					<ul>
						<%-- <li id="dashboard"><a id="dashboardA"
							href="${pageContext.request.contextPath}/Dashboard"
							><i class="fa fa-fire"></i>Dashboard</a></li> --%>
						<li id="manageContest"><a id="manageContestA" href="${pageContext.request.contextPath}/Contest?status=ALL"><i class="fa fa-glass"></i>Manage Contest</a></li>
						<li id="todaysContest"><a id="todaysContestA" href="${pageContext.request.contextPath}/Contest?status=TODAY"><i class="fa fa-font"></i>Todays Contest</a></li>
						<li id="contestSetting"><a id="contestSettingA" href="${pageContext.request.contextPath}/ContestSetting"><i class="fa fa-font"></i>Contest Setting</a></li>
						<li id="hostPage"><a id="hostPageA" href="${pageContext.request.contextPath}/HostPage"><i class="fa fa-power-off"></i>Host Page</a></li>
					</ul>
				</nav>
			</aside>
			<!--sidebar end-->

			<!--main content start-->
			<div id="page-content">
				<tiles:insertAttribute name="body"></tiles:insertAttribute>
				<div id="divLoading"></div>
			</div>
			<!--main content end-->

		</div>
	</div>
</body>
</html>
